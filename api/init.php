<?php
error_reporting(0);
//CORE PATH
	define("view","views/");
	define("USER_AVATAR_BASEPATH", "../assets/user_avatar/");
	define("CONVO_AVATAR_BASEPATH", "../assets/convo_avatar/");
	define("MYGROUP_AVATAR_BASEPATH", "../assets/mygroup_avatar/");
	define("MSG_ATTACHMENT_BASEPATH", "../assets/gc_msg_attachments/");
	define("FILE_ATTACHMENT_BASEPATH", "../assets/file_extension_icon/");
	
// NOTICE
	define("SHOW_NOTICE", "Y");
	define("NOTICE_MSG", "<marquee><b>SUP!</b> You can change your password now, click your name located at the upper-right corner then select profile.</marquee>");

// GLOBALS DATABASE CONFIG AND OTHERS
	$GLOBALS['config'] = array(
		'mysql' => array(
			'host' => '50.62.135.136',
			'username' => 'wdysolut_root',
			'password' => 'jjwdy2177!',
			'database' => 'pms'
		),

		'footer' => array(
			'description' => '<span>Copyright © DUEDIEWIN.GPN 2020 - '.date('Y').'</span>'
		),

		'company' => array(
			'name' => 'PMS &trade;',
			'owner' => ''
		)
	);


// CSS LOADER 
	define("LOADER_ROLL", "<div class='lds-roller'><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>");
	define("LOADER_PULSE", "<div class='lds-ripple'><div></div><div></div></div>");


// CLASSES AND FUNCTIONS (inside directory)
	define ("VALUE",serialize (array ("auth.php","global_functions.php")));
