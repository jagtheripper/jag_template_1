<?php
error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_NOTICE);
include __DIR__ . '/../init.php';

// ini_set("post_max_size ","0");
// ini_set("upload_max_filesize","2048M");
// ini_set("memory_limit","2048M");
// ini_set("max_input_time","-1");
// ini_set("max_execution_time","3600");
// ini_set("max_input_vars","4048");

// SET TIMEZONE
	ini_set('date.timezone','Asia/Manila');
	date_default_timezone_set('Asia/Manila');
	$today = date('H:i:s');
	$date = date('Y-m-d H:i:s', strtotime($today)+28800);

// START THE SESSION
	session_start();

// CONNECT TO DATABASE SERVER
	$host = $GLOBALS['config']['mysql']['host'];
	$username = $GLOBALS['config']['mysql']['username'];
	$password = $GLOBALS['config']['mysql']['password'];
	$database = $GLOBALS['config']['mysql']['database'];

	@mysql_connect($host, $username, $password) or die("Cannot connect to MySQL Server");
	@mysql_select_db($database) or die ("Cannot connect to Database");
	@mysql_query("SET SESSION sql_mode=''");

// INCLUDE ALL CLASSES AND FUNCTIONS
	foreach(unserialize(VALUE) as $val){
		if(!empty($val)){
			include  __DIR__ .'/'.$val;
		}
	}


// UPDATE REMOVED CHAT MEMBERS
$cnvid = $_SESSION['chat']['convo'];
$cnvmembr = $_SESSION['system']['userid_'];
$refreshChat = SELECT_QUERY("count(member_id)","tbl_convo_member","convo_id = '$cnvid' AND member_id = '$cnvmembr' AND member_status = 1");
if($refreshChat[0] > 0){
	$_SESSION['chat']['convo'] = "";
	$_SESSION['chat']['channel'] = "";
}

// LOAD ALL NEEDED CLASS
	spl_autoload_register(function($class){
		switch ($class) {
			case 'GlobalModule':
				require_once 'classes/global_module.class.php';
				break;

			case 'Project':
				require_once 'classes/project.class.php';
				break;

			case 'Chat':
				require_once 'classes/chat.class.php';
				break;
			
			default:
				break;
		}
	});
