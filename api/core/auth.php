<?php
function checkLoginStatus(){
	if (!isset($_SESSION['system']['userid_'])){
        header("Location: auth/login.php");
        exit;
    }
}

function clean($str) {
        $str = @trim($str);
        if(get_magic_quotes_gpc()) {
            $str = stripslashes($str);
        }
        return htmlentities(mysql_real_escape_string($str));
}

function generateRandomString($length = 4) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return strtoupper($randomString);
}

function processLogin(){
    $userlogin = clean($_POST['username']);
    $userpassword = clean($_POST['password']);
    $query = "SELECT * FROM tbl_users WHERE username = '$userlogin' AND password = md5('$userpassword')";
    $result = mysql_query($query) or die (mysql_error());

    if(mysql_num_rows($result) == 1)
    {
        $row = mysql_fetch_assoc($result);
        $_SESSION['system']['userid_'] = $row[user_id];
        $_SESSION['system']['company_id_'] = $row[company_id];
        $_SESSION['system']['role_id_'] = $row[role_id];
        $_SESSION['system']['user_name_'] = $row['name'];
        $_SESSION['chat']['last_line'] = "";
        $_SESSION['chat']['last_counter'] = "";
        $_SESSION['chat']['convo'] = "";
        $_SESSION['chat']['channel'] = "";
        $_SESSION['rb']['last_rb_counter'] = "";
        $_SESSION['announcement']['last_an_counter'] = "";
        $_SESSION['chat']['last_dm_line'] = "";
        $_SESSION['chat']['recepient_id'] = "";
        $_SESSION['chat']['last_dm_counter'] = "";
        $_SESSION['chat']['start'] = "";

        $getUserTeamCode = mysql_fetch_array(mysql_query("SELECT teamCode FROM tbl_team_member WHERE user_id = '$row[user_id]'"));
        $_SESSION["system"]["teamcode_"] = $getUserTeamCode[0];
        $user_id = $_SESSION['system']['userid_'];

        mysql_query("UPDATE tbl_users set status='1' where user_id=$user_id ");
        $_SESSION['user_status_'] = 1;

        // header("Location:../index.php");
        // exit;
        return 1;

    }else {
        $_SESSION['system']['error_']  = "Your Credentials did not matched!";
        return 2;
        // header("Location:../auth/login.php");
        // exit;
    }
}
