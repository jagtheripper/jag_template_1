<?php

class Project
{

    public function ofLoggedUser()
    {
        // // PROJECTS OF LOGGED USERS
        $user_id = $_SESSION["system"]["userid_"];
        $loop_projects = mysql_query("SELECT t1.CODE as project_code FROM (SELECT pm.projectCode AS CODE FROM tbl_project_member AS pm, tbl_team_member AS tm WHERE pm.teamCode = tm.teamCode AND tm.user_id = '$user_id' AND pm.type = 1 GROUP BY pm.projectCode UNION ALL SELECT projectCode AS CODE FROM tbl_project_member WHERE user_id = '$user_id' AND TYPE = 0 UNION ALL SELECT prj.projectCode as CODE FROM tbl_project as prj WHERE prj.proj_pm = '$user_id' AND prj.status = 0) AS t1 GROUP BY t1.CODE");
        while($proList = mysql_fetch_array($loop_projects)){
            $projData = $this->getDetail($proList['project_code']);
            $data[] = array(
                'id'                    => $projData[project_id],
                'projectCode'           => clean($projData["projectCode"]),
                'projectName'           => clean($projData["projectName"]),
                'projectDescription'    => clean($projData["projectDescription"])
            );
        }
        return $data;    
    }

    public function checkUserteam($user_id)
    {
        $tm_data = SELECT_LOOP_QUERY("*","tbl_team_member","user_id = '$user_id' GROUP BY teamCode");
        if(count($tm_data) > 0){
            foreach($tm_data as $tm_list){
                $data[] = array(
                    'teamCode'   => clean($tm_list["teamCode"]),
                    'user_id'    => $tm_list[user_id],
                    'role_id'    => $tm_list[role_id]
                );
            }
            return $data;
        }
    }

    public function getMemberInTeam($teamCode)
    {
        $tm_data = SELECT_LOOP_QUERY("*","tbl_team_member","teamCode = '$teamCode'");
        if(count($tm_data) > 0){
            foreach($tm_data as $tm_list){
                $data[] = array(
                    'teamCode'   => clean($tm_list["teamCode"]),
                    'user_id'    => $tm_list[user_id],
                    'role_id'    => $tm_list[role_id]
                );
            }
            return $data;
        }
    }

    public function checkIFuserIsInGroup($projectCode, $deleted_user, $deleted_teamCode="")
    {
        $data = 0;
        $t_params = ($deleted_teamCode == "")?"AND `type` = 1":"AND teamCode != '$deleted_teamCode' AND `type` = 1";
        $loopRemainingGroup = SELECT_LOOP_QUERY("teamCode","tbl_project_member","projectCode = '$projectCode' $t_params");
        foreach($loopRemainingGroup as $rm_list){

            $Group = SELECT_QUERY("count(user_id)","tbl_team_member","teamCode = '$rm_list[teamCode]' AND user_id = '$deleted_user'");
            if($Group[0] > 0){
                $data += 1;
            }else{
                $data = 0;
            }
        }

        if($data > 0){
            return 1;
        }else{
            return 0;
        }
    }

    public function checkIFuserIsInMember($projectCode, $deleted_user)
    {

        $member = SELECT_QUERY("count(user_id)","tbl_project_member","user_id = '$deleted_user' AND projectCode = '$projectCode' AND type = 0");
 
        if($member[0] > 0){
            return 1;
        }else{
            return 0;
        }
    }

    public function deleteUserTask($member_id, $projectCode)
    {
        $getTaskData = SELECT_LOOP_QUERY("task_id","tbl_task_member","user_id = '$member_id' AND projectCode = '$projectCode'");
        if(count($getTaskData) > 0){
            foreach($getTaskData as $taskList){
                DELETE_QUERY("tbl_task","task_id = '$taskList[0]'");
            }
        }

        $reslt = DELETE_QUERY("tbl_task_member", "user_id = '$member_id' AND projectCode = '$projectCode'");
        echo $reslt;
    }

    public function deleteProjectMember($projectCode, $teamCode, $member_id, $type)
    {
        if($type == 1){
            // GROUP MEMBER
            $loop_group_in_project = $this->getMemberInTeam($teamCode);
            if(count($loop_group_in_project) > 0){
                foreach($loop_group_in_project as $groupList){
                    $isInGroup = $this->checkIFuserIsInGroup($projectCode, $groupList[user_id], $teamCode);
                    $isInmember = $this->checkIFuserIsInMember($projectCode, $groupList[user_id]);
                    $delProjectMember = DELETE_QUERY("tbl_project_member", "teamCode = '$teamCode' AND projectCode = '$projectCode' AND type = 1");
                    if($delProjectMember){
                        if($isInGroup == 1){
                            // echo $groupList[user_id]." IF equals: do not delete task!<br><br>";
                        }else{
                            if($isInmember == 1){
                                // echo $groupList[user_id]." IF equals: do not delete task!<br><br>";
                            }else{
                                $this->deleteUserTask($groupList[user_id], $projectCode);
                            }
                        }
                    }
                }
            }
        }else{
            $isInGroup = $this->checkIFuserIsInGroup($projectCode, $member_id);
            $isInmember = $this->checkIFuserIsInMember($projectCode, $member_id);
            $delProjectMember = DELETE_QUERY("tbl_project_member", "user_id = '$member_id' AND projectCode = '$projectCode' AND type = 0");
            if($delProjectMember){
                if($isInGroup == 1){
                    // echo $member_id." IF equals: do not delete task!<br><br>";
                }else{
                    if($isInmember == 1){
                        // echo $member_id." IF not: delete task!<br>";
                        $this->deleteUserTask($member_id, $projectCode);
                    }else{
                        // echo $member_id." IF equals: do not delete task!<br><br>";
                    }
                }
            }
        }
    }

    public function personal()
    {
        $user_id = $_SESSION["system"]["userid_"];
        $getProject = SELECT_LOOP_QUERY("project_id AS id, projectCode, projectName, projectDescription", "tbl_project", "proj_pm = '$user_id' ORDER BY projectName ASC");
        if(count($getProject) > 0){
            foreach($getProject as $personal_list){
                $data[] = array(
                    'id'                    => $personal_list[id],
                    'projectCode'           => clean($personal_list["projectCode"]),
                    'projectName'           => clean($personal_list["projectName"]),
                    'projectDescription'    => clean($personal_list["projectDescription"])
                );
            }
        }

        return $data;
    }

    public function getDetail($projectCode)
    {
        $getProject = SELECT_QUERY("*", "tbl_project", "projectCode = '$projectCode' ORDER BY projectName ASC");
        
        return $getProject;
    }

    public function getName($projectCode){
        $projectName = mysql_fetch_array(mysql_query("SELECT projectName FROM tbl_project WHERE projectCode = '$projectCode'"));
        return $projectName[0];
    }


    public function getAllTask($status, $projectCode, $teamcode = NULL)
    {
        if($teamcode != NULL){
            $team_code = "AND mem.teamcode = '$teamcode'";
        }else{
            $team_code = "";
        }

        $getPorject = mysql_query("SELECT mem.projectCode AS projectCode, mem.teamCode AS teamCode, mem.user_id AS `user_id`, task.projectCode AS projectCode,task.task_id AS task_id, task.taskDescription AS task_desc,
        task.taskDueDate AS task_due, task.taskCreateDate AS task_created, mem.teamCode AS teamCode, task.priority_stats AS priority_stats 
        from tbl_task AS task, tbl_task_member as mem where mem.task_id = task.task_id AND task.status = '$status' AND task.projectCode = '$projectCode' $team_code ORDER BY priority_stats DESC,taskDueDate ASC");
        while($getRow = mysql_fetch_array($getPorject)){
            if($getRow[priority_stats] == 2){
                $priorityStats = "red";
            }else if($getRow[priority_stats] == 1){
                $priorityStats = "orange";
            }else{
                $priorityStats = "green";
            }
            $data[] = array(
                'taskID'        => $getRow[task_id], 
                'teamCode'        => $getRow[teamCode], 
                'teamMember'      => getUserName($getRow[user_id]),
                'projectCode'     => $getRow[projectCode],
                'task'            => $getRow[task_desc],
                'date_created'    => $getRow[task_created],
                'date_due'        => $getRow[task_due],
                'priority'        => $priorityStats
            );
        }
        return $data;
    }


    public function getUserTask($status, $projectCode, $member)
    {
        $userid = $_SESSION["system"]["userid_"];
        $withFilter = ($member == "")?" AND mem.user_id = '$userid' ":" AND mem.user_id = '$member' ";
        $getPorject = mysql_query("SELECT task.projectCode AS projectCode,task.task_id AS task_id, task.taskDescription AS task_desc, task.taskDueDate AS task_due, task.taskCreateDate AS task_created, mem.teamCode AS teamCode, task.priority_stats AS priority_stats, task.task_code AS task_code FROM tbl_task AS task, tbl_task_member as mem where mem.task_id = task.task_id AND task.status = '$status' AND task.projectCode = '$projectCode' $withFilter ORDER BY priority_stats DESC , taskDueDate ASC");
        while($getRow = mysql_fetch_array($getPorject)){
            if($getRow[priority_stats] == 2){
                $priorityStats = "red";
            }else if($getRow[priority_stats] == 1){
                $priorityStats = "orange";
            }else{
                $priorityStats = "green";
            }
            $data[] = array(
                'taskID'          => $getRow[task_id],
                'projectCode'     => $getRow[projectCode],
                'task'            => $getRow[task_desc],
                'date_created'    => $getRow[task_created],
                'date_due'        => $getRow[task_due],
                'priority'        => $priorityStats,
                'task_code'        => $getRow[task_code]
            );
        }
        return $data;
    }

    public function remove($projectCode)
    {
        // PROJECT
        $res = DELETE_QUERY("tbl_project", "projectCode = '$projectCode'");
        if($res){
            // PROJECT MEMBER
            DELETE_QUERY("tbl_project_member", "projectCode = '$projectCode'");
            // TASK
            DELETE_QUERY("tbl_task", "projectCode = '$projectCode'");
            // TASK MEMBER
            DELETE_QUERY("tbl_task_member", "projectCode = '$projectCode'");
        }
    }

    public function getTeamByProjectCode($projectCode)
    {
        $teamCode = mysql_query("SELECT code.team_code AS team_code FROM (SELECT tm.teamCode AS team_code FROM tbl_project_member as pm, tbl_team_member as tm WHERE pm.user_id = tm.user_id AND pm.projectCode = '$projectCode' AND pm.`type` = 0 UNION ALL SELECT teamCode as team_code FROM tbl_project_member WHERE projectCode = '$projectCode'AND `type` = 1) as code GROUP BY code.team_code");
        while($tcList = mysql_fetch_array($teamCode)){
            $data[] = $tcList;
        }

        return $data;
    }
    
}