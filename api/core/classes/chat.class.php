<?php

class Chat 
{
    public function getChannels($convo_id)
    {
        $loop_channel = SELECT_LOOP_QUERY("*","tbl_convo_channel","convo_id = '$convo_id' ORDER BY name ASC");
        foreach($loop_channel as $channel_list){
            $data[] = array(
                'id' => $channel_list[channel_id],
                'name' => clean($channel_list["name"])
            ); 
        }
        return $data;
    }

    public function getConvos($user_id)
    {
        // IF CONVO MEMBER
        $loop_link_member = $this->getConvoLinkMember($user_id);
        if(count($loop_link_member) > 0){
            foreach($loop_link_member as $link_member_list){
                $loop_convos = SELECT_LOOP_QUERY("*","tbl_convo","convo_id = '$link_member_list[convo_id]' ORDER BY date_created DESC");
                if(count($loop_convos) > 0){
                    foreach($loop_convos as $convo_list){
                        $data[] = array(
                            'convo_id' => $convo_list[convo_id],
                            'created_by' => $convo_list["created_by"],
                            'date_created' => $convo_list["date_created"],
                            'convo_name' => clean($convo_list["convo_name"]),
                            'slug' => $convo_list["slug"]
                        ); 
                    }
                }
            }
        }

        return $data;
    }

    public function getConvoLinkMember($user_id)
    {
        $loop_membr = SELECT_LOOP_QUERY("*","tbl_convo_member","member_id = '$user_id' AND member_status = 0 GROUP BY convo_id");
        foreach($loop_membr as $membr_list){
            $data[] = array(
                'convo_member_id' => $membr_list[channel_id],
                'member_id' => $membr_list[member_id],
                'convo_id' => $membr_list[convo_id],
                'date_added' => $membr_list['date_added']
            ); 
        }
        return $data;
    }

    public function getPeople()
    {
        $convo_id = $_SESSION['chat']['convo'];
        $loop_membr = SELECT_LOOP_QUERY("*","tbl_convo_member","convo_id = '$convo_id' AND member_status = 0");
        foreach($loop_membr as $membr_list){
            $data[] = array(
                'convo_member_id' => $membr_list[channel_id],
                'member_id' => $membr_list[member_id],
                'member_name' => getUserName($membr_list[member_id]),
                'convo_id' => $membr_list[convo_id],
                'date_added' => $membr_list['date_added'],
                'nickname' => clean($membr_list['nickname'])
            ); 
        }
        return $data;
    }

    public function DM_CONVO()
    {
        // channel_id == receiver_id 
        $user_id = $_SESSION["system"]["userid_"];
        $loop_membr = SELECT_LOOP_QUERY("*,IF(sender_id = '$user_id',channel_id ,sender_id ) AS cc","tbl_convo_msg","convo_id = -1 AND (channel_id = '$user_id' OR sender_id = '$user_id') GROUP BY cc");
        foreach($loop_membr as $membr_list){
            $data[] = array(
                'receiver_id' => $membr_list[channel_id],
                'sender_id' => $membr_list[cc],
                'member_name' => getUserName($membr_list[cc]),
                'convo_id' => -1,
                'date_added' => $membr_list['date_added']
            ); 
        }
        return $data;
    }

    public function getConvoAdmin($user_id, $convo_id)
    {
        $res = SELECT_QUERY("count(member_id)","tbl_convo_member","member_id = '$user_id' AND convo_id = '$convo_id' AND convo_role = 1 AND member_status = 0");
        if($res[0] > 0){
            return 1;
        }else{
            return 0;
        }
    }

    public function convo_counter($convo_id)
    {
        $user_id = $_SESSION["system"]["userid_"];
        $data = SELECT_QUERY("count(notif_id) as total","tbl_convo_notif","receiver_id = '$user_id' AND convo_id = '$convo_id' GROUP BY convo_id");
        return $data[0];
    }

    public function channel_counter($convo_id, $channel_id)
    {
        $user_id = $_SESSION["system"]["userid_"];
        $data = SELECT_QUERY("count(notif_id) as total","tbl_convo_notif","receiver_id = '$user_id' AND convo_id = '$convo_id' AND channel_id = '$channel_id' GROUP BY channel_id");
        return $data[0];
    }

    public function all_counter()
    {
        $user_id = $_SESSION["system"]["userid_"];
        $data = SELECT_QUERY("count(notif_id) as total","tbl_convo_notif","receiver_id = '$user_id' AND convo_id != -1");
        $subTotal = $data[0] + DM_TOTAL_BADGE_COUNTER();
        $total = ($subTotal > 0)?$subTotal:"";
        return $total;
    }
}