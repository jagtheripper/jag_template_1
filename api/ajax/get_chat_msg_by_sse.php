<?php
include '../core/config.php';
$msg = $_POST['msg_id'];
$convo_id = $_SESSION['chat']['convo'];
$channel_id = $_SESSION['chat']['channel'];
$user_id = $_SESSION["system"]["userid_"];

$msgList = SELECT_QUERY("*","tbl_convo_msg","chat_id = '$msg'");

$sse_msg_img_basepath = MSG_ATTACHMENT_BASEPATH;
$author = SELECT_QUERY("*","tbl_users","user_id = '$msgList[sender_id]'");
$memberData = SELECT_QUERY("nickname","tbl_convo_member","convo_id = '$convo_id' AND member_id = '$msgList[sender_id]'");
$hasNickName = ($memberData[0] == "")?clean(getUserName($msgList[sender_id])):clean($memberData[0]);
$hasPriv = ($user_id == $msgList[sender_id])?1:0;
$hasFileAtachment = ($msgList["slug"] != "" && $msgList["filename"] != "")?1:0;
$img_ext = end(explode('.', $msgList["filename"]));
$attachment_extension = ($hasFileAtachment == 1)?extension_icon($img_ext):'';

$sse_msg_slug = ($msgList["slug"] != "")?'<div class="col pb-2" style="padding: 0px;display: grid;grid-auto-flow: row;grid-row-gap: .25rem;text-indent: 0;min-height: 0;min-width: 0;"><img src="'.$sse_msg_img_basepath.$msgList["slug"].'" style="width: 60%;object-fit: contain;object-position: 0 0;border-radius: 6px;justify-self: start;align-self: start;" onclick="previewMedia(\''.$sse_msg_img_basepath.$msgList["slug"].'\')"></div>':'';

$dl_var = "ajax/media_download.php?file_name=".clean($msgList["filename"]);
$sse_isFile = '<div class="col pb-2" style="text-align: left;padding: 0px;"><div style="width: 70%;display: flex;flex-direction: row;align-items: center;align-content: center;padding: 15px;background: #f1f1f1;border-radius: 8px;"><a href="#" style="width: 35px; height: 30px;"><img src="'.$attachment_extension.'" style="width: 100%;height: 100%;object-fit: cover;" data-toggle="tooltip" data-placement="left" data-original-title="" title=""></a><h4 class="text-muted" style="font-family: myFirstFont;font-size: 1rem;font-weight: 400;white-space: pre-wrap;margin-bottom: 0px;width: -webkit-fill-available;margin-left: 7px;word-break: break-all;">'.clean($msgList["filename"]).'</h4><a href="'.$dl_var.'"><i class="fas fa-download" style="padding-left: 17px;font-size: 19px;"></i></a></div></div>';

$sse_media_display = ($hasFileAtachment == 1)?$sse_isFile:$sse_msg_slug;

$sse_msg_options = ($hasPriv == 1)?"<div class='show-on-msg-hover' style='position: absolute;top: 3px;right: 8px;cursor: pointer;'><span class='badge badge-pill badge-default upload-media-icon' style='color: #48536f;border: 0px solid #fff;padding: 6px;margin: 2px;background: #e6e6e6;' title='edit' onclick='edit_gc_msg(".$msgList[chat_id].")'><i class='fa fa-pen' style='font-size: 12px;'></i></span><span class='badge badge-pill badge-default upload-media-icon' style='color: #48536f;border: 0px solid #fff;padding: 6px;margin: 2px;background: #e6e6e6;' title='delete' onclick='delete_gc_msg(".$msgList[chat_id].",".$msgList[convo_id].",".$msgList[channel_id].")'><i class='far fa-trash-alt' style='font-size: 12px;color: red;'></i></span></div>":"";

$sse_isEdited = ($msgList[edit_status] == 1)?' &bull; Edited':'';

$data = "<div class='pl-2 pt-2 pb-0 msg_hover' style='border: 0px;width: 100%;'><div class='row'><div class='col-auto pr-0 align-items-start' style='padding-top: 3px;'><img src='".getUserAvatar($msgList[sender_id])."' style='width:40px; height: 40px;object-fit: cover;' class='rounded-circle' data-toggle='tooltip' data-placement='left' title='".getUserName($msgList[sender_id])."'></div><div class='col pl-3 align-items-center'><div style='text-align: start;padding: 0px;'><div class='row' style='margin: 0px;'><div class='col-12' style='display: flex;width: 100px;padding: 0px;'><div style='width: 84%;'><h3 class='text-muted mb-0' style='font-family: myFirstFont;font-weight: bold;color: #4a4949 !important;cursor: pointer;text-overflow: ellipsis;width:-webkit-fill-available;white-space: nowrap;overflow: hidden;' id='channel_name'>".$hasNickName."</h3></div>".$sse_msg_options."</div><div class='col-12 pl-0' style='line-height: 7px;'><small class='text-muted' style='font-size: 71% !important;'>".date("m/d/Y h:i A", strtotime($msgList[date_added])).$sse_isEdited."</small></div></div></div><textarea hidden id='gc_msg_content_box_".$msgList[chat_id]."' style='font-size: 1rem;font-family: myFirstFont; word-break: break-word;white-space: pre-wrap;color: #4e4e4e;'>".$msgList["msg"]."</textarea><div style='font-size: 18px;font-family: myFirstFont;word-break: break-word;white-space: pre-wrap;color: #4e4e4e; margin-bottom: 5px;line-height: 22px;-webkit-user-modify: read-only-plaintext-only;outline: -webkit-focus-ring-color auto 0px;'>".html_entity_decode($msgList["msg"])."</div>".$sse_media_display."</div></div></div>";

echo $data;
