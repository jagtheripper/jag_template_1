<?php
include '../core/config.php';
$userid = $_SESSION['system']['userid_'];
$convo_id = $_SESSION['chat']['convo'];
$crop_img = $_POST['image'];

list($type, $crop_img) = explode(';', $crop_img);
list(, $crop_img) = explode(',', $crop_img);

$crop_img = base64_decode($crop_img);
$image_name = $convo_id.'-'.date('ymdhis').'.jpg';
$fileSlug = '../'.CONVO_AVATAR_BASEPATH.$image_name; //, $crop_img

// getOlAvatar
$old_data = SELECT_QUERY("slug","tbl_convo","convo_id = '$convo_id'");
$oldAvatar = ($old_data[0] == "")?'':'../'.CONVO_AVATAR_BASEPATH.$old_data[0];

if(file_put_contents($fileSlug, $crop_img)) {
    
    if(!empty($old_data[0])){
        if(file_exists($oldAvatar)){
            unlink($oldAvatar);
        }
    }

    $data = array(
        'slug' => $image_name
    );
    $res = UPDATE_QUERY("tbl_convo",$data,"convo_id = '$convo_id'");
    if($res){
        echo 'Uploaded!';
    }
}else{
    echo 'Error uploading image!';
}