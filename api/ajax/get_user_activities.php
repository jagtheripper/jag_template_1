<?php
include '../core/config.php';
$user_id = $_SESSION["system"]["userid_"];
$response = array();

$loop_activity = SELECT_LOOP_QUERY("*", "tbl_activity_logs" , "user_id = '$user_id' ORDER BY log_id DESC LIMIT 100");
if(count($loop_activity) > 0){
    foreach($loop_activity as $activity_list){
        $data = array(
            'log' => clean($activity_list["log"]), 
            'module' => clean($activity_list["module"]), 
            'date' => date("m/d/Y h:i A", strtotime($activity_list['date'])),
            'user_id' => $activity_list[user_id],
            'task_code' => clean($activity_list["task_code"]),
            'project_name' => getProjectByTask($activity_list["task_code"])
        );
        array_push($response,$data);
    }
}
echo json_encode($response);
