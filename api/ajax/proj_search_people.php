<?php
include '../core/config.php';
$search_q = clean($_POST['search_q']);
$projectCode = $_POST['project_code'];

if($search_q != ""){
    if(strpos($search_q, '@')){
        $loop_user = SELECT_LOOP_QUERY("*","tbl_users","email LIKE '%$search_q%'");
        if(count($loop_user) > 0){
            foreach($loop_user as $user_list){
                $projMemberChecker = SELECT_QUERY("count(user_id)","tbl_project_member","projectCode = '$projectCode' AND user_id = '$user_list[user_id]' AND type = 0");
                $user_avatar = getUserAvatar($user_list[user_id]);
                if($projMemberChecker[0] < 1){
                    $data .= '<li class="list-group-item px-0 pb-0"><div class="row align-items-center"><div class="col-auto pr-0"><a href="#" class="avatar rounded-circle" style="width: 40px;height: 40px;"><img src="'.$user_avatar.'" style="width: 100%;height: 100%;object-fit: cover;" class="rounded-circle"></a></div><div class="col pl-2"><h5 class="text-muted mb-0">'.clean($user_list["name"]).'</h5><small class="text-muted">'.clean($user_list["email"]).'</small></div><div class="col"><div style="align-items: baseline;justify-content: flex-end;display: flex;"><a href="#" class="btn btn-success btn-sm" onclick="invitePeopleToProject(\''.$user_list[user_id].'\')">invite</a></div></div></div></li>';
                }else{
                    $data .= '<li class="list-group-item px-0 pb-0"><b>'.clean($user_list["name"]).'</b> is already in your project.</li>';
                }
            }

            echo $data;
        }
    }else{
        echo 1;
    }
}