<?php
// include '../core/config.php';
// $chat = new Chat();
// $user_id = $_SESSION["system"]["userid_"];
// $loop_convo = $chat->getConvos($user_id);
// $DM_total_badge = DM_TOTAL_BADGE_COUNTER();
// $data .= '<div class="col" title="Direct Message" style="transition: all 0s ease 0s;"><a href="#" class="avatar rounded-circle mb-0 mt-1" style="width: 50px;height: 50px;" onclick="chat_session_updater(\'-1\',\'dm\')"><i class="fas fa-comment-alt"></i></a><div style="position: absolute;bottom: 0px;right: 6px;"><span class="badge badge-pill badge-danger" style="color: #fff;border: 3px solid #fff;" id="dm_badge_pill">'.$DM_total_badge.'</span></div></div><hr class="mx-2 my-2">';

// if(count($loop_convo) > 0){
//     foreach($loop_convo as $convo_list){
//         $counter_pill = $chat->convo_counter($convo_list[convo_id]);
//         $convoBadge = "convo_badge_".$convo_list[convo_id];
//         $data .= '<div class="col" title="'.$convo_list[convo_name].'" style="transition: 	all 0s ease 0s;margin-bottom: 7px;"><div onclick="chat_session_updater(\''.$convo_list[convo_id].'\',\'convo\')">'.getConvoAvatar($convo_list[convo_id]).'</div><div style="position: absolute;bottom: 0px;right: 6px;"><span class="badge badge-pill badge-danger" style="color: #fff;border: 3px solid #fff;" id="'.$convoBadge.'">'.$counter_pill.'</span></div></div>';
//         $data .= "<input type='hidden' value='".CONVO_AVATAR_BASEPATH.$convo_list[slug]."' id='convo_id_with_slug_".$convo_list[convo_id]."'>";
//         // $data = 'onclick="updater(\'"'.$data.'"\')"';
//     }
// }

// $data .= '<div class="col" title="New Group" style="transition: all 0s ease 0s;" onclick="addNewConvo()"><a href="#" class="avatar rounded-circle mb-0 mt-1" style="width: 50px;height: 50px;border: 2px dashed green;background-color: #d4d8dc;"><i class="fas fa-plus" style="color: green;"></i></a><div style="position: absolute;bottom: 0px;right: 6px;"></div></div>';


// echo $data;

include '../core/config.php';
$chat = new Chat();
$user_id = $_SESSION["system"]["userid_"];
$loop_convo = $chat->getConvos($user_id);
$DMcounter = DM_TOTAL_BADGE_COUNTER();
$DM_total_badge = ($DMcounter > 0)?$DMcounter:"";
$response = array();

if(count($loop_convo) > 0){
    foreach($loop_convo as $convo_list){
        $gc_count = $chat->convo_counter($convo_list[convo_id]);
        $counter_pill = ($gc_count > 0)?$gc_count:'';
        $data = array(
            'group' => array(
                'id' => $convo_list[convo_id],
                'name' => clean($convo_list["convo_name"]),
                'avatar' => getConvoAvatar($convo_list[convo_id]),
                'slug' => $convo_list["slug"],
                'unread_counter' => $counter_pill
            ),
            'author' => array(
                'id' => $convo_list[created_by],
                'avatar' => getUserAvatar($convo_list[created_by]),
                'username' => clean(getUserName($convo_list[created_by]))
            ),
            'date_exist' => date("Y-m-d h:i A", strtotime($convo_list[date_created])),
            'dm_unread_counter' => $DM_total_badge
        );
        array_push($response,$data);
    }
}
echo json_encode($response);