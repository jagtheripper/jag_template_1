<?php
include '../core/config.php';

header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
$convo_id = $_SESSION['chat']['convo'];
$channel_id = $_SESSION['chat']['channel'];
$user_id = $_SESSION["system"]["userid_"];

$msgList = SELECT_QUERY("chat_id","tbl_convo_msg","convo_id='$convo_id' AND channel_id='$channel_id' ORDER BY chat_id DESC LIMIT 1");
$last_line = $msgList[chat_id];
if($last_line != $_SESSION['chat']['last_line']){
    echo "data: {$last_line}\n\n";
    $_SESSION['chat']['last_line'] = $last_line;
}
ob_flush();
flush();
?>