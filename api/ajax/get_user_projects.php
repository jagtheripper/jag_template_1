<?php
include '../core/config.php';
$user_id = $_SESSION["system"]["userid_"];
$response = array();

$project = new Project();
$loop_projects = mysql_query("SELECT t1.CODE as project_code FROM (SELECT pm.projectCode AS CODE FROM tbl_project_member AS pm, tbl_team_member AS tm WHERE pm.teamCode = tm.teamCode AND tm.user_id = '$user_id' AND pm.type = 1 GROUP BY pm.projectCode UNION ALL SELECT pjm.projectCode AS CODE FROM tbl_project_member AS pjm, tbl_project AS p WHERE p.projectCode = pjm.projectCode AND pjm.user_id = '$user_id' AND pjm.type = 0 AND p.status = 0 UNION ALL SELECT prj.projectCode as CODE FROM tbl_project as prj WHERE prj.proj_pm = '$user_id' AND prj.status = 0) AS t1 GROUP BY t1.CODE");
while($proList = mysql_fetch_array($loop_projects)){
    $projData = $project->getDetail($proList['project_code']);
    $data = array(
        'id'                    => $projData[project_id],
        'projectCode'           => clean($projData["projectCode"]),
        'projectName'           => clean($projData["projectName"]),
        'projectDescription'    => clean($projData["projectDescription"]),
        'projectAvName'         => getAllFirstLetter(clean($projData["projectName"]))
    );
    array_push($response,$data);
}
echo json_encode($response);