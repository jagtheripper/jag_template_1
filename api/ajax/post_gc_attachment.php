<?php
include '../core/config.php';
$image = $_POST['attachment_val'];
$msg = clean($_POST['msg']);
$type = $_POST['type'];
$user_id = $_SESSION["system"]["userid_"];
$date_sent = date("Y-m-d H:i:s");
if($type == "GC"){
    $ch_id = $_SESSION['chat']['channel'];
    $convo_id = $_SESSION['chat']['convo'];
}else{
    $ch_id = $_POST['recpient_id'];
    $convo_id = -1;
}
if($ch_id != "" && $convo_id != ""){
    $crop_img = $image;
    list($type, $crop_img) = explode(';', $crop_img);
    list(, $crop_img) = explode(',', $crop_img);
    $extension = explode('/',$type);

    $crop_img = base64_decode($crop_img);
    $image_name = $convo_id.$ch_id.'-'.date('ymdhis').'.'.$extension[1];
    $fileSlug = '../'.MSG_ATTACHMENT_BASEPATH.$image_name; //, $crop_img

    if(file_put_contents($fileSlug, $crop_img)) {

        $data = array(
            'convo_id' => $convo_id,
            'sender_id' => $user_id,
            'channel_id' => $ch_id,
            'date_added' => $date_sent,
            'msg' => $msg,
            'slug' => $image_name
        );
    
        $msg_id = INSERT_QUERY("tbl_convo_msg", $data, "Y");
        if($msg_id){
            insertToNotif($convo_id, $msg_id, $ch_id, $user_id, $date_sent, $type);
            echo 1;
        }
    }else{
        echo 0;
    }
}else{
    echo 3;
}

function insertToNotif($convo_id, $msg_id, $channel_id, $sender_id, $date_sent, $type)
{
    if($type == "GC"){
        $loop_convo_users = SELECT_LOOP_QUERY("*","tbl_convo_member","convo_id = '$convo_id' AND member_id != '$sender_id'");
        if(count($loop_convo_users) > 0){
            foreach($loop_convo_users as $userList){
                $data = array(
                    'convo_id' => $convo_id,
                    'channel_id' => $channel_id,
                    'msg_id' => $msg_id,
                    'receiver_id' => $userList[member_id],
                    'sender_id' => $sender_id,
                    'date_sent' => $date_sent
                );

                INSERT_QUERY("tbl_convo_notif", $data);
            }
        }
    }else{
        $data = array(
            'convo_id' => $convo_id,
            'channel_id' => 0,
            'msg_id' => $msg_id,
            'receiver_id' => $channel_id,
            'sender_id' => $sender_id,
            'date_sent' => $date_sent
        );

        INSERT_QUERY("tbl_convo_notif", $data);
    }
}