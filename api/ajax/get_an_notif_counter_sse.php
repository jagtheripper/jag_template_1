<?php
include '../core/config.php';

header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
$totalCounter = total_announcement_counter();
$last_line_an_counter = $totalCounter;
if($last_line_an_counter != $_SESSION['announcement']['last_an_counter']){
    echo "data: $totalCounter\n\n";
    $_SESSION['announcement']['last_an_counter'] = $last_line_an_counter;
}

//echo "data:", json_encode($c) , "\n\n";
echo "retry: 15000\n";
ob_flush();
flush();
?>