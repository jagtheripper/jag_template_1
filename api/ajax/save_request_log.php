<?php
include '../core/config.php';
$user_id = $_SESSION["system"]["userid_"];
$requestedBy = $_POST['requestedBy'];
$description = clean($_POST['description']);

$data = array(
    'request_date' => date("Y-m-d H:i:s"),
    'logs' => $description,
    'requested_by' => $requestedBy,
    'person_assigned' => $user_id
);

$res = INSERT_QUERY("tbl_request_logs",$data);
echo $res;