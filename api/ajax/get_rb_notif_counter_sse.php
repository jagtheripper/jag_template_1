<?php
include '../core/config.php';

header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
$totalCounter = totalRequestBook();
$last_line_rb_counter = $totalCounter;
if($last_line_rb_counter != $_SESSION['rb']['last_rb_counter']){
    echo "data: $totalCounter\n\n";
    $_SESSION['rb']['last_rb_counter'] = $last_line_rb_counter;
}

//echo "data:", json_encode($c) , "\n\n";
echo "retry: 15000\n";
ob_flush();
flush();
?>