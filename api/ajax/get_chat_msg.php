<?php
include '../core/config.php';
$convo_id = $_SESSION['chat']['convo'];
$channel_id = $_SESSION['chat']['channel'];
$user_id = $_SESSION["system"]["userid_"];

$channelInfo = SELECT_QUERY("*","tbl_convo_channel","convo_id = '$convo_id' AND channel_id = '$channel_id'");
$getMsg = SELECT_LOOP_QUERY("sender_id,msg,date_added,slug,chat_id,filename,edit_status","tbl_convo_msg","convo_id='$convo_id' AND channel_id='$channel_id' ORDER BY chat_id ASC");
if($channel_id == ""){
    $data .= "<div class='pl-0 pt-0 pb-0' style='border: 0px;width: 100%;text-align: center;'><h1 class='pb-0 mb-0'>Accord</h1><p class='mb-1'>Welcome human, let's have an accord!</p></div>";
}else{

    $data .= "<div class='pl-0 pt-0 pb-0' style='width: 100%;border-bottom: 1px solid #ccc;'><h1 style='word-break: break-all;'>Welcome to #".clean($channelInfo["name"])."</h1><p style='word-break: break-all;line-height: 19px;'>This is the start of the #".clean($channelInfo["name"])." channel.</p></div>";

    if(count($getMsg) > 0){
        foreach($getMsg as $msg_list){
            $enddata[] = getUserName($msg_list[0]).",".$msg_list[1];
            $memberData = SELECT_QUERY("nickname","tbl_convo_member","convo_id = '$convo_id' AND member_id = '$msg_list[0]'");
            $hasNickName = ($memberData[0] == "")?clean(getUserName($msg_list[0])):clean($memberData[0]);
            $user_avatar = getUserAvatar($msg_list[0]);

            $attachment_extension = ($msg_list[3] != "" && $msg_list[5] != "")?explode('.', $msg_list[5]):'';

            $msg_slug = ($msg_list[3] != "")?'<div class="col pb-2" style="text-align: left;padding: 0px;"><img src="'.MSG_ATTACHMENT_BASEPATH.$msg_list[3].'" style="width: 100%;height: 224px;object-fit: contain;object-position: 0 0;" onclick="previewMedia(\''.MSG_ATTACHMENT_BASEPATH.$msg_list[3].'\')"></div>':'';

            $isFile = '<div class="col pb-2" style="text-align: left;padding: 0px;"><div style="width: 70%;display: flex;flex-direction: row;align-items: center;align-content: center;padding: 15px;background: #f1f1f1;border-radius: 8px;">
            <a href="#" style="width: 35px; height: 30px;"><img src="'.extension_icon(end($attachment_extension)).'" style="width: 100%;height: 100%;object-fit: cover;" data-toggle="tooltip" data-placement="left" data-original-title="" title="">
            </a><h4 class="text-muted" style="font-family: myFirstFont;font-size: 1rem;font-weight: 400;white-space: pre-wrap;margin-bottom: 0px;width: -webkit-fill-available;margin-left: 7px;word-break: break-all;">'.clean($msg_list[5]).'</h4><i class="fas fa-download" style="padding-left: 17px;font-size: 19px;"></i></div></div>';

            $media_display = ($msg_list[3] != "" && $msg_list[5] != "")?$isFile:$msg_slug;

            $msg_options = ($user_id == $msg_list[0])?"<div class='show-on-msg-hover' style='position: absolute;top: 3px;right: 8px;cursor: pointer;'><span class='badge badge-pill badge-default upload-media-icon' style='color: #48536f;border: 0px solid #fff;padding: 8px;margin: 2px;background: #e6e6e6;' title='edit' onclick='edit_gc_msg(".$msg_list[4].")'><i class='fa fa-pen' style='font-size: 12px;'></i></span><span class='badge badge-pill badge-default upload-media-icon' style='color: #48536f;border: 0px solid #fff;padding: 8px;margin: 2px;background: #e6e6e6;' title='delete' onclick='delete_gc_msg(".$msg_list[4].",".$convo_id.",".$channel_id.")'><i class='far fa-trash-alt' style='font-size: 12px;color: red;'></i></span></div>":"";

            $isEdited = ($msg_list[6] == 1)?' &bull; Edited':'';

            $data .= "<div class='pl-2 pt-2 pb-0 msg_hover' style='border: 0px;width: 100%;'><div class='row'><div class='col-auto pr-0 align-items-start' style='padding-top: 3px;'><img src='".$user_avatar."' style='width:40px; height: 40px;object-fit: cover;' class='rounded-circle' data-toggle='tooltip' data-placement='left' title='".getUserName($msg_list[0])."'></div><div class='col pl-3 align-items-center'>
            <div style='text-align: start;padding: 0px;'>
                <div class='row' style='margin: 0px;'>
                    <div class='col-12' style='display: flex;width: 100px;padding: 0px;'>
                        <div style='width: 84%;'>
                            <h3 class='text-muted mb-0' style='font-family: myFirstFont;font-weight: 400;color: #000 !important;cursor: pointer;text-overflow: ellipsis;width:-webkit-fill-available;white-space: nowrap;overflow: hidden;' id='channel_name'>".$hasNickName."</h3>
                        </div>
                        ".$msg_options."
                    </div>
                    <div class='col-12 pl-0' style='line-height: 7px;'><small class='text-muted' style='font-size: 71% !important;'>".date("m/d/Y h:i A", strtotime($msg_list[2])).$isEdited."</small></div>
                </div>
            </div>
            <textarea hidden id='gc_msg_content_box_".$msg_list[4]."' style='font-size: 1rem;font-family: myFirstFont; word-break: break-word;white-space: pre-wrap;color: #4e4e4e;'>".$msg_list[1]."</textarea><p style='font-size: 1rem;font-family: myFirstFont;word-break: break-word;white-space: pre-wrap;color: #4e4e4e; margin-bottom: 5px;line-height: 22px;'>".$msg_list[1]."</p>".$media_display."</div></div></div>";
        }
        $_SESSION['chat']['last_line'] = end($enddata);
    }
}
echo $data;

?>

