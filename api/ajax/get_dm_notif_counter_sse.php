<?php
include '../core/config.php';

header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');
$user_id = $_SESSION["system"]["userid_"];
$c = array();
$getChMsgNotif = SELECT_LOOP_QUERY("*,count(sender_id) as total","tbl_convo_notif","receiver_id = '$user_id' AND convo_id = -1 GROUP BY sender_id, convo_id");
$countConvo = 0;
foreach($getChMsgNotif as $msgList){

    $c[$msgList[convo_id]]['count_dm_convo'] += $msgList[total];
    $c[$msgList[convo_id]]['rcp'][$msgList[sender_id]] = $msgList[total];

}

$last_line_counter = $c;
if($last_line_counter != $_SESSION['chat']['last_dm_counter']){
    echo "data:", json_encode($c) , "\n\n";
    $_SESSION['chat']['last_dm_counter'] = $last_line_counter;
}

//echo "data:", json_encode($c) , "\n\n";
// echo "retry: 15000\n";
ob_flush();
flush();
?>