<?php
include '../core/config.php';
$user_id = $_SESSION["system"]["userid_"];
$desc = clean($_POST['desc']);

$data = array(
    'created_by' => $user_id,
    'date_created' => date("Y-m-d H:i:s"),
    'description' => $desc
);

$last_inserted_id = INSERT_QUERY("tbl_announcement", $data, "Y");
if($last_inserted_id > 0){
    $doNotInclude = "1,18,".$user_id;
    $loopUsers = SELECT_LOOP_QUERY("*","tbl_users","user_id NOT IN($doNotInclude)");
    if(count($loopUsers) > 0){
        foreach($loopUsers as $userList){
            $notif_data = array(
                'an_id' => $last_inserted_id,
                'sender_id' => $user_id,
                'receiver_id' => $userList[user_id],
                'date_notif' => date("Y-m-d H:i:s")
            );
           INSERT_QUERY("tbl_announcement_notif", $notif_data);
        }
    }
    
    echo 1;
}else{
    echo 0;
}