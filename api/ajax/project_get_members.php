<?php
include '../core/config.php';
$proj_name = $_POST['proj_name'];
$projCode = $_POST['projCode'];
$proj_stats = $_POST['proj_stats'];
?>
<div class="col-12 pb-0">
    <div class="card mb-3">
        <div class="card-header border-0 pb-0">
            <h5 class="mb-0">Groups with access to <b><?=$proj_name?></b></h5>
        </div>
        <div class="card-body border-0">
            <ul class="list-group list-group-flush list">
                <?php
                    $loop_grp = getProjectGroupMember($projCode);
                    if(count($loop_grp) < 1){
                        echo "invite member to this project";
                    }else{
                        foreach($loop_grp as $grpList){
                ?>
                    <li class="list-group-item chat-link-people px-0">
                        <div class="row align-items-center">
                            <div class='col-2 pr-0' style="text-align: center;">
                                <a href='#' class='avatar rounded-circle' style='width: 35px; height: 35px;margin-top: 7px;'>
                                    <?=getGroupAvatar($grpList["TeamCode"])?>
                                </a>
                            </div>
                            <div class="col pl-2">
                                <h5 class="text-muted mb-0"><?=$grpList["TeamName"]?></h5>
                                <small class="text-muted"><?=$grpList["TeamCode"]?></small>
                            </div>
                            <?php 
                                if($proj_stats == 0){
                            ?>
                            <div class="col-2">
                                <div style="align-items: baseline;justify-content: flex-end;display: flex;"><span class="badge badge-danger"></span><a href="#" class="btn btn-link btn-sm" style="color: red;"><i class="far fa-trash-alt" onclick="removeProjectGroup('<?=$grpList[TeamCode]?>')"></i></a></div>
                            </div>
                            <?php } ?>
                        </div>
                    </li>
                <?php } } ?>
                
            </ul>
        </div>
    </div>
</div>
<div class="col-12 pt-0">
    <div class="card">
        <div class="card-header border-0 pb-0">
            <h5 class="mb-0">Members of <b><?=$proj_name?></b></h5>
        </div>
        <div class="card-body border-0">
            <ul class="nav nav-sm flex-column msg_chat_scroll mb-1">
                <div class="msg_chat_scroll mb-1" style="max-height: 300px;padding-right: 10px;">
                <?php
                    $loop_mem = getProjectMember($projCode);
                    if(count($loop_mem) < 1){
                        echo "invite member to this project";
                    }else{
                        foreach($loop_mem as $memList){
                            $user_avatar = getUserAvatar($memList[user_id]);
                ?>
                    <li class='nav-item chat-link-people' style='border: 0px;'>
                        <div class='row'>
                            <div class='col-2 pr-0' style="text-align: center;">
                                <a href='#' class='avatar rounded-circle' style='width: 35px; height: 35px;margin-top: 7px;'>
                                    <img src='<?=$user_avatar?>' style='width: 100%;height: 100%;object-fit: cover;' class='rounded-circle' data-toggle='tooltip' data-placement='left'>
                                </a>
                            </div>
                            <div class='col-10 pl-3 align-items-center pr-0' style='display: flex;width: 100px;'>
                                <h4 class='text-muted mb-0' style='font-family: myFirstFont;font-weight: 400;color: #000 !important;cursor: pointer;text-overflow: ellipsis;width: calc(100% - 33px);white-space: nowrap;overflow: hidden;'><?=$memList["memberName"]?></h4>
                                <?php 
                                    if($proj_stats == 0){
                                ?>
                                    <span class='sidenav-mini-icon'><i class="far fa-trash-alt" onclick="removeProjectMember('<?=$memList[user_id]?>')" style="cursor: pointer;color: red;"></i></span>
                                <?php } ?>
                            </div>
                        </div>
                    </li>
                <?php } } ?>
                </div>
            </ul>
        </div>
    </div>
</div>