<?php
include '../core/config.php';
$user_id = $_SESSION["system"]["userid_"];
$response = array();

$getProject = SELECT_LOOP_QUERY("project_id AS id, projectCode, projectName, projectDescription, status", "tbl_project", "proj_pm = '$user_id' ORDER BY projectName ASC");
if(count($getProject) > 0){
    foreach($getProject as $personal_list){
        $data = array(
            'id'                    => $personal_list[id],
            'projectCode'           => clean($personal_list["projectCode"]),
            'projectName'           => clean($personal_list["projectName"]),
            'projectDescription'    => clean($personal_list["projectDescription"]),
            'status'                => $personal_list[status],
            'projectAvName'         => getAllFirstLetter(clean($personal_list["projectName"]))
        );
        array_push($response,$data);
    }
}
echo json_encode($response);