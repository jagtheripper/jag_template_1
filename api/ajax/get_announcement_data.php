<?php
include '../core/config.php';
$date = date("Y-m-d");
$user_id = $_SESSION["system"]["userid_"];

$loop_an = SELECT_LOOP_QUERY("*","tbl_announcement","DATEDIFF('$date', date_created) <= 4 ORDER BY an_id DESC");
if(count($loop_an) > 0){
    foreach($loop_an as $anList){
        $user_avatar = getUserAvatar($anList[created_by]);

        $msg_options = ($user_id == $anList[created_by])?"<div class='show-on-msg-hover' style='position: absolute;top: 3px;right: 8px;cursor: pointer;'><span class='badge badge-pill badge-default upload-media-icon' style='color: #48536f;border: 0px solid #fff;padding: 8px;margin: 2px;background: #e6e6e6;' title='edit' onclick='edit_an_notif(".$anList[an_id].",\"".clean($anList["description"])."\")'><i class='fa fa-pen' style='font-size: 12px;'></i></span><span class='badge badge-pill badge-default upload-media-icon' style='color: #48536f;border: 0px solid #fff;padding: 8px;margin: 2px;background: #e6e6e6;' title='delete' onclick='delete_an_notif(".$anList[an_id].")'><i class='far fa-trash-alt' style='font-size: 12px;color: red;'></i></span></div>":"";

        $data .= "<li class='list-group-item pl-0 msg_hover' style='border: 0px;border-bottom: 1px solid #ded9d9;'><div class='row'><div class='col-auto pr-0 align-items-start' style='padding-top: 3px;'><img src='".$user_avatar."' style='width: 40px; height: 40px;object-fit: cover;' class='avatar rounded-circle' data-toggle='tooltip' data-placement='left'></div><div class='col pl-2 align-items-center'>
        <div style='text-align: start;padding: 0px;'>
            <div class='row' style='margin: 0px;'>
                <div class='col-12' style='display: flex;width: 100px;padding: 0px;'>
                    <div style='width: 84%;'>
                        <h3 class='text-muted mb-0' style='font-family: myFirstFont;font-weight: 400;color: #000 !important;cursor: pointer;text-overflow: ellipsis;width:-webkit-fill-available;white-space: nowrap;overflow: hidden;' id='channel_name'>".getUserName($anList[created_by])."</h3>
                    </div>
                    ".$msg_options."
                </div>
                <div class='col-12 pl-0' style='line-height: 7px;'><small class='text-muted' style='font-size: 71% !important;'>".date("m/d/Y h:i A", strtotime($anList[date_created]))."</small></div>
            </div>
        </div>
        <p style='font-size: 1rem;font-family: myFirstFont;word-break: break-word;white-space: pre-wrap;color: #4e4e4e; margin-bottom: 5px;' id='msg_content_box'>".$anList["description"]."</p></div></div></li>";
    }
}

echo $data;