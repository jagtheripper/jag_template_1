<?php
include '../core/config.php';
$userid = $_SESSION['system']['userid_'];

$loop_user_groups = SELECT_LOOP_QUERY("*","tbl_team","created_by = '$userid'");
if(count($loop_user_groups) > 0){    
    foreach($loop_user_groups as $mygroupList){
        $group_avatar_ = ($mygroupList["slug"] != "")?MYGROUP_AVATAR_BASEPATH.clean($mygroupList["slug"]):MYGROUP_AVATAR_BASEPATH."default_group_avatar.jpg";
        $data .= '<li class="list-group-item chat-link-people px-0 py-1 mt-2">
        <div class="row align-items-center">
            <div class="col-2 pr-0" style="text-align: center;">
                <a href="#" class="avatar rounded-circle" style="width: 40px;height: 40px;">'.getGroupAvatar($mygroupList["teamCode"]).'</a>
            </div>
            <div class="col pl-2">
                <h5 class="text-muted mb-0">'.clean($mygroupList["teamName"]).'</h5>
                <small class="text-muted">'.clean($mygroupList["teamCode"]).'</small>
            </div>
            <div class="col-3">
                <div style="align-items: baseline;justify-content: flex-end;display: flex;"><span class="badge badge-danger"></span><a href="#" class="btn btn-link btn-sm" title="edit" onclick="groupSettings(\''.clean($mygroupList["teamCode"]).'\', \''.clean($mygroupList["teamName"]).'\', \''.$group_avatar_.'\')"><i class="far fa-edit"></i></a></div>
            </div>
        </div>
    </li>';
    }
}else{
    $data .= '<li class="list-group-item px-0">no data available</li>';
}

echo $data;