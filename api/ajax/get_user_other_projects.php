<?php
include '../core/config.php';
$user_id = $_SESSION["system"]["userid_"];
$response = array();

$project = new Project();
$getProject = SELECT_LOOP_QUERY("t1.CODE as project_code","(SELECT pm.projectCode AS CODE FROM tbl_project_member AS pm, tbl_team_member AS tm WHERE pm.teamCode = tm.teamCode AND tm.user_id = '$user_id' AND pm.type = 1 GROUP BY pm.projectCode UNION ALL SELECT projectCode AS CODE FROM tbl_project_member", "user_id = '$user_id' AND TYPE = 0 UNION ALL SELECT prj.projectCode as CODE FROM tbl_project as prj WHERE prj.proj_pm = '$user_id' AND prj.status = 0) AS t1 GROUP BY t1.CODE");
if(count($getProject) > 0){
    foreach($getProject as $personal_list){
        $projData = $project->getDetail($personal_list['project_code']);
        if($projData[proj_pm] != $user_id){
            $data = array(
                'id'                    => $projData[project_id],
                'projectCode'           => clean($projData["projectCode"]),
                'projectName'           => clean($projData["projectName"]),
                'projectDescription'    => clean($projData["projectDescription"]),
                'status'                => $projData[status],
                'projectAvName'         => getAllFirstLetter(clean($projData["projectName"]))
            );
            array_push($response,$data);
        }
    }
}
echo json_encode($response);