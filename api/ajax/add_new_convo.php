<?php
include '../core/config.php';
$user_id = $_SESSION["system"]["userid_"];
$userName = clean(getUserName($user_id));
$data_q = array(
    'created_by' => $user_id,
    'date_created' => date("Y-m-d H:i:s"),
    'convo_name' => "New Convo"
);
$in_id = INSERT_QUERY("tbl_convo",$data_q,"Y");
if($in_id){
    // INSERT TO MEMEBERS
    $data_m = array(
        'convo_id' => $in_id,
        'member_id' => $user_id,
        'date_added' => date("Y-m-d H:i:s"),
        'convo_role' => 1,
        'nickname' => $userName
    );
    $reslt = INSERT_QUERY("tbl_convo_member",$data_m);

    if($reslt){
        $getConvoData = SELECT_QUERY("*","tbl_convo","convo_id = '$in_id'");
        $convoBadge = "convo_badge_".$getConvoData[convo_id];
        $data = '<div class="col" title="'.$getConvoData[convo_name].'" style="transition: 	all 0s ease 0s;"><a href="#" onclick="chat_session_updater(\''.$getConvoData[convo_id].'\',\'convo\')" class="avatar rounded-circle mb-0 mt-1" style="width: 50px;height: 50px;">'.getConvoAvatar($getConvoData[convo_id]).'<div style="position: absolute;margin-top: 12px;margin-left: 13px;"><span class="badge badge-md badge-circle badge-floating badge-danger border-white" style="width: 27px;height: 27px;border: 3px solid;" id="'.$convoBadge.'"></span></div></a></div>';
    }
}

echo $data;