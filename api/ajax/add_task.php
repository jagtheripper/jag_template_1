<?php
include '../core/config.php';
$taskDueDate = $_POST['due_date'];
$user_id = $_SESSION["system"]["userid_"];
$taskDescription = $_POST['taskDescription'];
$status = 0;
$priority_stats = $_POST['priority_status'];
$projectCode = $_POST['projectCode'];
$date_mins = date('is');
$projectClass = new Project();
$randCode = strtoupper(RAND_GEN().$date_mins);
$data = array(
    'projectCode' => $projectCode,
    'taskDescription' => $taskDescription,
    'taskDueDate' => $taskDueDate,
    'taskCreateDate' => date("Y-m-d H:i:s"),
    'status' => $status,
    'priority_stats' => $priority_stats,
    'task_code' => $randCode
);
$taskID = INSERT_QUERY("tbl_task", $data, "Y");
if($taskID){
    $data = array(
        'projectCode' => $projectCode,
        'task_id' => $taskID,
        'user_id' => $user_id
    );
    $res = INSERT_QUERY("tbl_task_member", $data);
    echo $res;
}else{
    echo 0;
}