<?php

if($page == 'home'){	
	require_once('views/dashboard.php');
}else if($page == 'myproject'){	
	require_once('views/my_project.php');
}else if($page == 'myproject_settings'){	
	require_once('views/project_settings.php');
}else if($page == 'user_settings'){	
	require_once('views/user_settings.php');
}else{
	if(!empty($page) or $page != $page){
		require_once('views/error/error.php');
	}else{
		require_once('views/dashboard.php');
	}
}
