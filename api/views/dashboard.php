<!-- BREADCRUMBS -->
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
        <div class="row align-items-center py-4">
            <!-- <div class="col-lg-12 col-7">
                <nav aria-label="breadcrumb" class="float-right d-none d-md-inline-block ml-md-4">
                    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                    <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Default</li>
                    </ol>
                </nav>
            </div> -->
        </div>
        </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-4">
            <div class="card">
                <!-- Card header -->
                <div class="card-header border-0">
                    <h3 class="mb-0"><i class="fas fa-info-circle text-info"></i> Activity</h3>
                </div>
                <div class="card-body border-0">
                    <div class="row msg_chat_scroll" style="max-height: 500px;">
                        <div class="col">
                            <div class="timeline timeline-one-side" data-timeline-content="axis" data-timeline-axis-style="dashed" id="user_activity_bin">
                             
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card">
                <!-- Card header -->
                <div class="card-header border-0 pb-0">
                    <h3 class="mb-0"><i class="fas fa-bullhorn text-danger"></i> Announcement</h3>
                    <div id="mark_an_notif_as_read"></div>
                </div>
                <div class="card-body border-0 pt-0">
                    <div class="row">
                        <div class="col pb-3 msg_chat_scroll" style="background: transparent;max-height: 500px;" id="announcement_bin">
                            
                            <ul class="list-group list-group-flush list pt-3" id="announcement_content"></ul>
                            
                        </div>
                    </div>
                </div>
                <div style="position: absolute;top: 2px;right: 2px;"><span class="badge badge-pill badge-danger" style="color: #fff;border: 3px solid #fff;padding-top: 6px;" id="announcement_badge"><?=total_announcement_counter()?></span></div>
            </div>
        </div>
        <div class="col-4">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0 pb-0">
                        <h3 class="mb-0"><i class="far fa-user text-warning"></i> Welcome back, <?=getUserName($_SESSION["system"]["userid_"])?></h3>
                    </div>
                    <div class="card-body border-0">
                        <div class="row" style='font-size: 14px;'>
                            <div class="col-12">
                                <a href="index.php?page=user_settings" class="btn btn-primary btn-sm">Profile</a>
                                <?php
                                    if(in_array($_logID,array(18,19))){
                                ?>
                                    <a href="#" class="btn btn-success btn-sm" onclick="addNewAnnouncement()">Announcement</a>
                                <?php } ?>
                                <a href="#" class="btn btn-danger btn-sm" onclick="logout()">Sign out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0 pb-0">
                        <h3 class="mb-0">Projects</h3>
                    </div>
                    <div class="card-body border-0 pt-1">
                        <div class="row" style='font-size: 14px;'>
                            <div class="col-12">
                                <ul class="nav nav-tabs-code" id="form-controls-tab" role="tablist" style="font-size: 14px;">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="personal-tab" data-toggle="tab" href="#personal" role="tab" aria-controls="personal" aria-selected="true" style="padding: 5px;">Personal</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="others-tab" data-toggle="tab" href="#others" role="tab" aria-controls="others" aria-selected="false" style="padding: 5px;">Others</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div id="personal" class="tab-pane tab-example-result fade show active" role="tabpanel" aria-labelledby="personal-tab">
                                        <ul class="list-group list-group-flush list" id="personal_project_bin">
                                            
                                        </ul>
                                    </div>
                                    <div id="others" class="tab-pane tab-example-result fade" role="tabpanel" aria-labelledby="others-tab">
                                        <ul class="list-group list-group-flush list" id="personal_others_bin">
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0 pb-0">
                        <h3 class="mb-0">About Current Version</h3>
                    </div>
                    <div class="card-body border-0">
                        <div class="row" style='font-size: 14px;'>
                            <div class="col-12">
                                <a href="#" class="btn btn-primary btn-sm" onclick="openChangeLog()">What's New?</a>
                                <a href="#" class="btn btn-warning btn-sm" onclick="openFeedBack()">Feedback</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<?php include_once 'views/modals/modal_add_announcement.php';?>
<script>
    $(document).ready(function() {
        loadUserPersonalProjects();
        loadUserOtherProjects();
        loadUserActivities();
        getAnnouncement();
    });

    function loadUserPersonalProjects() {
        $.post("ajax/get_user_personal_projects.php",{
        },function(data){
            var user_pp_list;
            var userPersonalCloth = '';
            var user_personal_peroj = JSON.parse(data);
            if(user_personal_peroj.length > 0){
                for (var pp = 0; pp < user_personal_peroj.length; ++pp) {
                    user_pp_list = user_personal_peroj[pp];
                    var pp_stats = (parseInt(user_pp_list.status) == 0)?'<span style="color: green;">Active</span>':'<span style="color: #b57809;">Closed</span>';

                    var pp_avName = user_pp_list.projectAvName;
                    var pp_img = (1 != 1)?'<img src="../assets/user_avatar/2-200811084235.jpg" style="width: 40px;height: 40px;object-fit: cover;" class="avatar rounded-circle" title="'+user_pp_list.projectName+'">':'<div style="width: 40px;height: 40px;object-fit: cover;cursor: pointer;" class="avatar rounded-circle" title="'+user_pp_list.projectName+'">'+pp_avName.substr(0, 2)+'</div>';

                    // <div class="row align-items-center"><div class="col-auto pr-0"><a href="#" class="avatar rounded-circle" style="color: #233dd2;width: 40px;height: 40px;"><i class="ni ni-folder-17 text-link"></i></a></div>

                    userPersonalCloth += '<li class="list-group-item chat-link-people px-0 pb-1 pt-1" onclick=\'window.location="index.php?page=myproject&q='+user_pp_list.projectCode+'"\' style="cursor: pointer;padding-left: 5px !important;display: flex;flex-direction: row;padding-top: 8px !important;">'+pp_img+'<div class="col pl-2"><h5 class="mb-0">'+user_pp_list.projectName+'</h5><small class="text-muted" style="color: #3d4a5a !important;">'+user_pp_list.projectCode+'<label style="color: #606060;"> &mdash; '+pp_stats+'</label></small></div></div></li>';
                }
            }else{
                userPersonalCloth += '<span style="padding: 6px;"><i>no personal project found...</i></span>';
            }
            $("#personal_project_bin").html(userPersonalCloth);
        });
    }

    function loadUserOtherProjects() {
        $.post("ajax/get_user_other_projects.php",{
        },function(data){
            var user_oo_list;
            var userOtherCloth = '';
            var user_other_proj = JSON.parse(data);
            if(user_other_proj.length > 0){
                for (var oo = 0; oo < user_other_proj.length; ++oo) {
                    user_oo_list = user_other_proj[oo];
                    var oo_stats = (parseInt(user_oo_list.status) == 0)?'<span style="color: green;">Active</span>':'<span style="color: #b57809;">Closed</span>';

                    var oo_avName = user_oo_list.projectAvName;
                    var oo_img = (1 != 1)?'<img src="../assets/user_avatar/2-200811084235.jpg" style="width: 40px;height: 40px;object-fit: cover;" class="avatar rounded-circle" title="'+user_oo_list.projectName+'">':'<div style="width: 40px;height: 40px;object-fit: cover;cursor: pointer;" class="avatar rounded-circle" title="'+user_oo_list.projectName+'">'+oo_avName.substr(0, 2)+'</div>';

                    // <div class="row align-items-center"><div class="col-auto pr-0"><a href="#" class="avatar rounded-circle" style="color: #233dd2;width: 40px;height: 40px;"><i class="ni ni-folder-17 text-link"></i></a></div>

                    userOtherCloth += '<li class="list-group-item chat-link-people px-0 pb-1 pt-1" onclick=\'window.location="index.php?page=myproject&q='+user_oo_list.projectCode+'"\' style="cursor: pointer;padding-left: 5px !important;display: flex;flex-direction: row;padding-top: 8px !important;">'+oo_img+'<div class="col pl-2"><h5 class="mb-0">'+user_oo_list.projectName+'</h5><small class="text-muted" style="color: #3d4a5a !important;">'+user_oo_list.projectCode+'<label style="color: #606060;"> &mdash; '+oo_stats+'</label></small></div></div></li>';
                }
            }else{
                userOtherCloth += '<span style="padding: 6px;"><i>no other project found...</i></span>';
            }
            $("#personal_others_bin").html(userOtherCloth);
        });
    }

    function loadUserActivities() {
        $.post("ajax/get_user_activities.php",{
        },function(data){
            var user_act_list;
            var userActivityCloth = '';
            var user_activity_data = JSON.parse(data);
            for (var ua = 0; ua < user_activity_data.length; ++ua) {
                user_act_list = user_activity_data[ua];
                userActivityCloth += '<div class="timeline-block"><span class="timeline-step badge-success"><i class="ni ni-bell-55"></i></span><div class="timeline-content"><h4 class=" mt-0 mb-0">'+user_act_list.module+' <small class="text-muted font-weight-bold">'+user_act_list.date+'</small></h4><p class=" text-sm mt-0 mb-0">'+user_act_list.log+'</p><div class="mt-0"><span class="badge badge-pill badge-success" style="padding-top: 4px !important;">'+user_act_list.project_name+'</span></div></div></div>';
            }
            $("#user_activity_bin").html(userActivityCloth);
        });
    }

    function getAnnouncement() {
        $.post("ajax/get_announcement_data.php",{
        },function(data){
            checkiFHasNotif();
            $("#announcement_content").html(data);
        });
    }
    
    function checkiFHasNotif() {
        var an_counter = $("#announcement_badge").html();
        if(an_counter * 1 > 0){
            $("#mark_an_notif_as_read").html(notif_btn_mark);
        }else{
            $("#mark_an_notif_as_read").html("");
        }
    }

    function addNewAnnouncement() {
        $("#modal-add-announcement").modal('show');
    }

    function saveNewAnnouncement(){
        var desc = $("#an_description_input").val();
        $.post("ajax/post_announcement.php",{
            desc: desc
        },function(data){
            if(data == 1){
                getAnnouncement();
                $("#an_description_input").val("");
                $("#modal-add-announcement").modal('hide');
            }else{
                alertMe("Error in posting announcement!", "danger");
            }
        });
    }

    function markAnnouncementAsRead() {
        $.post("ajax/mark_announcement_as_read.php",{
        },function(data){
            getAnnouncement();
            $("#mark_an_notif_as_read").html("");
        });
    }
</script>