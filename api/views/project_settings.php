<?php
$projectcode = clean($_REQUEST["q"]);
$projdata = $project->getDetail($projectcode);
$userID = $_SESSION["system"]["userid_"];
?>
<!-- BREADCRUMBS -->
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
        <div class="row align-items-center py-4">
            <!-- <div class="col-lg-12 col-7">
                <nav aria-label="breadcrumb" class="float-right d-none d-md-inline-block ml-md-4">
                    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                    <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Default</li>
                    </ol>
                </nav>
            </div> -->
        </div>
        </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-8">
            <div class="card">
                <!-- Card header -->
                <div class="card-header border-0">
                    <h3 class="mb-0">Update your project information</h3>
                </div>
                <div class="card-body border-0">
                    <div class="row">
                        <div class="col-8">
                            <div class="form-group">
                                <label for="ps_project_name">Project name</label>
                                <input type="text" id="ps_project_name" class="form-control form-control-alternative" placeholder="My awesome project" value="<?=$projdata["projectName"]?>">
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label for="ps_project_code">Project ID</label>
                                <input type="text" id="ps_project_code" class="form-control form-control-alternative" placeholder="My awesome project" value="<?=$projdata["projectCode"]?>" disabled>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <label for="ps_project_code">Status</label>
                                <h2><?=($projdata["status"] == 1)?"<span style='color: orange !important;'>Closed</span>":"<span style='color: green !important;'>Active</span>";?></h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="project_desc">Project description (optional)</label>
                                <textarea id="project_desc" rows="3" class="form-control form-control-alternative" placeholder="My awesome project"><?=$projdata["projectDescription"]?></textarea>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col" style="display: flex;flex-direction: row;justify-content: space-between;">
                            <?php 
                                if($projdata["status"] != 1){
                            ?>
                                <a href="#" class="btn btn-success btn-md" id="save_proj_btn" onclick="updateProject()">Save changes</a>
                                <a href="#" class="btn btn-warning btn-md" id="close_proj_btn" onclick="closeProject()">Close project</a>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <hr>
                        </div>
                        <div class="col-12">
                                <h3>Remove project</h3>
                                <p class="text-muted">Removing the project will delete all related resources including task, linked members etc. <br><b>Removed projects cannot be restored!</b></p>
                                <a href="#" class="btn btn-danger btn-md" id="remove_proj_btn" onclick="removeProject()">Remove project</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0 pb-0">
                        <h3 class="mb-0">Members</h3>
                    </div>
                    <div class="card-body border-0">
                        <div class="row mb-2" style='font-size: 14px;'>
                            <?php 
                                if($projdata["status"] != 1){
                            ?>
                            <div class="col-12">
                                <a href="#" onclick="inviteMemberToProj()" class="btn btn-primary btn-sm">
                                    <i class="fas fa-plus"></i>
                                    Invite member
                                </a>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="row" style='font-size: 14px;' id="members_data_list">
                            
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<div style="position: absolute;top: 13px;left: 30px;">
    <a href="index.php?page=myproject&q=<?=$_REQUEST['q']?>" style="color: #fff;"><i class="fas fa-arrow-left"></i> Return to task board</a>
</div>
<?php include_once 'views/modals/project_invite_member.php';?>
<script>
    $(document).ready(function() {
        displayProjectMemebers();
    });

    function updateProject() {
        var code = $("#ps_project_code").val();
        var name = $("#ps_project_name").val();
        var description = $("#project_desc").val();
        $.post("ajax/save_project.php",{
            name: name,
            description: description,
            code: code
        },function(data,status){
            if(data == 1){
                alertMe('Changes saved.', 'success');
            }else{
                alertMe('Error in saving your changes.', 'danger');
            }
            location.reload();
        });
    }

    function removeProjectMember(id) {
        var projectCode = '<?=$projectcode?>';
        var rslt = confirm("Are you sure you want to remove this member?");
        if(rslt){
            $.post("ajax/delete_project_member.php",{
                id: id,
                projCode: projectCode
            },function(data){
                displayProjectMemebers();
                // if(data == 1){
                //     displayProjectMemebers();
                // }else{
                //     alert('Error in saving your changes.');
                // }
            });
        }
    }

    function removeProjectGroup(code) {
        var projectCode = '<?=$projectcode?>';
        var rslt = confirm("Are you sure you want to remove this team?");
        if(rslt){
            $.post("ajax/delete_project_group.php",{
                team_code: code,
                projCode: projectCode
            },function(data){
                displayProjectMemebers();
                // if(data == 1){
                //     displayProjectMemebers();
                // }else{
                //     alert('Error in saving your changes.');
                // }
            });
        }
    }

    function inviteMemberToProj(){
        $("#proj-search-people").val('');
        projSearchUser();

        $("#modal-add-member-to-project").modal({
            show: true,
            backdrop: 'static',
            keyboard: false
        });
    }

    function displayProjectMemebers() {
        var proj_name = '<?=$projdata["projectName"]?>';
        var proj_stats = '<?=$projdata["status"]?>';
        var projectCode = '<?=$projectcode?>';
        $.post("ajax/project_get_members.php",{
            proj_name: proj_name,
            projCode: projectCode,
            proj_stats: proj_stats
        },function(data){
           $("#members_data_list").html(data);
        });    
    }

    function removeProject() {
        var projCode = $("#ps_project_code").val();
        var rslt = confirm("Are you sure you want to remove this project?");
        if(rslt){
            $.post("ajax/remove_project.php",{
                projCode: projCode
            },function(data){
            window.location = 'index.php?page=home';
            });
        }
    }

    function closeProject() {
        var projectCode = '<?=$projectcode?>';
        var finish_rslt = confirm("Are you sure you want to finish this project?");
        if(finish_rslt){
            $.post("ajax/finish_project.php",{
                projectCode: projectCode
            },function(data){
                window.location = 'index.php?page=myproject_settings&q='+projectCode;
            });
        }
    }
</script>