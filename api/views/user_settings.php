<?php
$user_id = $_SESSION['system']['userid_'];
$getUserData = SELECT_QUERY("*","tbl_users","user_id = '$user_id'");
$userAvatar = getUserAvatar($user_id);
?>
<style>
.bg-gradient-default {
    background: linear-gradient(87deg, #369886 0, #175f82 100%) !important;
}

    .avatar-upload {
        position: unset;
        max-width: 205px;
        margin: 0px auto;
    }

    .avatar-upload .avatar-edit {
        position: absolute;
        right: 83px;
        z-index: 100;
        top: 0px;
    }

    .avatar-upload .avatar-edit input {
        display: none;
    }

    .avatar-upload .avatar-edit input + label {
        display: inline-block;
        width: 34px;
        height: 34px;
        margin-bottom: 0;
        border-radius: 100%;
        background: #FFFFFF;
        border: 1px solid transparent;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
        cursor: pointer;
        font-weight: normal;
        transition: all 0.2s ease-in-out;
    }

    .avatar-upload .avatar-edit input + label:hover {
        background: #f1f1f1;
        border-color: #d6d6d6;
    }

    .avatar-upload-i {
        color: #757575;
        position: absolute;
        top: 10px;
        left: 0;
        right: 0;
        text-align: center;
        margin: auto;
    }
</style>
<!-- BREADCRUMBS -->
<div class="header pb-6 d-flex align-items-center" style="min-height: 332px; background-size: cover; background-position: center top;">
    <!-- Mask -->
    <span class="mask bg-gradient-default opacity-8"></span>
    <!-- Header container -->
    <div class="container-fluid">
        <div class="col pl-0">
            <h1 class="display-2 text-white mb-0"><?=clean($getUserData["name"])?></h1>
            <p class="text-white mt-0 mb-3">
                <b><?=clean($getUserData["email"])?></b><br>
                <span><?=clean($getUserData["bio"])?></span>
            </p>
            <a href="#" class="btn btn-danger btn-sm" onclick="logout()">Sign out</a>
        </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-8">
            <div class="card">
                <!-- Card header -->
                <div class="card-header border-0 pb-0">
                    <h3 class="mb-0">Profile Info</h3>
                </div>
                <div class="card-body border-0">
                   <div class="row">
                        <div class="col-4">
                            <div class="col-12" id="user_avatar_container_" style="text-align: center;width: 100%;height: 230px;">
                                <img src="<?=$userAvatar?>" style="width: 185px;height: 185px;object-fit: cover;" class="rounded-circle" id="us_mid_cur_avatar">
                            </div>
                            <div class="avatar-upload">
                                <div class="avatar-edit" style="">
                                    <input type='file' name="ing_imageUpload" id="ing_imageUpload" accept=".png, .jpg, .jpeg"/>
                                    <label for="ing_imageUpload"><i class="fas fa-pen avatar-upload-i"></i></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-8">
                           <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="settings_user_group_name">Fullname</label>
                                    <div class="input-group input-group-merge">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                                        </div>
                                        <input type="text" id="pi_fullname" class="form-control" placeholder="set your fullname" value="<?=clean($getUserData["name"])?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="settings_user_group_name">Username</label>
                                    <div class="input-group input-group-merge">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-file"></i></span>
                                        </div>
                                        <input type="text" id="pi_username" class="form-control" placeholder="set your username" value="<?=clean($getUserData["username"])?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="settings_user_group_name">Email</label>
                                    <div class="input-group input-group-merge">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                                        </div>
                                        <input type="email" id="pi_email" class="form-control" placeholder="set your email" value="<?=clean($getUserData["email"])?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="settings_user_group_name">Password</label>
                                    <div class="input-group input-group-merge">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-eye"></i></span>
                                        </div>
                                        <input type="password" id="pi_password" class="form-control" placeholder="set your password">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="settings_user_group_name">Bio (optional)</label>
                                    <div class="input-group input-group-merge">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-list"></i></span>
                                        </div>
                                        <input type="text" id="pi_bio" class="form-control" placeholder="set your bio" value="<?=clean($getUserData["bio"])?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <label style="color: red;"><b>NOTE :</b> If password is blank, password will not be updated. If password is not blank, password will be updated. Be careful when clicking the "Save changes" button below.</label>
                                <div class="form-group float-right">
                                    <a href="#" class="btn btn-success btn-md" onclick="updateProfileInfo()"> Save changes</a>
                                </div>
                            </div>

                           </div>
                        </div>
                   </div>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0 pb-0">
                        <h3 class="mb-0">My Groups</h3>
                    </div>
                    <div class="card-body border-0">
                        <div class="row" style='font-size: 14px;'>
                            <div class="col-12">
                                <a href="#" class="btn btn-primary btn-sm" onclick="addNewGroup()"><i class="fas fa-plus"></i> Add new group</a>
                            </div>
                            <div class="col-12">
                                <ul class="list-group list-group-flush list msg_chat_scroll" id="mygroup_bin" style="max-height: 300px;padding-right: 15px;">
                                
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <!-- Card header -->
                    <div class="card-header border-0 pb-0">
                        <h3 class="mb-0">Notifications</h3>
                    </div>
                    <div class="card-body border-0">
                        <div class="row" style='font-size: 14px;'>
                            <div class="col-12">
                                <span class="badge badge-warning"> on dev...</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<?php include_once 'views/modals/user_groups_settings.php';?>
<script>
    $(document).ready(function() {
        getMyGroups();
    });

    function updateProfileInfo() {
        var pi_fullname = $("#pi_fullname").val();
        var pi_username = $("#pi_username").val();
        var pi_email = $("#pi_email").val();
        var pi_password = $("#pi_password").val();
        var pi_bio = $("#pi_bio").val();
        if(pi_fullname != "" || pi_username != "" || pi_email != "" || pi_bio != ""){
            $.post("ajax/update_profile_info.php",{
                pi_fullname: pi_fullname,
                pi_username: pi_username,
                pi_email: pi_email,
                pi_password: pi_password,
                pi_bio: pi_bio
            },function(data){
                if(data == 1){
                   location.reload();
                }else{
                    alertMe("Error saving information", 'danger');
                }
            });
        }else{
            alertMe("Some fields should not be blank!", 'warning');
        }
    }

    function groupSettings(teamcode, teamname, slug) {
        $("#settings_user_group_name").val(teamname);
        $("#settings_teamcode").val(teamcode);
        $("#my_group_avatar").attr('src', slug);
        displayGroupMembers();
        $("#modal-user-mygroups-settings").modal({
            show: true
        });
    }

    function addNewGroup() {
        var res = confirm("Are you sure you want to add a new group?");
        if(res){
            $.post("ajax/add_my_new_group.php",{
            },function(data){
                if(data == 1){
                    getMyGroups();
                    $("#modal-user-mygroups-settings").modal('hide');
                    alertMe("New Group created", 'success');
                }else{
                    alertMe("Error creating new group", 'danger');
                }
            });
        }
    }

    function getMyGroups() {
        $.post("ajax/user_settings_mygroups.php",{
        },function(data){
            $("#mygroup_bin").html(data);
        });
    }

    function updateGroupName() {
        var team_name = $("#settings_user_group_name").val();
        var team_code = $("#settings_teamcode").val();
        $.post("ajax/update_mygroup_settings.php",{
            team_name: team_name,
            team_code: team_code
        },function(data){
            if(data == 1){
                getMyGroups();
                $("#modal-user-mygroups-settings").modal('hide');
                alertMe("Group updated", 'success');
            }else{
                alertMe("Error updating group name!", 'danger');
            }
        });
    }

    $('#mygroup-search-people').on('keypress', function (e) {
        if(e.keyCode == 13){
            grpSearchUser();
        }
        $('#mygroup-search-people').focus();
    });

    function grpSearchUser() {
        var search_q = $("#mygroup-search-people").val();
        $.post("ajax/mygroup_search_people.php",{
            search_q: search_q
        },function(data){
            if(data != 1){
                $("#mygroup-search-result").html(data);
            }else{
                alertMe("Please enter a valid email address.","danger");
            }
        });
    }

    function invitePeopleToGroup(invited_id) {
        var team_code = $("#settings_teamcode").val();
        $.post("ajax/invite_people_to_mygroup.php",{
            invited_id: invited_id,
            team_code: team_code
        },function(data){
            if(data == 1){
                displayGroupMembers();
                $('#mygroup-search-people').val('');
                grpSearchUser();
                alertMe("Member added", 'success');
            }else if(data == 2){
                alertMe("same person exist in this group", 'warning');
            }else{
                alertMe("Error inviting human", 'danger');
            }
        });
    }

    function displayGroupMembers() {
        var team_code = $("#settings_teamcode").val();
        $.post("ajax/us_get_group_member.php",{
            team_code: team_code
        },function(data){
            $("#group_member_bin").html(data);
        });
    }

    function deleteGroupMember(id) {
        var team_code = $("#settings_teamcode").val();
        $.post("ajax/deleteGroupMember.php",{
            id: id,
            team_code: team_code
        },function(data){
            if(data == 1){
                displayGroupMembers();
                alertMe("Deleted member", 'success');
            }else{
                alertMe("Error deleting member", 'danger');
            }
        });
    }

// CROPPIE
    function us_readURL(input) {
        $("#crop_btn_upload").html('');
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                avatar_croppie.croppie('bind', {
                    url: e.target.result
                });
                $("#crop_btn_upload").html('<button type="button" class="btn btn-link" onclick="closeAvatarModal(\'ing_imageUpload\')">Cancel</button><button type="button" class="btn btn-success" onclick="uploadUserAvatar()" id="cropper_apply_crop">Apply</button>');
                $("#modal-crop-avatar").modal('show');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#ing_imageUpload").change(function() {
        us_readURL(this);
    });

    function uploadUserAvatar(){
        $("#cropper_apply_crop").html('Uploading...');
        avatar_croppie.croppie('result', {
            type: 'canvas',
		    size: 'viewport'
        }).then(function(response){
            $.post("ajax/upload_user_avatar.php",{
                image: response
            },function(data){
                alertMe(data, 'success');
                $("#us_mid_cur_avatar").attr('src', response);
                $("#cropper_apply_crop").html('Apply');
                $("#ing_imageUpload").val('');
                $("#modal-crop-avatar").modal('hide');
            });
        });
    }

</script>