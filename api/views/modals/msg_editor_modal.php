<div class="modal fade modal-black" id="modal-edit-message" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
<div class="modal-dialog modal- modal-dialog-centered modal-" role="document" style="width: 37%;">
        <div class="modal-content">
        	
            <div class="modal-header" style="padding-bottom: 0px;">
                <h6 class="modal-title" id="modal-title-default">Edit message</h6>
            </div>
            
            <div class="modal-body pb-0 pt-1">
                <div class="row">
                    <div class="col-12 mt-2">
                        <div class="form-group mb-0">
                            <textarea class="form-control msg_chat_scroll" id="msg_edit_txt" placeholder="text message" style="height: 249px;font-size: 14px;font-family: myFirstFont;white-space: pre-wrap;resize: none;color: #000;border: 1px solid #ccc !important;box-shadow: none !important;border-radius: 4px;" rows="1"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
                <span id="up_edit_btn"></span>
            </div>
            
        </div>
    </div>
</div>