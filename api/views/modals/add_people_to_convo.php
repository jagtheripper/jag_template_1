<div class="modal fade modal-black" id="modal-add-people-to-convo" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
<div class="modal-dialog modal- modal-dialog-centered modal-" role="document" style="width: 30%;">
        <div class="modal-content">
        	
            <div class="modal-header" style="padding-bottom: 0px;">
                <h6 class="modal-title" id="modal-title-default">Invite people to this convo</h6>
            </div>
            
            <div class="modal-body">
                <div class="row">

                    <div class="col">
                        <div class="form-group">
                            <input type="text" id="convo-search-people" class="form-control form-control-alternative" placeholder="type e-mail of user, hit enter to search" autocomplete="off">
                        </div>
                        <div class="row">
                            <div class="col msg_chat_scroll" style="overflow: auto; max-height: 200px; min-height: 60px;">
                                <ul class="list-group list-group-flush list my--3" id="search-result">

                                </ul>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Close</button>
            </div>
            
        </div>
    </div>
</div>
<script>
    function invitePeople(id) {
        $.post("ajax/chat_save_invite.php",{
            id: id
        },function(data){
            if(data == 1){
                getLinkPeople();
                alertMe('Person Added', 'success');
            }else if(data == 3){
                alertMe("Same person exist in the convo!", 'warning');
            }else{
                alertMe("Error saving invite", 'danger');
            }
        });
    }

    $('#convo-search-people').on('keypress', function (e) {
        if(e.keyCode == 13){
            searchUser();
        }
        $('#convo-search-people').focus();
    });

    function searchUser() {
        var search_q = $("#convo-search-people").val();
        $.post("ajax/chat_search_people.php",{
            search_q: search_q
        },function(data){
            if(data != 1){
                $("#search-result").html(data);
            }else{
                alertMe("Please enter a valid email address.","danger");
            }
        });
    }
</script>