<div class="modal fade" id="modal-crop-avatar" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
<div class="modal-dialog modal- modal-dialog-centered modal-" role="document" style="width: 37%;">
        <div class="modal-content">
        	
            <div class="modal-header" style="padding-bottom: 0px;">
                <h6 class="modal-title" id="modal-title-default">EDIT MEDIA</h6>
            </div>
            
            <div class="modal-body" style="overflow-y: auto; height: 385px;">
                <div class="row">
                    <div class="col-12" id="cropper_container" style="height: 230px;">
                                            
                    </div>
                </div>
            </div>
            
            <div class="modal-footer">
                <span id="crop_btn_upload"></span>
            </div>
            
        </div>
    </div>
</div>