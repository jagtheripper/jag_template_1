<div class="modal fade modal-black" id="modal-add-feedback" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
<div class="modal-dialog modal- modal-dialog-centered modal-" role="document" style="width: 43%;">
        <div class="modal-content">
        	
            <div class="modal-header" style="padding-bottom: 0px;">
                <h6 class="modal-title" id="modal-title-default">Feedback to the author</h6>
            </div>
            
            <div class="modal-body" style="overflow-y: auto; height: 375px;">
                <div class="row">
                    <div class="col-12 mb-1" style="">
                     <p class="text-muted pb-0 mb-0">Help alfather author make this ecosystem a better place to breathe in for you; for everyone.</p>
                     <small class="text-muted">See a bug? don't step! let me do the job for you. <i class="fa fa-coffee" aria-hidden="true"></i></small>
                    </div>
                    <div class="col mt-1" style="">
                        <div class="form-group">
                            <label for="fb_module">Module</label>
                            <select class="form-control" name="fb_module" id="fb_module">
                                <option value="">-- select a module --</option>
                                <option value="dashboard">Dashboard</option>
                                <option value="user_profile">User profile</option>
                                <option value="request_book">Request book</option>
                                <option value="remember_me_notes">Remember me notes</option>
                                <option value="chat">Chat</option>
                                <option value="project">Project</option>
                                <option value="projec_detail">Projec detail</option>
                                <option value="others">Others</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="fb_desc">Your thoughts ( <span class="text-muted">elaborate more</span> )</label>
                            <textarea class="form-control" id="fb_desc" rows="4" style="resize: none;"></textarea>
                        </div>
                    </div>
                    
                </div>
                
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" onclick="sendToAuthor()">Send to author</button>
            </div>
            
        </div>
    </div>
</div>
<script>
function sendToAuthor() {
    var fb_module = $("#fb_module").val();
    var fb_desc = $("#fb_desc").val();
    if(fb_module != "" && fb_desc != ""){
        $.post("ajax/save_feedback.php",{
            fb_module: fb_module,
            fb_desc: fb_desc
        },function(data){
            if(data == 1){
                alertMe("Thank for your thouhgts, this will help a lot. \n\n--duediewin", 'success');

                $("#fb_module").val('');
                $("#fb_desc").val('');
                $("#modal-add-feedback").modal('hide');
            }else{
                alertMe("Error sending to the author.", 'danger');
            }
        });
    }
}
</script>