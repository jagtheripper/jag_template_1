<div class="modal fade" id="modal-preview-media" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true" style="background: #000000ed;">
<div class="modal-dialog modal- modal-dialog-centered modal-" role="document" style="width: 70%;">
        <div class="modal-content">
            <div class="modal-body" style="padding: 0px;margin: 0px;">
                <div class="row">
                    <div class="col" style="text-align: left;padding: 0px;"><img src="" style="width: 100%;object-fit: contain;border-radius: 12px;" id="preview_media_container"></div>
                </div>
            </div>
            <div style="position: absolute; top: 10px; right: 0;">
                <button type="button" class="btn btn-link" data-dismiss="modal" style="background: #ffffffcf;padding: 4px;color: #000;">Close</button>
            </div>
            
        </div>
    </div>
</div>