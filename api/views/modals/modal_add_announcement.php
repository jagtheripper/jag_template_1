<div class="modal fade" id="modal-add-announcement" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
<div class="modal-dialog modal- modal-dialog-centered modal-" role="document" style="width: 30%;">
        <div class="modal-content">
        	
            <div class="modal-header" style="padding-bottom: 0px;">
                <h6 class="modal-title" id="modal-title-default">Create a new announcement</h6>
            </div>
            
            <div class="modal-body" style="overflow-y: auto; max-height: 310px;">
                <div class="row">

                    <div class="col" style="">
                        <div class="form-group">
                            <label for="an_description_input">Announcement description</label>
                            <textarea class="form-control form-control-alternative" id="an_description_input" rows="4" placeholder="your announcement statement here..." style="resize: none;"></textarea>
                        </div>
                    </div>
                    
                </div>
                
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="saveNewAnnouncement()">Post Announcement</button>
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>
            
        </div>
    </div>
</div>