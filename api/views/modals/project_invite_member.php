<div class="modal fade modal-black" id="modal-add-member-to-project" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
<div class="modal-dialog modal- modal-dialog-centered modal-" role="document" style="width: 30%;">
        <div class="modal-content">
        	
            <div class="modal-header" style="padding-bottom: 0px;">
                <h6 class="modal-title" id="modal-title-default">Invite member to Project <?=$projdata["projectName"]?></h6>
            </div>
            
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <center><small>&mdash; Invite by Group &mdash;</small></center>
                        <div class="form-group">
                            <select id="project-group-select" class="form-control form-control-alternative">
                                <option value="">-- select group --</option>
                                <?php 
                                    $loopgroup = getMyGroups($userID);
                                    if(count($loopgroup) > 0){
                                        foreach($loopgroup as $grplist){
                                ?>
                                    <option value="<?=$grplist['team_code']?>"><?=$grplist['team_name']?></option>
                                <?php } } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-success btn-sm float-right" data-dismiss="modal" onclick="inviteSelectedGroup()">invite selected group</button>
                        </div>
                    </div>
                    <div class="col-12 mt-1">
                        <center><small>&mdash; or &mdash;</small></center>
                        <div class="form-group">
                            <input type="text" id="proj-search-people" class="form-control form-control-alternative" placeholder="type e-mail of user, hit enter to search" autocomplete="off">
                        </div>
                        <div class="row">
                            <div class="col msg_chat_scroll" style="overflow: auto; max-height: 200px; min-height: 60px;">
                                <ul class="list-group list-group-flush list my--3" id="proj-search-result">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Close</button>
            </div>
            
        </div>
    </div>
</div>
<script>
    function invitePeopleToProject(id) {
        var projCode = "<?=$projectcode?>";
        $.post("ajax/proj_save_invite.php",{
            id: id,
            projCode: projCode
        },function(data){
            if(data == 1){
                displayProjectMemebers();
                $("#proj-search-people").val('');
                projSearchUser();
            }else{
                alertMe("Error saving invite", "danger");
            }
        });
    }

    function inviteSelectedGroup(){
        var selected_group = $("#project-group-select").val();
        var projCode = "<?=$projectcode?>";
        if(selected_group == ""){
            alertMe("please select a group to invite", "warning");
        }else{
            $.post("ajax/save_group_invite_in_project.php",{
                selected_group: selected_group,
                projCode: projCode
            },function(data){
                if(data == 1){
                    alertMe("Group Added to the project",  "success");
                }else if(data == 2){
                    alertMe("Group already exist in the project", "warning");
                }else{
                    alertMe("Error adding group to the project", "danger");
                }
                displayProjectMemebers();
            });
        }
    }

    $('#proj-search-people').on('keypress', function (e) {
        if(e.keyCode == 13){
            projSearchUser();
        }
        $('#proj-search-people').focus();
    });

    function projSearchUser() {
        var search_q = $("#proj-search-people").val();
        var project_code = "<?=$projectcode?>";
        $.post("ajax/proj_search_people.php",{
            search_q: search_q,
            project_code: project_code
        },function(data){
            if(data != 1){
                $("#proj-search-result").html(data);
            }else{
                alertMe("Please enter a valid email address.","danger");
            }
        });
    }
</script>