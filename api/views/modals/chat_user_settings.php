<div class="modal fade modal-black" id="modal-chat-user-settings" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
<div class="modal-dialog modal- modal-dialog-centered modal-" role="document" style="width: 30%;">
        <div class="modal-content">
        	
            <div class="modal-header" style="padding-bottom: 0px;">
                <h6 class="modal-title" id="chat_member_settings">Member settings</h6>
            </div>
            <div class="modal-body">
                <input type="hidden" id="user_memberid_hidden">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="settings_chat_nickname">Nickname</label>
                            <div class="input-group input-group-merge">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                                </div>
                                <input type="text" id="settings_chat_nickname" class="form-control" placeholder="set your nickname">
                            </div>
                        </div>

                        <div class="form-group" id="nickname_save_btn_bin">
                            <button type="button" class="btn btn-primary btn-sm" onclick="updateNewNickName()">Save changes</button>
                        </div>

                    </div>
                    <div class="col-12">
                        <div class="row" id="link_people_settings_bin">
                            
                        </div>
                    </div>
                </div>
                
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Close</button>
            </div>
            
        </div>
    </div>
</div>