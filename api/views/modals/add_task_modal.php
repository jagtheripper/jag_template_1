<div class="modal fade modal-black" id="modal-add-task" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
<div class="modal-dialog modal- modal-dialog-centered modal-" role="document" style="width: 30%;">
        <div class="modal-content">
        	
            <div class="modal-header" style="padding-bottom: 0px;">
                <h6 class="modal-title" id="modal-title-default">Create New Task</h6>
            </div>
            
            <div class="modal-body" style="overflow-y: auto; height: 430px;">
                <div class="row">

                    <div class="col" style="">
                        <div class="form-group">
                            <label for="task_date">Due date</label>
                            <input type="date" id="task_date" class="form-control form-control-alternative">
                        </div>
                        <div class="form-group">
                            <label for="task_status">Priority status</label>
                            <select name="task_status" id="task_status" class="form-control form-control-alternative">
                                <option value="0">Low</option>
                                <option value="1">Medium</option>
                                <option value="2">High</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="task_description">Task description</label>
                            <textarea class="form-control form-control-alternative" id="task_description" rows="4" placeholder="Description format" style="resize: none;"></textarea>
                        </div>
                    </div>
                    
                </div>
                
            </div>
            
            <div class="modal-footer">
                <button type="button" onclick="addNewTask()" class="btn btn-success">Create Task</button>
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>
            
        </div>
    </div>
</div>