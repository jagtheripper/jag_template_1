<div class="modal fade modal-black" id="modal-user-mygroups-settings" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
<div class="modal-dialog modal- modal-dialog-centered modal-" role="document" style="width: 50%;">
        <div class="modal-content">
        	
            <div class="modal-header" style="padding-bottom: 0px;">
                <h6 class="modal-title" id="chat_member_settings">My groups settings</h6>
            </div>
            <div class="modal-body pb-0 pt-2">
                <div class="row">
                    <div class="col-6">
                        <input type="hidden" id="user_memberteamcode_hidden">
                        <div class="row">
                            
                            <div class="col-6">
                                <label for="settings_user_group_name">Group Code</label>
                                <div class="form-group">
                                    <input type="text" id="settings_teamcode" class="form-control" disabled>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="settings_user_group_name">Group Name</label>
                                    <div class="input-group input-group-merge">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-users"></i></span>
                                        </div>
                                        <input type="text" id="settings_user_group_name" class="form-control" placeholder="set your group name">
                                    </div>
                                </div>

                                <div class="form-group" id="nickname_save_btn_bin">
                                    <button type="button" class="btn btn-primary btn-sm" onclick="updateGroupName()">Save changes</button>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-12" id="group_avatar_container" style="text-align: center;width: 100%;height: 230px;">
                                        <img src="" style="width: 185px;height: 185px;object-fit: cover;" class="rounded-circle" id="my_group_avatar">
                                    </div>
                                    <div class="avatar-upload">
                                        <div class="avatar-edit" style="">
                                            <input type='file' name="group_imageUpload" id="group_imageUpload" accept=".png, .jpg, .jpeg"/>
                                            <label for="group_imageUpload"><i class="fas fa-pen avatar-upload-i"></i></label>
                                        </div>
                                    </div>
                                    <!-- <div class="col-12" id="mygroup_avatar_container" style="height: 230px;"">
                                    
                                    </div>
                                    <div class="col-12 mt-3">
                                        <button class="btn btn-sm btn-primary" onclick="uploadMygAvatar()">Upload avatar</button>
                                    </div>

                                    
                                    <div class="avatar-upload">
                                        <div class="avatar-edit">
                                            <input type='file' name="myg_imageUpload" id="myg_imageUpload" accept=".png, .jpg, .jpeg"/>
                                            <label for="myg_imageUpload"><i class="fas fa-pen avatar-upload-i"></i></label>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 px-1">
                        <div class="row">
                            <div class="col-12">
                                <center><small>&mdash; invite member to group &mdash;</small></center>
                                <div class="form-group">
                                    <input type="text" id="mygroup-search-people" class="form-control form-control-alternative" placeholder="enter e-mail of user" autocomplete="off">
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <ul class="list-group list-group-flush list my--3 msg_chat_scroll" id="mygroup-search-result" style="max-height: 100px;">
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 pt-5">
                                <center><small>&mdash; Group Members &mdash;</small></center>
                                <ul class="list-group list-group-flush list msg_chat_scroll pr-3" id="group_member_bin" style="max-height: 260px;">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Close</button>
            </div>
            
        </div>
    </div>
</div>
<script>
// GROUP AVATAR CROPPIE
    function myg_readURL(input) {
        $("#crop_btn_upload").html('');
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e_myg) {
                avatar_croppie.croppie('bind', {
                    url: e_myg.target.result
                });
                $("#crop_btn_upload").html('<button type="button" class="btn btn-link" onclick="closeAvatarModal(\'group_imageUpload\')">Cancel</button><button type="button" class="btn btn-success" onclick="uploadMygAvatar()" id="cropper_apply_crop">Apply</button>');
                $("#modal-crop-avatar").modal('show');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#group_imageUpload").change(function() {
        myg_readURL(this);
    });

    function uploadMygAvatar(){
        avatar_croppie.croppie('result', {
            type: 'canvas',
		    size: 'viewport'
        }).then(function(response){
            var groupCode = $("#settings_teamcode").val();
            $.post("ajax/upload_mygroup_avatar.php",{
                image: response,
                groupCode: groupCode
            },function(data){
                alertMe(data, "success");
                getMyGroups();
                $("#my_group_avatar").attr('src', response);
                $("#cropper_apply_crop").html('Apply');
                $("#group_imageUpload").val('');
                $("#modal-crop-avatar").modal('hide');
            });
        });
    }
</script>