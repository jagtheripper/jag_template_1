<div class="modal fade modal-black" id="modal-channel-settings" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
<div class="modal-dialog modal- modal-dialog-centered modal-" role="document" style="width: 30%;">
        <div class="modal-content">
        	
            <div class="modal-header" style="padding-bottom: 0px;">
                <h6 class="modal-title" id="channel_title_settings" style="word-break: break-all;"></h6>
            </div>
            <div class="modal-body">
                <input type="hidden" id="ch_name_hidden">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <div class="input-group input-group-merge">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-hashtag"></i></span>
                                </div>
                                <input type="text" id="settings_channel_name" class="form-control" placeholder="your channel name" onkeyup="channel_name_no_space()">
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-primary btn-sm" onclick="updateNewChannelName()">Save changes</button>
                        </div>
                    </div>
                    <div class="col-12 px-0 mx-0">
                        <hr class="mt-0">
                    </div>
                    <div class="col-12">
                        <h3>Delete Channel</h3>
                        <small>All messages under this channel will also be deleted.</small><br>
                        <button type="button" class="btn btn-danger btn-sm" onclick="deleteChannel()">DELETE CHANNEL</button>
                    </div>
                    
                </div>
                
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Close</button>
            </div>
            
        </div>
    </div>
</div>
<script>
function channel_name_no_space(){
    var inputedString = $("#settings_channel_name").val();
    var newStr = inputedString.replace(" ", "-");
    $("#settings_channel_name").val(newStr);
}
</script>