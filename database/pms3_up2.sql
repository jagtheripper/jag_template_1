-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.38-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for pms
CREATE DATABASE IF NOT EXISTS `pms` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pms`;

-- Dumping structure for table pms.tbl_activity_logs
DROP TABLE IF EXISTS `tbl_activity_logs`;
CREATE TABLE IF NOT EXISTS `tbl_activity_logs` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `log` text NOT NULL,
  `module` text NOT NULL,
  `date` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `task_code` varchar(50) NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM AUTO_INCREMENT=421 DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_activity_logs: 293 rows
/*!40000 ALTER TABLE `tbl_activity_logs` DISABLE KEYS */;
INSERT INTO `tbl_activity_logs` (`log_id`, `log`, `module`, `date`, `user_id`, `task_code`) VALUES
	(404, 'Abel Bayon updated a task to done', 'Task', '2020-04-03 12:32:57', 0, '0'),
	(405, 'Abel Bayon updated a task to done', 'Task', '2020-04-03 12:32:57', 0, '0'),
	(403, 'Abel Bayon updated a task to done', 'Task', '2020-04-03 12:31:04', 0, '0'),
	(402, 'Abel Bayon updated a task to done', 'Task', '2020-04-03 12:31:02', 0, '0'),
	(409, 'Kaye Jacildo Deleted a request Log [Update Check in Payment - Ref # CP-128-040820145436; Customer CP-128-040820145436; FROM BDO 682901 682901 TO /rcbc 9000147 April 8,2020]', 'Request Logs', '2020-04-13 14:19:53', 0, '0'),
	(408, 'Kaye Jacildo updated a task to done', 'Task', '2020-04-13 13:12:23', 0, '0'),
	(407, 'Kaye Jacildo updated a task to done', 'Task', '2020-04-13 13:12:20', 0, '0'),
	(406, 'Kaye Jacildo updated a task to done', 'Task', '2020-04-13 13:08:28', 0, '0'),
	(412, 'Jerry Saydoquis updated a task to ongoing', 'Task', '2020-04-22 10:46:09', 0, '0'),
	(411, 'Jerry Saydoquis updated a task to ongoing', 'Task', '2020-04-22 10:44:38', 0, '0'),
	(410, 'Jerry Saydoquis updated a task to ongoing', 'Task', '2020-04-22 10:38:58', 0, '0'),
	(124, 'Jochelle Bravo updated a task to ongoing', 'Task', '2019-10-02 13:25:59', 0, '0'),
	(125, 'Jochelle Bravo updated a task to ongoing', 'Task', '2019-10-02 13:26:03', 0, '0'),
	(126, 'Kaye Jacildo updated a task to done', 'Task', '2019-10-05 08:12:26', 0, '0'),
	(127, 'Kaye Jacildo updated a task to done', 'Task', '2019-10-05 08:12:28', 0, '0'),
	(128, 'Kaye Jacildo updated a task to done', 'Task', '2019-10-05 08:12:39', 0, '0'),
	(129, 'Kaye Jacildo updated a task to ongoing', 'Task', '2019-10-05 08:12:49', 0, '0'),
	(130, 'Kaye Jacildo updated a task to ongoing', 'Task', '2019-10-05 08:13:04', 0, '0'),
	(131, 'Kaye Jacildo updated a task to ongoing', 'Task', '2019-10-05 08:13:12', 0, '0'),
	(132, 'Kaye Jacildo updated a task to ongoing', 'Task', '2019-10-05 08:13:12', 0, '0'),
	(133, 'Kaye Jacildo updated a task to ongoing', 'Task', '2019-10-05 08:13:15', 0, '0'),
	(134, 'Kaye Jacildo updated a task to ongoing', 'Task', '2019-10-05 08:13:17', 0, '0'),
	(135, 'Kaye Jacildo updated a task to ongoing', 'Task', '2019-10-05 08:13:19', 0, '0'),
	(136, 'Kaye Jacildo updated a task to done', 'Task', '2019-10-05 08:13:21', 0, '0'),
	(137, 'Kaye Jacildo updated a task to done', 'Task', '2019-10-05 08:14:34', 0, '0'),
	(138, 'Kaye Jacildo updated a task to ongoing', 'Task', '2019-10-05 08:14:38', 0, '0'),
	(139, 'Jochelle Bravo updated a task to done', 'Task', '2019-10-09 09:40:20', 0, '0'),
	(140, 'Kim Jade Baroa updated a task to ongoing', 'Task', '2019-10-09 09:41:00', 0, '0'),
	(141, 'Kim Jade Baroa updated a task to done', 'Task', '2019-10-09 09:43:28', 0, '0'),
	(142, 'Kim Jade Baroa updated a task to done', 'Task', '2019-10-12 09:43:10', 0, '0'),
	(143, 'Jeffred Lim updated a task to ongoing', 'Task', '2019-10-12 10:17:18', 0, '0'),
	(144, 'Jeffred Lim updated a task to ongoing', 'Task', '2019-10-12 10:17:49', 0, '0'),
	(145, 'Jochelle Bravo updated a task to done', 'Task', '2019-10-12 10:36:18', 0, '0'),
	(146, 'Jochelle Bravo updated a task to ongoing', 'Task', '2019-10-12 10:36:24', 0, '0'),
	(147, 'Jochelle Bravo updated a task to ongoing', 'Task', '2019-10-12 10:36:31', 0, '0'),
	(148, 'Jochelle Bravo updated a task to done', 'Task', '2019-10-12 10:36:35', 0, '0'),
	(149, 'Jochelle Bravo updated a task to done', 'Task', '2019-10-12 10:40:35', 0, '0'),
	(150, 'Jochelle Bravo updated a task to ongoing', 'Task', '2019-10-12 10:40:43', 0, '0'),
	(151, 'Judywen Guapin updated a task to done', 'Task', '2019-10-12 11:52:48', 0, '0'),
	(152, 'Judywen Guapin updated a task to done', 'Task', '2019-10-12 11:52:50', 0, '0'),
	(153, 'Judywen Guapin updated a task to done', 'Task', '2019-10-12 11:52:54', 0, '0'),
	(154, 'Judywen Guapin updated a task to ongoing', 'Task', '2019-10-12 11:53:00', 0, '0'),
	(155, 'Judywen Guapin updated a task to ongoing', 'Task', '2019-10-12 11:53:10', 0, '0'),
	(156, 'Judywen Guapin updated a task to done', 'Task', '2019-10-12 11:53:14', 0, '0'),
	(157, 'Judywen Guapin updated a task to done', 'Task', '2019-10-12 11:53:32', 0, '0'),
	(158, 'Eduard Rino Carton updated a task to done', 'Task', '2019-10-14 09:34:11', 0, '0'),
	(159, 'Eduard Rino Carton updated a task to done', 'Task', '2019-10-14 09:34:39', 0, '0'),
	(160, 'Eduard Rino Carton updated a task to done', 'Task', '2019-10-14 09:34:46', 0, '0'),
	(161, 'Eduard Rino Carton updated a task to done', 'Task', '2019-10-14 09:35:09', 0, '0'),
	(162, 'Eduard Rino Carton updated a task to done', 'Task', '2019-10-14 09:35:28', 0, '0'),
	(163, 'Abel Bayon updated a task to ongoing', 'Task', '2019-10-14 12:01:14', 0, '0'),
	(164, 'Judywen Guapin updated a task to ongoing', 'Task', '2019-10-14 14:50:24', 0, '0'),
	(165, 'Jochelle Bravo updated a task to done', 'Task', '2019-10-15 08:44:36', 0, '0'),
	(166, 'Jochelle Bravo updated a task to ongoing', 'Task', '2019-10-15 08:44:45', 0, '0'),
	(167, 'Abel Bayon updated a task to done', 'Task', '2019-10-15 11:13:00', 0, '0'),
	(168, 'Jochelle Bravo updated a task to done', 'Task', '2019-10-15 11:39:43', 0, '0'),
	(169, 'Judywen Guapin updated a task to done', 'Task', '2019-10-15 13:48:41', 0, '0'),
	(170, 'Judywen Guapin updated a task to ongoing', 'Task', '2019-10-15 13:49:12', 0, '0'),
	(171, 'Jeffred Lim updated a task to ongoing', 'Task', '2019-10-15 13:56:39', 0, '0'),
	(172, 'Jeffred Lim updated a task to done', 'Task', '2019-10-15 13:56:46', 0, '0'),
	(173, 'Jeffred Lim updated a task to done', 'Task', '2019-10-15 13:57:03', 0, '0'),
	(174, 'Judywen Guapin updated a task to ongoing', 'Task', '2019-10-15 14:31:08', 0, '0'),
	(175, 'Kim Jade Baroa updated a task to ongoing', 'Task', '2019-10-17 09:18:41', 0, '0'),
	(176, 'Judywen Guapin updated a task to done', 'Task', '2019-10-17 09:43:55', 0, '0'),
	(177, 'Judywen Guapin updated a task to ongoing', 'Task', '2019-10-17 09:44:00', 0, '0'),
	(178, 'Mohammed Waseem Rosquillo Anjum updated a task to ongoing', 'Task', '2019-10-17 10:35:32', 0, '0'),
	(179, 'Mohammed Waseem Rosquillo Anjum updated a task to done', 'Task', '2019-10-17 10:35:37', 0, '0'),
	(180, 'Eduard Rino Carton updated a task to done', 'Task', '2019-10-17 14:21:05', 0, '0'),
	(181, 'Jeffred Lim updated a task to done', 'Task', '2019-10-17 14:41:56', 0, '0'),
	(182, 'Kim Jade Baroa updated a task to done', 'Task', '2019-10-18 08:21:22', 0, '0'),
	(183, 'Jerry Saydoquis updated a task to done', 'Task', '2019-10-19 10:04:29', 0, '0'),
	(184, 'Jerry Saydoquis updated a task to ongoing', 'Task', '2019-10-19 10:06:06', 0, '0'),
	(185, 'Jerry Saydoquis updated a task to ongoing', 'Task', '2019-10-19 10:06:26', 0, '0'),
	(186, 'Jerry Saydoquis updated a task to done', 'Task', '2019-10-19 10:06:30', 0, '0'),
	(187, 'Judywen Guapin updated a task to ongoing', 'Task', '2019-10-19 10:40:10', 0, '0'),
	(188, 'Judywen Guapin updated a task to done', 'Task', '2019-10-19 14:33:18', 0, '0'),
	(189, 'Jochelle Bravo updated a task to ongoing', 'Task', '2019-10-21 09:03:46', 0, '0'),
	(190, 'Jochelle Bravo updated a task to ongoing', 'Task', '2019-10-22 09:56:47', 0, '0'),
	(191, 'Kaye Jacildo updated a task to ongoing', 'Task', '2019-10-23 08:44:16', 0, '0'),
	(192, 'Kaye Jacildo updated a task to ongoing', 'Task', '2019-10-23 08:44:19', 0, '0'),
	(193, 'Kaye Jacildo updated a task to ongoing', 'Task', '2019-10-23 08:44:23', 0, '0'),
	(194, 'Kaye Jacildo updated a task to ongoing', 'Task', '2019-10-23 08:44:25', 0, '0'),
	(195, 'Kaye Jacildo updated a task to ongoing', 'Task', '2019-10-23 08:44:26', 0, '0'),
	(196, 'Kaye Jacildo updated a task to ongoing', 'Task', '2019-10-23 08:44:43', 0, '0'),
	(197, 'Kim Jade Baroa updated a task to ongoing', 'Task', '2019-10-23 09:02:13', 0, '0'),
	(198, 'Kaye Jacildo updated a task to ongoing', 'Task', '2019-10-23 15:28:38', 0, '0'),
	(199, 'Kaye Jacildo updated a task to ongoing', 'Task', '2019-10-23 15:28:58', 0, '0'),
	(200, 'Ginery Songaling updated a task to done', 'Task', '2019-10-23 15:30:35', 0, '0'),
	(201, 'Ginery Songaling updated a task to ongoing', 'Task', '2019-10-23 15:44:13', 0, '0'),
	(202, 'Ginery Songaling updated a task to done', 'Task', '2019-10-23 16:05:46', 0, '0'),
	(203, 'Jeffred Lim updated a task to ongoing', 'Task', '2019-10-24 11:57:27', 0, '0'),
	(204, 'Jeffred Lim updated a task to done', 'Task', '2019-10-24 13:51:52', 0, '0'),
	(205, 'Jeffred Lim updated a task to ongoing', 'Task', '2019-10-24 13:52:27', 0, '0'),
	(206, 'Abel Bayon updated a task to done', 'Task', '2019-10-24 16:02:07', 0, '0'),
	(207, 'Jeffred Lim updated a task to done', 'Task', '2019-10-26 13:37:48', 0, '0'),
	(208, 'Kim Jade Baroa updated a task to done', 'Task', '2019-10-26 13:56:37', 0, '0'),
	(209, 'Kim Jade Baroa updated a task to ongoing', 'Task', '2019-10-26 13:56:44', 0, '0'),
	(210, 'Kim Jade Baroa updated a task to done', 'Task', '2019-10-26 13:56:52', 0, '0'),
	(211, 'Ginery Songaling updated a task to done', 'Task', '2019-10-28 14:48:17', 0, '0'),
	(212, 'Judywen Guapin updated a task to ongoing', 'Task', '2019-10-28 15:28:57', 0, '0'),
	(213, 'Jochelle Bravo updated a task to done', 'Task', '2019-10-29 09:01:48', 0, '0'),
	(214, 'Jochelle Bravo updated a task to ongoing', 'Task', '2019-10-29 09:02:07', 0, '0'),
	(215, 'Jeffred Lim updated a task to ongoing', 'Task', '2019-10-30 09:17:30', 0, '0'),
	(216, 'Jeffred Lim updated a task to done', 'Task', '2019-10-30 10:42:00', 0, '0'),
	(217, 'Kim Jade Baroa updated a task to ongoing', 'Task', '2019-10-31 10:38:05', 0, '0'),
	(218, 'Judywen Guapin updated a task to ongoing', 'Task', '2019-11-04 10:34:08', 0, '0'),
	(219, 'Judywen Guapin updated a task to ongoing', 'Task', '2019-11-04 10:34:24', 0, '0'),
	(220, 'Kim Jade Baroa updated a task to ongoing', 'Task', '2019-11-04 14:04:00', 0, '0'),
	(221, 'Jeffred Lim updated a task to ongoing', 'Task', '2019-11-04 15:37:00', 0, '0'),
	(222, 'Jeffred Lim updated a task to done', 'Task', '2019-11-07 11:47:01', 0, '0'),
	(223, 'Jeffred Lim updated a task to ongoing', 'Task', '2019-11-07 11:47:13', 0, '0'),
	(224, 'Jeffred Lim updated a task to done', 'Task', '2019-11-15 14:53:43', 0, '0'),
	(225, 'Jeffred Lim updated a task to done', 'Task', '2019-11-15 14:53:53', 0, '0'),
	(226, 'Judywen Guapin updated a task to ongoing', 'Task', '2019-11-16 09:55:35', 0, '0'),
	(227, 'Jeffred Lim updated a task to ongoing', 'Task', '2019-11-16 11:03:32', 0, '0'),
	(228, 'Jochelle Bravo updated a task to done', 'Task', '2019-11-16 11:10:46', 0, '0'),
	(229, 'John Mark Amar updated a task to done', 'Task', '2019-11-16 11:41:34', 0, '0'),
	(230, 'John Mark Amar updated a task to done', 'Task', '2019-11-16 11:41:46', 0, '0'),
	(231, 'John Mark Amar updated a task to ongoing', 'Task', '2019-11-16 11:41:57', 0, '0'),
	(232, 'John Mark Amar updated a task to done', 'Task', '2019-11-16 11:42:06', 0, '0'),
	(233, 'John Mark Amar updated a task to done', 'Task', '2019-11-16 11:42:13', 0, '0'),
	(234, 'John Mark Amar updated a task to ongoing', 'Task', '2019-11-16 11:46:15', 0, '0'),
	(235, 'Jeffred Lim updated a task to done', 'Task', '2019-11-19 11:02:03', 0, '0'),
	(236, 'Jeffred Lim updated a task to ongoing', 'Task', '2019-11-19 11:02:08', 0, '0'),
	(237, 'Jeffred Lim updated a task to done', 'Task', '2019-11-19 16:19:23', 0, '0'),
	(238, 'Jeffred Lim updated a task to done', 'Task', '2019-11-20 16:41:30', 0, '0'),
	(239, 'Jeffred Lim updated a task to ongoing', 'Task', '2019-11-20 16:41:53', 0, '0'),
	(240, 'Kaye Jacildo Deleted a request Log [Update amount in payment - PR # 3304; Customer FMB FARM; FROM 265,000.00 TO 265,500.00]', 'Request Logs', '2019-11-23 10:05:59', 0, '0'),
	(241, 'Jochelle Bravo updated a task to ongoing', 'Task', '2019-11-25 09:35:34', 0, '0'),
	(242, 'John Mark Amar updated a task to ongoing', 'Task', '2019-11-25 15:19:40', 0, '0'),
	(243, 'Jeffred Lim updated a task to ongoing', 'Task', '2019-11-27 16:51:00', 0, '0'),
	(244, 'Jeffred Lim updated a task to done', 'Task', '2019-12-02 08:48:04', 0, '0'),
	(245, 'Jochelle Bravo updated a task to done', 'Task', '2019-12-02 15:34:48', 0, '0'),
	(246, 'Jochelle Bravo updated a task to done', 'Task', '2019-12-05 15:36:35', 0, '0'),
	(247, 'Jochelle Bravo updated a task to ongoing', 'Task', '2019-12-05 15:36:41', 0, '0'),
	(248, 'Jochelle Bravo updated a task to ongoing', 'Task', '2019-12-05 15:38:25', 0, '0'),
	(249, 'Judywen Guapin Deleted a request Log [test]', 'Request Logs', '2019-12-07 13:25:29', 0, '0'),
	(250, 'Jochelle Bravo updated a task to done', 'Task', '2019-12-11 08:16:53', 0, '0'),
	(251, 'Jeffred Lim updated a task to ongoing', 'Task', '2019-12-11 10:08:15', 0, '0'),
	(252, 'Jeffred Lim updated a task to ongoing', 'Task', '2019-12-27 09:54:09', 0, '0'),
	(253, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-01-02 23:18:33', 0, '0'),
	(254, 'Jeffred Lim updated a task to done', 'Task', '2020-01-03 08:47:17', 0, '0'),
	(255, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-01-03 14:43:55', 0, '0'),
	(256, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-01-03 14:44:04', 0, '0'),
	(257, 'John Mark Amar updated a task to done', 'Task', '2020-01-06 09:48:46', 0, '0'),
	(258, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-01-06 09:55:43', 0, '0'),
	(259, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-01-06 09:55:58', 0, '0'),
	(260, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-01-06 16:32:24', 0, '0'),
	(261, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-01-06 16:32:29', 0, '0'),
	(262, 'Judywen Guapin updated a task to done', 'Task', '2020-01-06 16:33:43', 0, '0'),
	(263, 'Jeffred Lim updated a task to ongoing', 'Task', '2020-01-07 11:47:43', 0, '0'),
	(264, 'Jeffred Lim updated a task to done', 'Task', '2020-01-07 11:47:50', 0, '0'),
	(265, 'Kim Jade Baroa updated a task to done', 'Task', '2020-01-10 10:28:53', 0, '0'),
	(266, 'Kim Jade Baroa updated a task to done', 'Task', '2020-01-10 10:28:59', 0, '0'),
	(267, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-01-11 08:34:00', 0, '0'),
	(268, 'Judywen Guapin updated a task to done', 'Task', '2020-01-14 10:05:43', 0, '0'),
	(269, 'Kim Jade Baroa updated a task to ongoing', 'Task', '2020-01-15 09:05:52', 0, '0'),
	(270, 'Kim Jade Baroa updated a task to done', 'Task', '2020-01-15 09:05:55', 0, '0'),
	(271, 'Kim Jade Baroa updated a task to done', 'Task', '2020-01-15 09:05:58', 0, '0'),
	(272, 'Kim Jade Baroa updated a task to ongoing', 'Task', '2020-01-15 15:15:13', 0, '0'),
	(273, 'Kim Jade Baroa updated a task to done', 'Task', '2020-01-15 15:15:17', 0, '0'),
	(274, 'Eduard Rino Carton updated a task to done', 'Task', '2020-01-16 09:26:45', 0, '0'),
	(275, 'Eduard Rino Carton updated a task to done', 'Task', '2020-01-16 09:34:45', 0, '0'),
	(276, 'Eduard Rino Carton updated a task to done', 'Task', '2020-01-16 09:35:42', 0, '0'),
	(277, 'Eduard Rino Carton updated a task to done', 'Task', '2020-01-16 09:35:51', 0, '0'),
	(278, 'Eduard Rino Carton updated a task to done', 'Task', '2020-01-16 09:36:04', 0, '0'),
	(279, 'Eduard Rino Carton updated a task to done', 'Task', '2020-01-16 09:36:07', 0, '0'),
	(280, 'Eduard Rino Carton updated a task to ongoing', 'Task', '2020-01-16 09:37:12', 0, '0'),
	(281, 'Judywen Guapin updated a task to done', 'Task', '2020-01-16 14:29:00', 0, '0'),
	(282, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-01-16 15:38:06', 0, '0'),
	(283, 'Judywen Guapin updated a task to done', 'Task', '2020-01-16 15:38:27', 0, '0'),
	(284, 'Judywen Guapin updated a task to done', 'Task', '2020-01-16 15:38:58', 0, '0'),
	(285, 'Judywen Guapin updated a task to done', 'Task', '2020-01-16 15:39:23', 0, '0'),
	(286, 'Kaye Jacildo updated a task to done', 'Task', '2020-01-16 15:54:28', 0, '0'),
	(287, 'Kaye Jacildo updated a task to done', 'Task', '2020-01-16 15:54:30', 0, '0'),
	(288, 'Kaye Jacildo updated a task to done', 'Task', '2020-01-16 15:54:37', 0, '0'),
	(289, 'Kaye Jacildo updated a task to ongoing', 'Task', '2020-01-16 15:54:41', 0, '0'),
	(290, 'Ginery Songaling updated a task to done', 'Task', '2020-01-16 16:03:22', 0, '0'),
	(291, 'Ginery Songaling updated a task to done', 'Task', '2020-01-16 16:03:26', 0, '0'),
	(292, 'Ginery Songaling updated a task to done', 'Task', '2020-01-16 16:03:37', 0, '0'),
	(293, 'Ginery Songaling updated a task to done', 'Task', '2020-01-16 16:03:45', 0, '0'),
	(294, 'Ginery Songaling updated a task to ongoing', 'Task', '2020-01-16 16:03:46', 0, '0'),
	(295, 'Ginery Songaling updated a task to ongoing', 'Task', '2020-01-16 16:04:00', 0, '0'),
	(296, 'Kaye Jacildo updated a task to ongoing', 'Task', '2020-01-16 16:05:01', 0, '0'),
	(297, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-01-18 14:05:06', 0, '0'),
	(298, 'John Mark Amar updated a task to ongoing', 'Task', '2020-01-19 22:03:57', 0, '0'),
	(299, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-01-20 15:49:21', 0, '0'),
	(300, 'Judywen Guapin updated a task to done', 'Task', '2020-01-20 15:52:00', 0, '0'),
	(301, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-01-20 15:52:06', 0, '0'),
	(302, 'Ginery Songaling updated a task to done', 'Task', '2020-01-20 16:23:45', 0, '0'),
	(303, 'Ginery Songaling updated a task to done', 'Task', '2020-01-20 16:23:46', 0, '0'),
	(304, 'Ginery Songaling updated a task to ongoing', 'Task', '2020-01-20 16:23:49', 0, '0'),
	(305, 'Ginery Songaling updated a task to done', 'Task', '2020-01-20 16:23:52', 0, '0'),
	(306, 'Ginery Songaling updated a task to ongoing', 'Task', '2020-01-20 16:23:54', 0, '0'),
	(307, 'John Mark Amar updated a task to ongoing', 'Task', '2020-01-21 08:43:46', 0, '0'),
	(308, 'John Mark Amar updated a task to ongoing', 'Task', '2020-01-21 08:43:59', 0, '0'),
	(309, 'John Mark Amar updated a task to done', 'Task', '2020-01-21 08:44:21', 0, '0'),
	(310, 'John Mark Amar updated a task to done', 'Task', '2020-01-21 08:47:21', 0, '0'),
	(311, 'John Mark Amar updated a task to done', 'Task', '2020-01-21 08:47:24', 0, '0'),
	(312, 'John Mark Amar updated a task to done', 'Task', '2020-01-21 08:47:36', 0, '0'),
	(313, 'John Mark Amar updated a task to done', 'Task', '2020-01-21 08:49:37', 0, '0'),
	(314, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-01-21 09:02:01', 0, '0'),
	(315, 'Ginery Songaling updated a task to done', 'Task', '2020-01-21 10:34:16', 0, '0'),
	(316, 'Judywen Guapin updated a task to done', 'Task', '2020-01-22 08:58:08', 0, '0'),
	(317, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-01-22 08:58:25', 0, '0'),
	(318, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-01-22 09:04:11', 0, '0'),
	(319, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-01-22 09:04:26', 0, '0'),
	(320, 'Kim Jade Baroa updated a task to done', 'Task', '2020-01-29 16:35:44', 0, '0'),
	(321, 'Kim Jade Baroa updated a task to done', 'Task', '2020-01-29 16:35:47', 0, '0'),
	(322, 'Kaye Jacildo updated a task to ongoing', 'Task', '2020-02-04 09:42:02', 0, '0'),
	(323, 'Kaye Jacildo updated a task to done', 'Task', '2020-02-04 09:42:04', 0, '0'),
	(324, 'Jeffred Lim updated a task to ongoing', 'Task', '2020-02-11 15:10:17', 0, '0'),
	(325, 'Jeffred Lim updated a task to done', 'Task', '2020-02-11 15:10:21', 0, '0'),
	(326, 'Jeffred Lim updated a task to done', 'Task', '2020-02-13 10:44:08', 0, '0'),
	(327, 'Jeffred Lim updated a task to ongoing', 'Task', '2020-02-17 08:56:39', 0, '0'),
	(328, 'Jeffred Lim updated a task to done', 'Task', '2020-02-17 08:56:47', 0, '0'),
	(329, 'Judywen Guapin updated a task to done', 'Task', '2020-02-17 09:13:33', 0, '0'),
	(330, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-02-17 09:13:44', 0, '0'),
	(331, 'Judywen Guapin updated a task to done', 'Task', '2020-02-17 09:13:46', 0, '0'),
	(332, 'Judywen Guapin updated a task to done', 'Task', '2020-02-17 09:13:51', 0, '0'),
	(333, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-02-17 09:14:01', 0, '0'),
	(334, 'Judywen Guapin updated a task to done', 'Task', '2020-02-17 09:14:04', 0, '0'),
	(335, 'Kaye Jacildo updated a task to done', 'Task', '2020-02-17 14:20:21', 0, '0'),
	(336, 'Kaye Jacildo updated a task to ongoing', 'Task', '2020-02-17 14:20:24', 0, '0'),
	(337, 'Kaye Jacildo updated a task to ongoing', 'Task', '2020-02-17 14:20:28', 0, '0'),
	(338, 'Ginery Songaling updated a task to ongoing', 'Task', '2020-02-17 14:28:54', 0, '0'),
	(339, 'Kaye Jacildo updated a task to ongoing', 'Task', '2020-02-22 10:31:01', 0, '0'),
	(340, 'Ginery Songaling updated a task to done', 'Task', '2020-02-26 13:09:47', 0, '0'),
	(341, 'Ginery Songaling updated a task to done', 'Task', '2020-02-26 13:09:54', 0, '0'),
	(342, 'Kaye Jacildo updated a task to ongoing', 'Task', '2020-02-26 13:12:56', 0, '0'),
	(343, 'Judywen Guapin updated a task to done', 'Task', '2020-02-27 08:57:07', 0, '0'),
	(344, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-02-27 09:22:49', 0, '0'),
	(345, 'Judywen Guapin updated a task to done', 'Task', '2020-02-27 09:23:18', 0, '0'),
	(346, 'Judywen Guapin updated a task to done', 'Task', '2020-02-27 09:23:22', 0, '0'),
	(347, 'Judywen Guapin updated a task to done', 'Task', '2020-02-27 09:23:24', 0, '0'),
	(348, 'Judywen Guapin updated a task to done', 'Task', '2020-02-27 09:23:26', 0, '0'),
	(349, 'Judywen Guapin updated a task to done', 'Task', '2020-02-27 09:23:29', 0, '0'),
	(350, 'Judywen Guapin updated a task to done', 'Task', '2020-02-27 09:23:48', 0, '0'),
	(351, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-02-27 09:24:12', 0, '0'),
	(352, 'Eduard Rino Carton updated a task to done', 'Task', '2020-02-29 11:30:01', 0, '0'),
	(353, 'Eduard Rino Carton updated a task to done', 'Task', '2020-02-29 11:30:04', 0, '0'),
	(354, 'Eduard Rino Carton updated a task to done', 'Task', '2020-02-29 11:30:15', 0, '0'),
	(355, 'Eduard Rino Carton updated a task to done', 'Task', '2020-02-29 11:30:34', 0, '0'),
	(356, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-02-29 11:51:35', 0, '0'),
	(357, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-02-29 15:44:51', 0, '0'),
	(358, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-02-29 16:27:04', 0, '0'),
	(359, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-02-29 16:27:09', 0, '0'),
	(360, 'John Mark Amar updated a task to done', 'Task', '2020-03-02 08:38:56', 0, '0'),
	(361, 'John Mark Amar updated a task to ongoing', 'Task', '2020-03-02 08:42:09', 0, '0'),
	(362, 'John Mark Amar updated a task to done', 'Task', '2020-03-02 08:44:33', 0, '0'),
	(363, 'John Mark Amar updated a task to ongoing', 'Task', '2020-03-02 08:44:37', 0, '0'),
	(364, 'Judywen Guapin updated a task to done', 'Task', '2020-03-02 09:05:52', 0, '0'),
	(365, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-03-03 11:14:17', 0, '0'),
	(366, 'Jochelle Bravo updated a task to done', 'Task', '2020-03-06 16:35:53', 0, '0'),
	(367, 'Kaye Jacildo updated a task to done', 'Task', '2020-03-07 10:25:02', 0, '0'),
	(368, 'Jeffred Lim updated a task to ongoing', 'Task', '2020-03-10 09:55:02', 0, '0'),
	(369, 'John Mark Amar updated a task to ongoing', 'Task', '2020-03-10 10:54:44', 0, '0'),
	(370, 'John Mark Amar updated a task to ongoing', 'Task', '2020-03-10 10:55:49', 0, '0'),
	(371, 'John Mark Amar updated a task to done', 'Task', '2020-03-10 10:55:58', 0, '0'),
	(372, 'John Mark Amar updated a task to ongoing', 'Task', '2020-03-10 10:56:30', 0, '0'),
	(373, 'John Mark Amar updated a task to done', 'Task', '2020-03-10 10:56:37', 0, '0'),
	(374, 'John Mark Amar updated a task to ongoing', 'Task', '2020-03-10 10:57:18', 0, '0'),
	(375, 'John Mark Amar updated a task to done', 'Task', '2020-03-10 10:58:40', 0, '0'),
	(376, 'Jochelle Bravo updated a task to done', 'Task', '2020-03-10 16:19:51', 0, '0'),
	(377, 'John Mark Amar updated a task to done', 'Task', '2020-03-11 14:48:55', 0, '0'),
	(378, 'John Mark Amar updated a task to done', 'Task', '2020-03-11 14:49:11', 0, '0'),
	(379, 'Kaye Jacildo Deleted a request Log [Update Date in Delivery - Ref # DR-193-032120102906; \nFROM Mar 20, 2020 TO Feb 28, 2020]', 'Request Logs', '2020-03-21 11:16:39', 0, '0'),
	(380, 'Judywen Guapin updated a task to done', 'Task', '2020-03-30 10:12:13', 0, '0'),
	(381, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-04-01 12:59:46', 0, '0'),
	(382, 'Abel Bayon updated a task to done', 'Task', '2020-04-02 08:44:54', 0, '0'),
	(383, 'Abel Bayon updated a task to ongoing', 'Task', '2020-04-02 08:45:06', 0, '0'),
	(384, 'Abel Bayon updated a task to ongoing', 'Task', '2020-04-02 11:19:31', 0, '0'),
	(385, 'Abel Bayon updated a task to ongoing', 'Task', '2020-04-02 11:19:55', 0, '0'),
	(386, 'Abel Bayon updated a task to ongoing', 'Task', '2020-04-02 11:20:04', 0, '0'),
	(387, 'Abel Bayon updated a task to done', 'Task', '2020-04-02 11:20:13', 0, '0'),
	(388, 'Abel Bayon updated a task to done', 'Task', '2020-04-02 11:45:19', 0, '0'),
	(389, 'Abel Bayon updated a task to ongoing', 'Task', '2020-04-02 11:48:37', 0, '0'),
	(390, 'Abel Bayon updated a task to ongoing', 'Task', '2020-04-02 11:55:19', 0, '0'),
	(391, 'Judywen Guapin updated a task to done', 'Task', '2020-04-02 12:00:04', 0, '0'),
	(392, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-04-02 12:00:08', 0, '0'),
	(393, 'Abel Bayon updated a task to done', 'Task', '2020-04-02 12:01:50', 0, '0'),
	(394, 'Abel Bayon updated a task to ongoing', 'Task', '2020-04-02 12:06:53', 0, '0'),
	(395, 'Judywen Guapin updated a task to done', 'Task', '2020-04-02 12:09:38', 0, '0'),
	(396, 'Judywen Guapin updated a task to ongoing', 'Task', '2020-04-02 12:09:40', 0, '0'),
	(397, 'Judywen Guapin updated a task to done', 'Task', '2020-04-02 12:39:05', 0, '0'),
	(398, 'Abel Bayon updated a task to done', 'Task', '2020-04-02 21:41:17', 0, '0'),
	(399, 'Abel Bayon updated a task to ongoing', 'Task', '2020-04-02 21:41:47', 0, '0'),
	(400, 'Abel Bayon updated a task to done', 'Task', '2020-04-02 22:42:23', 0, '0'),
	(401, 'Abel Bayon updated a task to done', 'Task', '2020-04-02 22:48:03', 0, '0'),
	(419, 'updated a task [RPZT4752] to done', 'Task', '2020-05-01 11:57:48', 2, 'RPZT4752'),
	(418, 'updated a task [WCKV4752] to ongoing', 'Task', '2020-05-01 07:43:31', 2, 'WCKV4752'),
	(417, 'updated a task [FQXY4752] to ongoing', 'Task', '2020-05-01 07:26:42', 2, 'FQXY4752'),
	(420, 'updated a task [WCKV4752] to ongoing', 'Task', '2020-05-03 20:22:10', 2, 'WCKV4752');
/*!40000 ALTER TABLE `tbl_activity_logs` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_category
DROP TABLE IF EXISTS `tbl_category`;
CREATE TABLE IF NOT EXISTS `tbl_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` text NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_category: ~25 rows (approximately)
/*!40000 ALTER TABLE `tbl_category` DISABLE KEYS */;
INSERT INTO `tbl_category` (`category_id`, `category_name`) VALUES
	(1, 'Mouse'),
	(2, 'Keyboard'),
	(3, 'Hard Disk'),
	(4, 'Monitor'),
	(5, 'Laptop'),
	(6, 'Chair'),
	(7, 'Table'),
	(8, 'Mac Computer'),
	(9, 'Printer'),
	(10, 'Ups'),
	(11, 'Avr'),
	(12, 'Electric fan'),
	(13, 'Router'),
	(14, 'web cam'),
	(15, 'white board'),
	(16, 'Extension Cord'),
	(17, 'Computer Set'),
	(18, 'Mac Mini'),
	(19, 'Network Crimper'),
	(20, 'Video Card'),
	(21, 'Wireless PCI Adapter'),
	(22, 'Wi-Fi Range Extender'),
	(23, 'Mobile Pocket Wifi'),
	(24, 'DVD/CD Rewritable Drive'),
	(25, 'Network Switch');
/*!40000 ALTER TABLE `tbl_category` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_convo
DROP TABLE IF EXISTS `tbl_convo`;
CREATE TABLE IF NOT EXISTS `tbl_convo` (
  `convo_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `convo_name` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`convo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_convo: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_convo` DISABLE KEYS */;
INSERT INTO `tbl_convo` (`convo_id`, `created_by`, `date_created`, `convo_name`) VALUES
	(2, 2, '2020-05-02 21:21:01', 'ACCOUNTING SUPPORT'),
	(24, 2, '2020-05-03 18:10:36', 'Scammaz Tribe'),
	(36, 2, '2020-05-04 23:03:43', 'DEVELOPMENT FOREVER!'),
	(37, 2, '2020-05-08 19:53:16', 'ROSING');
/*!40000 ALTER TABLE `tbl_convo` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_convo_channel
DROP TABLE IF EXISTS `tbl_convo_channel`;
CREATE TABLE IF NOT EXISTS `tbl_convo_channel` (
  `channel_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL DEFAULT '',
  `convo_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`channel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_convo_channel: ~7 rows (approximately)
/*!40000 ALTER TABLE `tbl_convo_channel` DISABLE KEYS */;
INSERT INTO `tbl_convo_channel` (`channel_id`, `name`, `convo_id`, `created_by`, `date_created`) VALUES
	(4, 'support', 2, 2, '2020-05-02 21:27:19'),
	(5, 'socialize', 2, 2, '2020-05-02 21:27:19'),
	(11, 'socialize', 24, 2, '2020-05-03 19:37:21'),
	(25, 'socialize', 36, 2, '2020-05-04 23:04:31'),
	(27, 'wala-lang', 37, 2, '2020-05-08 19:53:36'),
	(39, 'soft-devels', 37, 2, '2020-05-10 18:22:20'),
	(40, 'soft-devels-legend', 36, 2, '2020-05-10 21:39:54');
/*!40000 ALTER TABLE `tbl_convo_channel` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_convo_member
DROP TABLE IF EXISTS `tbl_convo_member`;
CREATE TABLE IF NOT EXISTS `tbl_convo_member` (
  `convo_member_id` int(11) NOT NULL AUTO_INCREMENT,
  `convo_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `convo_role` int(11) NOT NULL,
  PRIMARY KEY (`convo_member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_convo_member: ~5 rows (approximately)
/*!40000 ALTER TABLE `tbl_convo_member` DISABLE KEYS */;
INSERT INTO `tbl_convo_member` (`convo_member_id`, `convo_id`, `member_id`, `date_added`, `convo_role`) VALUES
	(26, 2, 2, '2020-05-02 23:16:40', 1),
	(27, 24, 2, '2020-05-03 18:10:36', 1),
	(39, 36, 2, '2020-05-04 23:03:43', 1),
	(40, 37, 2, '2020-05-08 19:53:16', 1),
	(41, 37, 4, '2020-05-08 19:54:42', 0);
/*!40000 ALTER TABLE `tbl_convo_member` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_convo_msg
DROP TABLE IF EXISTS `tbl_convo_msg`;
CREATE TABLE IF NOT EXISTS `tbl_convo_msg` (
  `chat_id` int(11) NOT NULL AUTO_INCREMENT,
  `convo_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `msg` text NOT NULL,
  PRIMARY KEY (`chat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_convo_msg: ~15 rows (approximately)
/*!40000 ALTER TABLE `tbl_convo_msg` DISABLE KEYS */;
INSERT INTO `tbl_convo_msg` (`chat_id`, `convo_id`, `sender_id`, `channel_id`, `date_added`, `msg`) VALUES
	(4, 2, 2, 4, '2020-05-03 17:09:10', 'test support'),
	(11, 24, 2, 11, '2020-05-03 19:37:48', 'testing 213'),
	(12, 24, 2, 11, '2020-05-03 19:37:51', 'asd'),
	(13, 24, 2, 11, '2020-05-03 19:41:33', '@everyone \nang DEV-[GLOBAL, FEEDS etc.] updated na na from master as of wednesday last week. will update by your team leader\ndisiplina lang sa code please. kag ndi lang ko mag strict sa inyo para ka pahuway man kamo dutay ah.\n\n(checking branch / consolidation of all dev- * / testing branch)\n[remote/prefinal_notes] -&gt; (updated from master)\n\n8am-5pm support for all concerns (check vibers, fb chats, text, calls) if pwede \n@ShamTenOnce \n kung mag tawag ang branches, pangayo.e fb account kay may chatgroup for branch support. TY!\n\nNOTE: halongi mayo ang inyo gamit parehas sa paghalong mo sa palangga mo. :heart:\nKEEP SAFE EVERYONE. PLEASE PRAY SA MGA NA BILIN SA OFFICE. GOD BLESS US ALL'),
	(14, 24, 2, 11, '2020-05-03 19:45:48', 'test shit'),
	(24, 24, 2, 11, '2020-05-04 22:51:31', 'yes sir!'),
	(29, 36, 2, 25, '2020-05-04 23:05:28', 'hey!'),
	(30, 36, 2, 25, '2020-05-04 23:05:30', 'You can now Boost servers to help them gain really cool perks! (and you get some yourself) :nitro:\nTo boost a server, you require Discord Nitro, then click the button in the screenshot below to open the menu!\n\nYou don\'t have to boost this server (I would really appreciate if you did) but you can choose your favourite and help them out!\nIf you don\'t have this feature yet, please update Discord to see it appear.'),
	(31, 36, 2, 25, '2020-05-04 23:06:37', 'test sh@#$t'),
	(32, 37, 2, 27, '2020-05-08 19:54:58', 'ulo mo'),
	(33, 37, 2, 27, '2020-05-08 19:55:11', 'test'),
	(34, 37, 2, 27, '2020-05-08 19:57:29', 'test'),
	(35, 37, 4, 27, '2020-05-10 22:01:48', 'ulo mo man!'),
	(36, 37, 2, 27, '2020-05-10 22:22:40', 'test sound'),
	(37, 37, 2, 27, '2020-05-10 22:37:42', 'testing with sound and notif'),
	(38, 37, 2, 27, '2020-05-10 22:40:23', 'asdasdasdasd'),
	(39, 37, 2, 27, '2020-05-10 22:40:40', 'test 123123123123123');
/*!40000 ALTER TABLE `tbl_convo_msg` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_convo_notif
DROP TABLE IF EXISTS `tbl_convo_notif`;
CREATE TABLE IF NOT EXISTS `tbl_convo_notif` (
  `notif_id` int(11) NOT NULL AUTO_INCREMENT,
  `convo_id` int(11) NOT NULL,
  `channel_id` int(11) NOT NULL,
  `msg_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `date_sent` datetime NOT NULL,
  PRIMARY KEY (`notif_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_convo_notif: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_convo_notif` DISABLE KEYS */;
INSERT INTO `tbl_convo_notif` (`notif_id`, `convo_id`, `channel_id`, `msg_id`, `receiver_id`, `sender_id`, `date_sent`) VALUES
	(4, 37, 27, 39, 2, 2, '2020-05-10 22:40:40'),
	(5, 37, 27, 39, 4, 2, '2020-05-10 22:40:40');
/*!40000 ALTER TABLE `tbl_convo_notif` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_convo_role
DROP TABLE IF EXISTS `tbl_convo_role`;
CREATE TABLE IF NOT EXISTS `tbl_convo_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(150) NOT NULL DEFAULT '',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_convo_role: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_convo_role` DISABLE KEYS */;
INSERT INTO `tbl_convo_role` (`role_id`, `level`) VALUES
	(1, 'Admin');
/*!40000 ALTER TABLE `tbl_convo_role` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_expense
DROP TABLE IF EXISTS `tbl_expense`;
CREATE TABLE IF NOT EXISTS `tbl_expense` (
  `expense_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  `date` varchar(50) NOT NULL,
  `date_borrowed` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `expense_name` varchar(100) NOT NULL,
  `reference` varchar(50) NOT NULL,
  PRIMARY KEY (`expense_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_expense: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_expense` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_expense` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_inventory
DROP TABLE IF EXISTS `tbl_inventory`;
CREATE TABLE IF NOT EXISTS `tbl_inventory` (
  `inv_id` int(11) NOT NULL AUTO_INCREMENT,
  `inv_serial` varchar(100) NOT NULL DEFAULT '',
  `inv_unit` int(11) NOT NULL,
  `inv_category` int(11) NOT NULL,
  `inv_location` int(11) NOT NULL,
  `inv_item_name` text NOT NULL,
  `inv_date` datetime NOT NULL,
  PRIMARY KEY (`inv_id`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_inventory: ~146 rows (approximately)
/*!40000 ALTER TABLE `tbl_inventory` DISABLE KEYS */;
INSERT INTO `tbl_inventory` (`inv_id`, `inv_serial`, `inv_unit`, `inv_category`, `inv_location`, `inv_item_name`, `inv_date`) VALUES
	(1, '9556091107353', 1, 1, 1, 'A4tech mouse color black', '2020-01-02 22:48:44'),
	(2, 'PHI708049968', 1, 2, 1, 'A4tech Black Mouse', '2020-01-27 15:08:17'),
	(3, 'PHI706041952', 1, 1, 1, 'A4Tech Black Mouse', '2020-01-27 15:09:12'),
	(4, 'PH1708049978', 1, 2, 1, 'A4Tech Black Keyboard', '2020-01-27 15:13:11'),
	(5, 'PH1708049977', 1, 2, 1, 'A4Tech color black keyboard', '2020-01-27 15:16:29'),
	(6, 'PH1708049873', 1, 1, 1, 'A4Tech color black mouse', '2020-01-27 15:17:23'),
	(7, 'PH1708049274', 1, 1, 1, 'A4Tech black mouse', '2020-01-27 15:19:53'),
	(8, 'PH1708048846', 1, 1, 1, 'A4Tech Black mouse', '2020-01-27 15:21:06'),
	(9, 'PH1708049967', 1, 2, 1, 'A4tech black keyboard', '2020-01-27 15:23:46'),
	(10, 'PE1712020959', 1, 1, 1, 'A4Tech color black mouse', '2020-01-27 15:25:30'),
	(11, 'PE1712020954', 1, 2, 1, 'A4tech color black Keyboard', '2020-01-27 15:26:28'),
	(12, 'PE1712023160', 1, 1, 1, 'A4Tech black mouse', '2020-01-27 15:28:09'),
	(13, 'PE1712023170', 1, 2, 1, 'A4Tech black Keyboard', '2020-01-27 15:46:08'),
	(14, 'PE1712023181', 1, 1, 1, 'A4Tech black mouse', '2020-01-27 15:47:23'),
	(15, 'PE1712023166', 1, 2, 1, 'A4Tech black Keyboard', '2020-01-27 15:47:55'),
	(16, 'PH1706041903', 1, 2, 1, 'A4Tech black keyboard', '2020-01-27 15:48:37'),
	(17, 'PH1708049966', 1, 2, 1, 'A4Tech black keyboard', '2020-01-27 15:49:34'),
	(18, 'PE1712020953', 1, 2, 1, 'A4Tech black keyboard', '2020-01-27 15:50:21'),
	(19, 'PH1708049964', 1, 2, 1, 'A4Tech black keyboard', '2020-01-27 15:50:56'),
	(20, 'PH1708049838', 1, 1, 1, 'A4Tech black mouse', '2020-01-27 15:51:27'),
	(21, 'PH1708049938', 1, 2, 1, 'A4Tech black keyboard', '2020-01-27 15:51:59'),
	(22, 'PH1708049859', 1, 1, 1, 'A4Tech black mouse', '2020-01-27 15:52:26'),
	(23, 'PH1708050838', 1, 1, 1, 'A4Tech black mouse', '2020-01-27 15:53:28'),
	(24, 'PH1708049965', 1, 2, 1, 'A4Tech black keyboard', '2020-01-27 15:54:02'),
	(25, 'PH1706041959', 1, 1, 1, 'A4Tech black mouse', '2020-01-27 15:54:36'),
	(26, 'PH1706041902', 1, 2, 1, 'A4tech black keyboard', '2020-01-27 15:55:08'),
	(27, 'PH1708059968', 1, 1, 1, 'A4tech black mouse', '2020-01-27 16:06:09'),
	(28, 'PH1708020123', 1, 1, 1, 'A4Tech black mouse', '2020-01-27 16:08:13'),
	(29, 'PH1708050124', 1, 2, 1, 'A4Tech black keyboard', '2020-01-27 16:09:35'),
	(30, 'PH1708060125', 1, 1, 1, 'A4tech black mouse', '2020-01-27 16:10:18'),
	(31, 'PH1708060126', 1, 1, 1, 'A4Tech black mouse', '2020-01-27 16:10:45'),
	(32, 'PH1708060127', 1, 1, 1, 'A4Tech black mouse', '2020-01-27 16:11:12'),
	(33, 'PH1810040802', 1, 14, 1, 'black web cam', '2020-01-27 16:13:16'),
	(34, 'AVR-01272001', 1, 11, 1, 'Avr', '2020-01-27 16:18:45'),
	(35, 'AVR-01272002', 1, 11, 1, 'Avr', '2020-01-27 16:19:08'),
	(36, 'UPS-01272001', 1, 10, 1, 'UPS', '2020-01-27 16:19:55'),
	(37, 'UPS-0127002', 1, 10, 1, 'UPS', '2020-01-27 16:20:52'),
	(38, 'UPS-0127003', 1, 10, 1, 'UPS', '2020-01-27 16:21:05'),
	(39, 'UPS-0127004', 1, 10, 1, 'UPS', '2020-01-27 16:21:30'),
	(40, 'UPS-0127005', 1, 10, 1, 'UPS', '2020-01-27 16:21:44'),
	(41, 'UPS-0127006', 1, 10, 1, 'UPS', '2020-01-27 16:22:07'),
	(42, 'UPS-0127007', 1, 10, 1, 'UPS', '2020-01-27 16:22:26'),
	(43, 'UPS-0127008', 1, 10, 1, 'UPS', '2020-01-27 16:22:41'),
	(44, 'UPS-0127009', 1, 10, 1, 'UPS', '2020-01-27 16:22:56'),
	(45, 'UPS-0127010', 1, 10, 1, 'UPS', '2020-01-27 16:23:17'),
	(46, 'UPS-0127011', 1, 10, 1, 'UPS', '2020-01-27 16:23:32'),
	(47, 'UPS-0127012', 1, 10, 1, 'UPS', '2020-01-27 16:23:50'),
	(48, 'UPS-0127013', 1, 10, 1, 'UPS', '2020-01-27 16:24:03'),
	(49, 'UPS-0127014', 1, 10, 1, 'UPS', '2020-01-27 16:24:53'),
	(50, 'KPPH51A005428', 1, 4, 1, '20702 Display 19.5 inch', '2020-01-27 16:30:24'),
	(51, 'KPPH51A005895', 1, 4, 1, '20702 Display 19.5 inch', '2020-01-27 16:30:57'),
	(52, 'KPPH51A005898', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:31:29'),
	(53, 'KPPH51A005404', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:32:04'),
	(54, 'ANIG81A014337', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:32:40'),
	(55, 'ANIG81A015320', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:34:28'),
	(56, 'ANIG81A011271', 1, 4, 1, '18.4 inch monitor', '2020-01-27 16:35:17'),
	(57, 'ANIG81A014172', 1, 4, 1, '18.4 inch monitor', '2020-01-27 16:35:50'),
	(58, '63700172724', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:36:38'),
	(59, '63700213724', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:37:02'),
	(60, 'ANIG81A014753', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:37:32'),
	(61, 'KCYH21A00658', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:38:38'),
	(62, 'KCYH21A006990', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:39:13'),
	(63, 'KCYH21A006580', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:39:43'),
	(64, 'ANIH81A006328', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:40:18'),
	(65, 'ANIH81A0064328', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:40:48'),
	(66, '70701010324', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:41:21'),
	(67, '70701010325', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:42:05'),
	(68, 'MRCQHYCDB00734K', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:42:46'),
	(69, 'RHVCDB00734K', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:43:22'),
	(70, 'KCYH21A006191', 1, 4, 1, '18.4 inch monitor', '2020-01-27 16:44:19'),
	(71, 'KCYH21A006926', 1, 4, 1, '18.4 inch monitor', '2020-01-27 16:44:54'),
	(72, 'MRCQHYCF100692H', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:45:28'),
	(73, 'MRCQHYCF101612N', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:45:56'),
	(74, 'ANIG81A014168', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:46:30'),
	(75, 'ANIG81A014760', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:46:58'),
	(76, 'KCYH21A006587', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:47:30'),
	(77, 'KCYH21A006583', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:47:56'),
	(78, 'KPPG61A001149', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:48:32'),
	(79, 'KPPG61A001147', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:48:58'),
	(80, 'KPPH51A004939', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:49:36'),
	(81, 'KPPH51A005894', 1, 4, 1, '19.5 inch monitor', '2020-01-27 16:50:07'),
	(82, 'CO71037PJGHV', 1, 18, 1, 'MacOS Mojave v10.14.6; Processor: 1.4 GHz intel core i5; memory: 4gb 1600 MHz DDR3; GPU: intel HD Graphics 5000 1536mb', '2020-01-28 08:35:08'),
	(83, 'CO7VK004GDO', 1, 18, 1, 'MacOS Mojave v10.14.6; Processor: 1.4GHz intel core i5; memory 8GB 1600MHz DDR3; GPU: intel HD Graphics 5000 1536MB', '2020-01-28 08:39:03'),
	(84, 'CO7VK007GIJO', 1, 18, 1, 'MacOS Mojave v.10.14.4; Processor: 1.4GHz intel core i5; memory: 8GB 1600MHz DDR3; GPU: intel HD Graphics 5000 1536MB', '2020-01-28 08:42:39'),
	(85, 'CO71032NDGIHV', 1, 18, 1, 'MacOS Mojave v10.14.6; Processor: 1.4GHz intel core i5; memory: 4GB 1600MHz DDR3; GPU: intel HD Graphics 5000 1536MB', '2020-01-28 08:44:36'),
	(86, 'CO7VK605GIJO', 1, 18, 1, 'MacOS Mojave v.10.14.6; Processor: 1.4GHz intel core i5; Memory: 8GB 1600MHz DDR3; GPU: intel HD Graphics 5000 1536MB', '2020-01-28 08:47:13'),
	(87, 'CO71032POGIHV', 1, 18, 1, 'MacOS High Sierra v10.13.6; Processor: 1.4GHz intel core i5; Memory: 4GB 1600MHz DDR3; GPU: intel HD Graphics 5000 1536MB', '2020-01-28 08:49:23'),
	(88, 'CO7VK0036IJO', 1, 18, 1, 'MacOS Sierra v10.12.6; Proccessor: 1.4GHz intel core i5; Memory: 8GB 1600MhHz DDR3; GPU: intel HD Graphics 5000 1536MB', '2020-01-28 08:51:58'),
	(89, 'CO71032MWGIHV', 1, 18, 1, 'MacOS Mojave v10.14.6; Processor: 1.4GHz intel core i5; Memory: 4GB 1600MHz DDR3; GPU: intel HD Graphics 5000 1536MB', '2020-01-28 08:53:41'),
	(90, 'CO7VK006GIJO', 1, 18, 1, 'MacOS Sierra v10.12.6; Proccessor: 1.4GHz intel core i5; Memory: 8GB 1600MHz DDR3; GPU: intel HD Graphics 5000 1536MB', '2020-01-28 08:55:46'),
	(91, 'CO7VK008GIJO', 1, 18, 1, 'MacOS Mojave v10.14.6; Processor: 1.4GHz intel core i5; Memory: 8GB 1600MHz DDR3; GPU: intel HD Graphics 5000 1536MB', '2020-01-28 08:57:31'),
	(92, 'CS-012820001', 1, 17, 1, 'Linux OS Ubuntu; Processor: intel core i7-7700 (8 CPUs) @ 4200GHz; Memory: 8GB; GPU: NVIDIA GeForce GT 730; HDD: 2TB', '2020-01-28 09:03:58'),
	(93, 'CS-012820002', 1, 17, 1, 'Windows 10 Pro 64-bit; Processor: intel(R) core(TM) i7-7700 CPU @ 3.60GHz (8 CPUs) ~ 3.6GHz; GPU: NVIDIA GeForce GT 730; HDD: 2TB', '2020-01-28 09:06:18'),
	(94, 'CS-012820003', 1, 17, 1, 'Windows 10 Pro 64-bit; Processor: intel(R) core(TM) i7-7700 CPU @ 3.60GHz(8 CPUs) ~ 3.6GHz; Memory: 8GB; GPU: NVIDIA GeForce GT 730; HDD: 2TB', '2020-01-28 09:08:34'),
	(95, 'CS-012820004', 1, 17, 1, 'Windows 10 Pro 64-bit; Processor: intel(R) core(TM) CPU @ 3.60GHz (8 CPUs) ~ 3.6GHz; Memory: 4GB; GPU: NVIDIA GeForce GT 730', '2020-01-28 09:10:30'),
	(96, 'CS-012820005', 1, 17, 1, 'WIndows 10 Pro 64-bit; Processor: intel(R) core(TM) i7-7700 CPU @ 3.60GHz (8 CPUs) ~ 3.6GHz; Memory: 8GB; GPU: NVIDIA GeForce GT 730', '2020-01-28 09:12:18'),
	(97, 'CS-012820006', 1, 17, 1, 'Windows 10 Pro 64-bit; Processor: intel(R) core(TM) i7-7700 CPU @ 3.60GHz (8 CPUs) ~ 3.6GHz; Memory: 4GB; GPU: NVIDIA GeForce GT 730', '2020-01-28 09:14:19'),
	(98, 'TBL-012820001', 1, 7, 1, 'Work Table (Sir wilson table)', '2020-01-28 09:24:28'),
	(99, 'TBL-012820002', 1, 7, 1, 'Wood Computer Table', '2020-01-28 09:25:13'),
	(100, 'TBL-012820003', 1, 7, 1, 'Wood Computer Table', '2020-01-28 09:26:10'),
	(101, 'TBL-012820004', 1, 7, 1, 'Wood Computer Table', '2020-01-28 09:30:09'),
	(102, 'TBL-012820005', 1, 7, 1, 'Wood Computer Table', '2020-01-28 09:30:36'),
	(103, 'TBL-012820006', 1, 7, 1, 'Wood Computer Table', '2020-01-28 09:31:06'),
	(104, 'TBL-012820007', 1, 7, 1, 'Wood Computer Table', '2020-01-28 09:31:22'),
	(105, 'TBL-012820008', 1, 7, 1, 'Wood Computer Table', '2020-01-28 09:31:46'),
	(106, 'TBL-012820009', 1, 7, 1, 'Wood Computer Table', '2020-01-28 09:32:00'),
	(107, 'TBL-012820010', 1, 7, 1, 'Wood Computer Table', '2020-01-28 09:32:20'),
	(108, 'TBL-012820011', 1, 7, 1, 'Wood Table', '2020-01-28 09:33:07'),
	(109, 'TBL-012820012', 1, 7, 1, 'Plastic Table', '2020-01-28 09:33:29'),
	(110, 'ROUT-012820001', 1, 13, 1, 'TP-Link Wifi router', '2020-01-28 09:37:24'),
	(111, 'T7170107126', 1, 20, 1, 'Palit Video Card', '2020-01-28 09:47:05'),
	(112, 'PVDJ1B8022758', 1, 21, 1, 'D-Link Wireless N 150 Desktop Adapter', '2020-01-28 09:49:02'),
	(113, 'PVDJ1B8022754', 1, 21, 1, 'D-Link Wireless N 150 Desktop Adapter', '2020-01-28 09:49:41'),
	(114, 'CRMP-012820001', 1, 19, 1, 'Crimper', '2020-01-28 09:51:29'),
	(115, '2171370003286', 1, 22, 1, 'TP-Link Universal Wi-fi Range Extender', '2020-01-28 09:53:33'),
	(116, 'T4F7S15C05000662', 1, 23, 1, 'Tattoo Huawei Mobile Wifi (Alex)', '2020-01-28 09:56:47'),
	(117, 'T4FDW15A13026570', 1, 23, 1, 'Tattoo Huawei Mobile Wifi (Pototan Branch)', '2020-01-28 09:58:20'),
	(118, 'T4DW15C15000148', 1, 23, 1, 'Tattoo Huawei Mobile Wifi (Carmen Pingkay)', '2020-01-28 09:59:57'),
	(119, 'T4FS15C050002368', 1, 23, 1, 'Tattoo Huawei Mobile Wifi (Doc Joan)', '2020-01-28 10:01:19'),
	(120, 'T4FS15C050001729', 1, 23, 1, 'Tattoo Huawei Mobile Wifi (Genelyn Mandaue)', '2020-01-28 10:02:26'),
	(121, 'T4DW15C15000226', 1, 23, 1, 'Tattoo Huawei Mobile Wifi (Passi)', '2020-01-28 10:03:23'),
	(122, 'T4FS15C05001985', 1, 23, 1, 'Tattoo Huawei Mobile Wifi (Iloilo)', '2020-01-28 10:04:34'),
	(123, '3B81155000463743524261', 1, 1, 1, 'Lite-on It Corp. DVD/CD Rewritable Disk', '2020-01-28 10:08:27'),
	(124, '2684225003723743524665', 1, 24, 1, 'Lite-on It Corp. DVD/CD Rewritable Drive', '2020-01-28 10:09:30'),
	(125, 'DY0T274001284', 1, 13, 1, 'D-Link Ethernet Broadband Router', '2020-01-28 10:11:45'),
	(126, '47A01717F', 1, 1, 1, 'ASUS Black Mouse', '2020-01-28 10:13:39'),
	(127, 'MS- 012820001', 1, 1, 1, 'A4Tech black mouse', '2020-01-28 10:14:34'),
	(128, '5VV6XZ2T', 1, 3, 1, 'Seagate Hard Drive', '2020-01-28 10:15:44'),
	(129, '5VV65CNB', 1, 3, 1, 'Seagate Hard Drive', '2020-01-28 10:16:26'),
	(130, 'ANIH81A006345', 1, 4, 1, '19.5 inch Monitor', '2020-01-28 10:20:44'),
	(131, 'PH1706041904', 1, 2, 1, 'A4Tech black keyboard', '2020-01-28 10:21:15'),
	(132, 'MS-012820002', 1, 1, 1, 'Powkit USB Optical Mouse', '2020-01-28 10:22:51'),
	(133, 'MS-012820003', 1, 1, 1, 'CD-R KING Optical Mouse', '2020-01-28 10:23:26'),
	(134, 'MS-012820004', 1, 1, 1, '3D Optical Mouse', '2020-01-28 10:24:01'),
	(135, 'EF18144806', 1, 12, 1, 'Electric Fan', '2020-01-28 10:25:48'),
	(136, 'E73185C6H558050', 1, 9, 1, 'Brother printer with photocopier', '2020-01-28 10:32:30'),
	(137, 'NXMF2SP0013370F7DA6600', 1, 5, 1, 'Asus Aspire E1-470; Processor: intel core i3 3217U(1.8GHz L3 cache);  Memory: 4GB DDR3; 750GB HDD; DVD Super Multi DL drive; GPU: intel HD Graphics 4000, up to 1817MB', '2020-01-28 10:37:31'),
	(138, 'G7N0CV04Y786227A', 1, 5, 1, 'Asus Windows 10; CPU: intel core i3-50050, 2.0GHz; Memory: 4GB; HDD: 500GB; ODD: DVD Sup.MTL', '2020-01-28 10:41:10'),
	(139, 'FAN0WU287339444', 1, 5, 1, 'Asus Windows 10; CPU: intel core i5-5200U, up to 2.7GHz; Memory: 4GB; HDD: 500GB; ODD: DVD sup.MTL', '2020-01-28 10:43:56'),
	(140, 'E73186E4F260403', 1, 9, 1, 'Brother printer and photocopier (Guba printer head)', '2020-01-28 10:46:17'),
	(141, 'EF-0128200001', 1, 12, 1, 'Wall fan', '2020-01-28 10:48:02'),
	(142, 'SERVER-01282000', 1, 17, 1, 'Linux 4.15.0-74-generic on x86_64; CPU: intel core i5-2320 CPU @ 3.00GHz, 4 Cores; Memory: 4GB; HDD: 1TB (Server)', '2020-01-28 10:53:33'),
	(143, 'PF0ARM8K', 1, 5, 1, 'Lenovo G40 Windows 8 64-bit; CPU: intel core i3; Memory: 2GB; HDD: 500GB;', '2020-01-28 11:07:40'),
	(144, ' KYBRD- 012820001', 1, 2, 1, 'A4Tech black keyboard', '2020-01-28 11:09:12'),
	(145, '218C252004175', 1, 25, 1, 'TP-Link 8-Port Gigabit Desktop Switch', '2020-01-28 11:10:42'),
	(146, 'SERVER-012820001', 1, 4, 1, '19.5 inch monitor (Server)', '2020-01-28 11:17:21');
/*!40000 ALTER TABLE `tbl_inventory` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_late
DROP TABLE IF EXISTS `tbl_late`;
CREATE TABLE IF NOT EXISTS `tbl_late` (
  `late_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `date` varchar(50) NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  `date_late` varchar(50) NOT NULL,
  `reference` varchar(50) NOT NULL,
  PRIMARY KEY (`late_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_late: ~3 rows (approximately)
/*!40000 ALTER TABLE `tbl_late` DISABLE KEYS */;
INSERT INTO `tbl_late` (`late_id`, `user_id`, `date`, `amount`, `date_late`, `reference`) VALUES
	(1, 3, '2020-01-24', 50.00, '2020-01-24', 'LTE-200124060335'),
	(2, 5, '2020-01-24', 50.00, '2020-01-24', 'LTE-200124060350'),
	(4, 7, '2020-02-08', 50.00, '2020-02-08', 'LTE-200208031710');
/*!40000 ALTER TABLE `tbl_late` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_location
DROP TABLE IF EXISTS `tbl_location`;
CREATE TABLE IF NOT EXISTS `tbl_location` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `location_name` text NOT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_location: ~4 rows (approximately)
/*!40000 ALTER TABLE `tbl_location` DISABLE KEYS */;
INSERT INTO `tbl_location` (`location_id`, `location_name`) VALUES
	(1, 'Programmers Office'),
	(2, 'Accounting Office'),
	(3, 'Production Office'),
	(4, 'H3 Office');
/*!40000 ALTER TABLE `tbl_location` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_note
DROP TABLE IF EXISTS `tbl_note`;
CREATE TABLE IF NOT EXISTS `tbl_note` (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `note_title` text NOT NULL,
  `note_content` text NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`note_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_note: ~5 rows (approximately)
/*!40000 ALTER TABLE `tbl_note` DISABLE KEYS */;
INSERT INTO `tbl_note` (`note_id`, `note_title`, `note_content`, `user_id`) VALUES
	(10, 'MRE TO WORK', 'Ingredients\n\n1 bunch asparagus, cut into 1-inch pieces\n1 head broccoli, stems removed and cut into pieces\n1 cup baby carrots, chopped into small pieces\n1 sweet potato, diced very small\nRed and green peppers, chopped into large pieces\n1 package chicken sausage (I use AmyLu brand from Costco), sliced into large pieces\nOlive oil\n2-3 tablespoons Italian Seasoning', 4),
	(11, 'test', 'asdasd', 4),
	(12, 'My TODO\'s for pms beta', '*Due Task\n*Profile Settings\n*Create Project\n*Project Settings\n*Request Logs\n*chat\n*more', 2),
	(13, 'asdasd', 'notes ko di para di ko malipat kay remember me ang title ka modal\n\n\nasdasdasd\n\nasdasd', 3),
	(14, 'my notes', 'deleteChannel(\\\'\'.$ch_list[id].\'\\\')', 2);
/*!40000 ALTER TABLE `tbl_note` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_payment
DROP TABLE IF EXISTS `tbl_payment`;
CREATE TABLE IF NOT EXISTS `tbl_payment` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(50) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `date_payment` varchar(50) NOT NULL,
  `reference` varchar(50) NOT NULL,
  `type` text NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_payment: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_payment` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_project
DROP TABLE IF EXISTS `tbl_project`;
CREATE TABLE IF NOT EXISTS `tbl_project` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `projectCode` varchar(100) NOT NULL,
  `projectName` text NOT NULL,
  `projectDescription` text NOT NULL,
  `proj_pm` int(11) NOT NULL,
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_project: ~3 rows (approximately)
/*!40000 ALTER TABLE `tbl_project` DISABLE KEYS */;
INSERT INTO `tbl_project` (`project_id`, `projectCode`, `projectName`, `projectDescription`, `proj_pm`) VALUES
	(1, 'J6K5L4BM21', 'NOTES SERVICE', 'TO DEVELOP AND FINISH THE NOTES FARM MANAGEMENT SOFTWARE', 18),
	(2, 'Q9W8ETYUO', 'OPTINOTES', 'FOR NUTRITIONIST FORMULATION', 18),
	(3, 'ASDX987XCV', 'MOBILE APPLICATION', 'NOTES mobile apps', 18);
/*!40000 ALTER TABLE `tbl_project` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_project_member
DROP TABLE IF EXISTS `tbl_project_member`;
CREATE TABLE IF NOT EXISTS `tbl_project_member` (
  `project_member_id` int(11) NOT NULL AUTO_INCREMENT,
  `projectCode` text NOT NULL,
  `teamCode` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` int(3) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`project_member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_project_member: ~19 rows (approximately)
/*!40000 ALTER TABLE `tbl_project_member` DISABLE KEYS */;
INSERT INTO `tbl_project_member` (`project_member_id`, `projectCode`, `teamCode`, `user_id`, `type`, `role_id`) VALUES
	(1, 'J6K5L4BM21', '6D5GB5NM4', 0, 1, 4),
	(3, 'J6K5L4BM21', '', 4, 0, 0),
	(4, 'J6K5L4BM21', '', 6, 0, 4),
	(6, 'Q9W8ETYUO', '', 5, 0, 0),
	(7, 'J6K5L4BM21', '', 3, 0, 4),
	(8, 'J6K5L4BM21', '', 9, 0, 4),
	(9, 'J6K5L4BM21', '', 10, 0, 4),
	(10, 'ASDX987XCV', '', 16, 0, 0),
	(11, 'J6K5L4BM21', '', 14, 0, 4),
	(12, 'J6K5L4BM21', '', 11, 0, 0),
	(13, 'J6K5L4BM21', '', 11, 0, 0),
	(14, 'J6K5L4BM21', '', 12, 0, 0),
	(15, 'J6K5L4BM21', '', 15, 0, 0),
	(16, 'J6K5L4BM21', '', 7, 0, 0),
	(17, 'J6K5L4BM21', '', 8, 0, 0),
	(18, 'J6K5L4BM21', '', 13, 0, 0),
	(19, 'J6K5L4BM21', '', 5, 0, 0),
	(20, 'ASDX987XCV', '', 17, 0, 0),
	(23, 'ASDX987XCV', 'B5M4YTU', 0, 1, 0),
	(25, 'Q9W8ETYUO', '6D5GB5NM4', 0, 1, 0);
/*!40000 ALTER TABLE `tbl_project_member` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_request_logs
DROP TABLE IF EXISTS `tbl_request_logs`;
CREATE TABLE IF NOT EXISTS `tbl_request_logs` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_date` datetime NOT NULL,
  `logs` text NOT NULL,
  `requested_by` text NOT NULL,
  `person_assigned` int(11) NOT NULL,
  `approved_by` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `approve_date` datetime NOT NULL,
  `remarks` text NOT NULL,
  PRIMARY KEY (`request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_request_logs: ~41 rows (approximately)
/*!40000 ALTER TABLE `tbl_request_logs` DISABLE KEYS */;
INSERT INTO `tbl_request_logs` (`request_id`, `request_date`, `logs`, `requested_by`, `person_assigned`, `approved_by`, `status`, `approve_date`, `remarks`) VALUES
	(8, '2019-09-05 08:33:43', 'Update Collection - From Cr #5662 To PR#1965', 'Ronel Fuentes', 14, 19, 1, '2019-09-05 09:21:24', ''),
	(9, '2019-09-05 09:18:37', 'Update Delivery Status from Finished to Saved - Charge Invoice # 753; Customer: OCS Farm', 'Bantayan Branch', 14, 19, 1, '2019-09-05 09:21:31', ''),
	(10, '2019-09-05 10:25:58', 'Update CR # NR-CR-10709 to CR # 839 - Customer: Aldrin Jusay; Ref # CP-131-090219093247', 'Mandaue Branch', 14, 19, 1, '2019-09-05 10:40:39', 'Erroneously clicked the NO RECEIPT option'),
	(11, '2019-09-06 10:53:28', 'Update Collection Amount - Ref #: CR # 778; Customer: RJZ Farm; From 75,600.00 to 76500', 'Bantayan Branch', 14, 19, 1, '2019-09-06 10:54:39', ''),
	(14, '2019-09-13 10:25:04', 'Update Payment CR Receipt - Customer: Quirino Mata;PR #: 2909; From: 792;To: 794', 'Bantayan Branch', 14, 19, 1, '2019-09-13 10:28:37', ''),
	(15, '2019-09-19 09:29:48', 'Update CR # - Ref #: CP-128-091919091021; Customer: Med Farm; From 6365 - To No Receipt.', 'Madel Pamatian', 14, 19, 1, '2019-09-19 09:43:50', 'should be undeclared transaction'),
	(16, '2019-09-24 10:08:25', 'Add Remarks in Collection: Ref # CP-131-092419095727 and  Ref # CP-131-092419095939; Remarks "Partial Payment"', 'Mandaue Branch', 14, 19, 1, '2019-09-24 10:13:41', ''),
	(17, '2019-09-24 10:16:30', 'Open date September 18 , deposit need to encode', 'Mandaue Branch', 3, 19, 1, '2019-09-24 10:17:53', ''),
	(18, '2019-10-04 11:14:11', 'Update date in collection: Ref # CP-205-092819162057; From September 28, 2019 To September 27, 2019', 'Ronel Fuentes', 14, 19, 1, '2019-10-04 11:15:41', ''),
	(19, '2019-10-08 09:55:37', 'Update Source Location in Request - Ref # RST-128-100819094205; From Carmen Warehouse - Carmen Branch To Production Branch Warehouse 1', 'Madel', 14, 19, 1, '2019-10-08 09:57:02', ''),
	(20, '2019-10-22 14:06:01', 'Update Document Type (Pettycash Request) From Others - Official Receipt', 'Richie Fernandez', 7, 19, 1, '2019-10-31 10:27:54', ''),
	(21, '2019-10-31 10:26:00', 'Add another chart in CV - CV#: CV-132-103119094843; Account: Payroll; Branch: Technical team.', 'Fatima', 14, 19, 1, '2019-10-31 10:27:59', ''),
	(22, '2019-11-04 09:02:13', 'Open Deposit Date to October 28,2019 (Due to internet connection).', 'Mandaue Branch', 14, 19, 1, '2019-11-04 09:04:30', ''),
	(23, '2019-11-04 10:02:26', 'delete customer payments microfiliming(incorrect image uploaded) ref #CP-127-110419093717', 'Iloilo Branch', 15, 19, 1, '2019-11-04 10:04:12', ''),
	(24, '2019-11-07 10:07:26', 'Open Deposit Date to October 29,2019 (Encode Deposit).', 'Madel', 3, 19, 1, '2019-11-07 10:09:13', ''),
	(25, '2019-11-13 14:12:43', 'Add Service - One time payment (South Mindanao Branch)', 'Pristine Baguios', 14, 19, 1, '2019-11-13 14:13:54', ''),
	(27, '2019-11-23 10:05:30', 'Update amount in payment - PR # 3304; Customer FMB FARM; FROM 265,000.00 TO 265,500.00', 'Mandaue', 14, 19, 1, '2019-11-23 10:08:55', ''),
	(28, '2019-12-04 14:40:13', 'Update product in Delivery: DR # DR-131-120419100855; Invoice #: 1031; From PREMIUM SWINE LACTATING MASH to PREMIUM SWINE GESTATING MASH', 'Manduae', 14, 19, 1, '2019-12-04 14:47:25', 'Okay!'),
	(30, '2019-12-07 13:26:28', 'Update payment type in customer payment: CR # 6539;  Customer: Fresh Harvest Farm; FROM Check TO Cash.', 'Madel', 14, 19, 1, '2019-12-07 13:27:10', ''),
	(31, '2019-12-21 09:41:35', 'Update Bank in Payment - Ref # CP-131-121119113742; PR # 3316(Check); Customer Christian Canada; FROM Cash in Bank -BDO (Bacolod) CA-6420 TO Cash in Bank - METRO (Bacolod) CA-91016-2', 'Mandaue', 14, 19, 1, '2019-12-21 09:45:36', ''),
	(32, '2019-12-27 09:56:01', 'Update DR invoice - DR # DR-126-122719094233; Status: SAVED; Customer: Quirino Mata; FROM cash invoice 278 TO  charge invoice 964.', 'Bantayan', 14, 19, 1, '2019-12-27 09:57:44', ''),
	(33, '2019-12-28 08:30:10', 'Update DR invoice - DR # DR-126-122719102305; Status: SAVED; Customer: Junper Russel; FROM charge invoice 967 TO cash invoice 279.', 'Bantayan', 14, 19, 1, '2019-12-28 09:36:40', ''),
	(34, '2020-01-02 10:36:54', 'Update product and price in Delivery - Charge invoice # 1036; Customer Rosales Farm; FROM PREMIUM SWINE FINISHER MASH w/ RAC @ 1385 TO PREMIUM SWINE FINISHER MASH @ 1330.', 'Manduae', 14, 19, 1, '2020-01-02 10:37:26', ''),
	(35, '2020-01-06 09:09:35', 'Update chart of account - PCV-126-122119134306\nFROM: Employee Benefits - Fuel(Main)\nTO: Fuel and Oil', 'RICHIE', 7, 19, 1, '2020-01-06 09:13:50', ''),
	(36, '2020-01-07 16:16:01', 'Update Bank ( Fund Transfer )\nRef #: CV-132-010720025818\nFrom : Cash in Bank -BDO (Bacolod) CA-6420\nTo: Cash in Bank - METRO (Bacolod) CA-91016-2', 'Onlly', 7, 19, 1, '2020-01-07 16:18:01', ''),
	(37, '2020-01-08 14:34:14', 'Update Declared Status \nFROM: Declared \nTO: Undeclared\n, Document Type\nFROM: Official Receipt\nTO: Others\n , Receipt Status\nFROM: W/ Receipt\nTO: W/O Receipt', 'Richie', 7, 19, 1, '2020-01-13 09:30:28', ''),
	(38, '2020-01-13 09:35:48', 'Update Declared Status\nFROM: DECLARED\nTO: UNDECLARED\nRef No.: PCV-126-010720105009', 'Richie', 7, 19, 1, '2020-01-13 09:37:26', ''),
	(39, '2020-01-13 10:22:27', 'Change Chart of Account (Pettycash)\nRef No.: PCV-191-072319180510\n\nDelete Double Entry\nRef No.:PCV-193-091219075931\n\nDelete Double Entry\nRef No.:RFV-124-070219082618', 'Richie', 7, 19, 1, '2020-01-15 10:35:37', ''),
	(40, '2020-01-15 10:34:13', 'Update Check # in Payment - Ref # CP-131-010920153949; PR # 3325; Customer Von Kierulf; FROM 00264 TO 00266', 'Mandaue', 14, 19, 1, '2020-01-15 10:35:34', ''),
	(41, '2020-01-16 13:36:50', 'Update CV Salaries and wages - Sales Team (Main) - Technical Team debit - Ref # CV-132-011620010627; FROM 24,250.00 TO 25,500.00.', 'Stephanie', 14, 19, 1, '2020-01-16 13:42:00', ''),
	(42, '2020-01-30 10:37:24', 'Update Product quantity in Request Stock Transfer  - Ref # REF-131-013020100243; CUSTOMIZED SWINE STARTER BASEMIX 2% FROM 4.00	TO 12.000; CUSTOMIZED SWINE GROWER BASEMIX 2% FROM 12.00 TO 4.', 'Astrid Chua', 14, 19, 1, '2020-01-30 10:40:17', ''),
	(43, '2020-02-08 11:10:32', 'Change Doc Type\nRef #: PCV-192-020820103924\nFrom: Pettycash Voucher\nTo: Official Receipt', 'SOUTH LUZON BRANCH', 7, 19, 1, '2020-02-08 11:18:59', ''),
	(44, '2020-02-15 11:05:16', 'Update Trucking Expense in RR - Ref # RR-124-011520105752; FROM Trucking Expense - Finished Product (MAIN) TO Trucking Expense - Raw Materials (MAIN)', 'Maricor', 14, 19, 1, '2020-02-15 11:06:07', ''),
	(45, '2020-02-26 10:17:58', 'Close Request Stock Transfer - Ref # RST-128-022420024738', 'Madel', 14, 19, 1, '2020-02-26 10:44:06', ''),
	(46, '2020-02-26 16:13:28', 'Update Check Date - Ref # CP-128-022020154631; Check # 682888; FROM February 19,2020 TO March 08,2020', 'Madel', 14, 19, 1, '2020-02-26 16:15:07', ''),
	(47, '2020-03-07 15:36:05', 'Update Product in Request Stock Transfer - Ref # RST-130-030720101453; FROM REGULAR SWINE LACTATING MASH TO PREMIUM SWINE LACTATING MASH', 'Passi', 14, 19, 1, '2020-03-07 15:37:30', ''),
	(48, '2020-03-20 14:53:19', 'Change Charge Invoice - Ref # DR-127-032020141619; FROM 1560 TO 1561; Ref # DR-127-032020144222; FROM 1561 TO 1560.', 'Iloilo', 14, 19, 1, '2020-03-20 14:53:41', ''),
	(50, '2020-03-21 11:18:56', 'Update Delivery date - Ref # DR-193-022820205217; CH Invoice # 6335; FROM Feb 28, 2020 TO March 2,2020', 'Ronel Fuentes', 14, 19, 1, '2020-03-21 11:19:53', ''),
	(52, '2020-04-13 14:17:26', 'Update Check in Payment - Ref # CP-128-040820145436; Customer CP-128-040820145436; FROM BDO 682901 682901 TO /rcbc 9000147 April 8,2020', 'Madel', 14, 19, 1, '2020-04-13 14:21:11', ''),
	(53, '2020-04-18 10:14:15', 'Update Charge invoice to Cash invoice - Ref # DR-126-041820094920; Customer Wilfreda Chavez; FROM charge invoice 1128 TO cash invoice 458.', 'BANTAYAN', 14, 19, 1, '2020-04-18 10:15:44', ''),
	(54, '2020-04-18 10:23:44', 'Update Declared to Undeclared in RR - Ref # RR-124-032520153422; Supplier New China Enterprise Inc..', 'Maricor', 14, 19, 1, '2020-04-18 10:25:26', '');
/*!40000 ALTER TABLE `tbl_request_logs` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_role
DROP TABLE IF EXISTS `tbl_role`;
CREATE TABLE IF NOT EXISTS `tbl_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_role: 5 rows
/*!40000 ALTER TABLE `tbl_role` DISABLE KEYS */;
INSERT INTO `tbl_role` (`role_id`, `role_name`) VALUES
	(0, 'Member'),
	(1, 'root'),
	(2, 'Project Manager'),
	(3, 'Accounting'),
	(4, 'Team Leader');
/*!40000 ALTER TABLE `tbl_role` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_task
DROP TABLE IF EXISTS `tbl_task`;
CREATE TABLE IF NOT EXISTS `tbl_task` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `projectCode` text NOT NULL,
  `taskDescription` text NOT NULL,
  `taskDueDate` datetime NOT NULL,
  `taskCreateDate` datetime NOT NULL,
  `status` int(1) NOT NULL COMMENT '0=todo, 1=on progress, 2=done',
  `priority_stats` int(1) NOT NULL COMMENT '0=low, 1=medium, 2=high',
  `task_code` varchar(50) NOT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=326 DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_task: ~234 rows (approximately)
/*!40000 ALTER TABLE `tbl_task` DISABLE KEYS */;
INSERT INTO `tbl_task` (`task_id`, `projectCode`, `taskDescription`, `taskDueDate`, `taskCreateDate`, `status`, `priority_stats`, `task_code`) VALUES
	(1, 'J6K5L4BM21', 'login module', '2019-08-30 00:00:00', '2019-07-24 00:00:00', 2, 2, 'RAS64751'),
	(3, 'J6K5L4BM21', 'update script files', '2019-08-30 00:00:00', '2019-07-24 00:00:00', 2, 1, 'P7944751'),
	(5, 'J6K5L4BM21', 'update due accounts report put extended days', '2019-08-30 00:00:00', '2019-07-24 00:00:00', 0, 1, 'D8BA4751'),
	(6, 'J6K5L4BM21', 'Inventory adjustment change dr, cr fetching', '2019-10-31 00:00:00', '2019-07-24 00:00:00', 2, 2, 'YYGJ4751'),
	(8, 'J6K5L4BM21', 'Patch (Local Server)', '2020-12-30 00:00:00', '2019-07-25 15:47:46', 2, 1, 'FC764751'),
	(13, 'J6K5L4BM21', 'corporate check', '2020-11-27 00:00:00', '2019-08-23 16:52:00', 2, 1, 'X6LO4751'),
	(14, 'J6K5L4BM21', 'credit memo', '2020-12-01 00:00:00', '2019-08-23 16:53:30', 2, 1, 'FXY04751'),
	(15, 'J6K5L4BM21', 'check memo', '2019-12-20 00:00:00', '2019-08-23 16:53:46', 2, 1, 'BAGC4751'),
	(20, 'J6K5L4BM21', 'Fomulation Filter by ga pilit dyapun please check', '2019-08-30 00:00:00', '2019-08-27 10:18:02', 0, 1, 'JB464751'),
	(21, 'J6K5L4BM21', 'Microfilming revised', '2019-09-21 00:00:00', '2019-08-27 10:24:51', 1, 1, 'APNH4751'),
	(23, 'J6K5L4BM21', 'Add packaging material in formulation projection. formula  [iban nga gagamit pgd ka sako += (defaultFormulation_sack * monthly_produced)]', '2019-08-31 00:00:00', '2019-08-27 16:57:36', 2, 2, 'AMER4751'),
	(25, 'J6K5L4BM21', 'Remove Projected Base On Packaging Materials  sa sidebar(Projection > Packaging Material)', '2019-08-31 00:00:00', '2019-08-28 14:35:54', 2, 0, 'FJRO4751'),
	(26, 'J6K5L4BM21', 'Create Feed Production Variance > Master Data,\nkung ano ang na set sa Feed Production Variance, for example 1% , anything +/- 1% dapat ma color red', '2019-08-31 00:00:00', '2019-08-28 14:38:31', 2, 2, 'ABT04751'),
	(27, 'J6K5L4BM21', 'Declared Undeclared Status for every Company\nkung Off ang Declared/Undeclared status sa company ,auto declared sa tnan na may dropdown nga dec/undec \nremove Filter by', '2019-08-31 00:00:00', '2019-08-28 14:40:39', 2, 2, 'M3TH4751'),
	(28, 'J6K5L4BM21', 'Duplicate Formulation dapat mkabaton decimals ang factor', '2019-08-31 00:00:00', '2019-08-28 14:43:56', 2, 1, 'I2IJ4751'),
	(31, 'J6K5L4BM21', 'Mark user color in Accounts Payable Ledger', '2019-08-31 00:00:00', '2019-08-29 09:48:36', 2, 1, 'DOWG4751'),
	(32, 'J6K5L4BM21', 'Mark user color in Accounts Receivable Ledger', '2019-08-31 00:00:00', '2019-08-29 09:48:47', 2, 2, '4RRV4751'),
	(33, 'J6K5L4BM21', 'update asset files', '2019-08-31 00:00:00', '2019-08-29 10:21:26', 2, 0, 'Q31M4751'),
	(36, 'Q9W8ETYUO', 'mac installer for nutritionist formulation', '2019-09-30 00:00:00', '2019-08-29 10:23:50', 0, 1, 'EYUL4751'),
	(37, 'J6K5L4BM21', 'Mark user color in Accounts Payable Ledger', '2019-08-31 00:00:00', '2019-08-29 10:24:20', 2, 2, 'INT44751'),
	(40, 'J6K5L4BM21', 'Inventory Adjustment Report', '2019-08-31 00:00:00', '2019-08-29 13:56:22', 2, 2, 'M8CB4751'),
	(41, 'J6K5L4BM21', 'CANDLING ADD REJECTED OPTION, ADD TO WAREHOUSE INVENTORY\n', '2019-09-04 00:00:00', '2019-08-29 15:58:17', 2, 1, '2O3X4751'),
	(42, 'J6K5L4BM21', 'EGGS SALES OF PCS AND TRAYS (SPECIAL CONDITION)', '2019-09-04 00:00:00', '2019-08-29 15:59:53', 2, 2, 'DUUZ4751'),
	(43, 'J6K5L4BM21', 'MASS FEEDING CHECK ALL ENABLE TEXTBOXES SALA', '2019-09-04 00:00:00', '2019-08-29 16:00:34', 2, 1, 'XYUX4751'),
	(44, 'J6K5L4BM21', 'MASS FEEDING LEGEND SALA', '2019-09-04 00:00:00', '2019-08-29 16:00:54', 2, 1, 'IFWH4751'),
	(45, 'J6K5L4BM21', 'MED/VAC CONSUMPTION â€”â€”> CRITICAL INVENTORY REPORT (PIG) MED/VAC ONLY', '2019-09-04 00:00:00', '2019-08-29 16:01:11', 2, 1, 'X4YO4751'),
	(48, 'J6K5L4BM21', 'ALL DATA-TOGGLE FORMULA IN REPORT KAKSON', '2019-09-04 00:00:00', '2019-08-29 16:06:09', 2, 1, '072D4751'),
	(49, 'J6K5L4BM21', 'Credit Memo - Delivery Return (Remove Delivery Return)', '2019-09-07 00:00:00', '2019-08-30 10:02:07', 2, 2, '4FY64751'),
	(50, 'J6K5L4BM21', 'Debit Memo - Purchase Return (Remove Purchase Return)', '2019-09-07 00:00:00', '2019-08-30 10:02:19', 2, 2, 'R2NY4752'),
	(52, 'J6K5L4BM21', 'ALL REPORTS COMBINE ARCHIVE', '2020-12-31 00:00:00', '2019-08-30 12:01:13', 1, 1, 'B5VA4752'),
	(54, 'J6K5L4BM21', 'If enable Declared/Undeclare Company \nremove List of Declared/Undeclare Purchases in sidebar', '2019-08-17 00:00:00', '2019-08-30 12:02:58', 2, 1, '1AY24752'),
	(55, 'J6K5L4BM21', 'BALANCE SHEET V.S. INCOME STATEMENT (TO BE REVIEWED)', '2020-12-31 00:00:00', '2019-08-30 12:02:58', 0, 0, 'J6VL4752'),
	(56, 'J6K5L4BM21', 'BALANCE SHEET - MONTHLY REPORT (TO BE REVIEWED)', '2020-12-31 00:00:00', '2019-08-30 12:03:53', 1, 0, 'EG3Q4752'),
	(57, 'J6K5L4BM21', 'Update report related to swine sales (change status F to R)', '2019-09-14 00:00:00', '2019-08-30 12:03:54', 2, 2, 'XSWD4752'),
	(59, 'J6K5L4BM21', 'GRAPH REPORT - ALL CHART', '2020-12-31 00:00:00', '2019-08-30 12:04:45', 1, 0, 'F33S4752'),
	(60, 'Q9W8ETYUO', 'Formulate with Client who does not subscribed with NOTES program.', '2019-09-05 00:00:00', '2019-08-30 12:05:15', 2, 1, 'HUKW4752'),
	(61, 'J6K5L4BM21', 'AI Production', '2019-09-30 00:00:00', '2019-08-30 12:05:40', 2, 1, 'OKHT4752'),
	(62, 'J6K5L4BM21', 'PETTYCASH/REVOLVING - P.O.', '2020-12-31 00:00:00', '2019-08-30 12:05:46', 0, 0, '2BQB4752'),
	(63, 'J6K5L4BM21', 'Customize Farm Reports', '2019-09-07 00:00:00', '2019-08-30 12:06:44', 2, 1, 'GNVT4752'),
	(64, 'J6K5L4BM21', '', '2020-12-31 00:00:00', '2019-08-30 12:07:03', 0, 0, 'Y2DN4752'),
	(65, 'J6K5L4BM21', 'Mass Table syncing on uploading Database Schema from Dev to Production', '2019-09-15 00:00:00', '2019-08-30 12:07:04', 0, 0, 'CETX4752'),
	(66, 'J6K5L4BM21', 'Farm Statistics Details', '2019-09-30 00:00:00', '2019-08-30 12:07:24', 2, 1, 'PEDP4752'),
	(68, 'J6K5L4BM21', 'Update Date Related Farm Stats', '2019-09-30 00:00:00', '2019-08-30 12:08:28', 2, 1, 'LNV54752'),
	(69, 'J6K5L4BM21', '13TH MONTH/SEPARATION PAY', '2020-12-31 00:00:00', '2019-08-30 12:08:46', 0, 0, 'NVZJ4752'),
	(70, 'Q9W8ETYUO', 'Encode data in OPTINOTES , fetch from HENDRIX OR EvaPig database.', '2019-08-31 00:00:00', '2019-08-30 12:09:12', 2, 1, 'DZDC4752'),
	(72, 'J6K5L4BM21', 'List of Declared/Undeclare Purchases If declared ,mga finish transaction connected sa purchases dpat ma declare ang status like gl_tran_header\nIf undeclare put RR-UD + ref_number status ma update to undeclare', '2019-08-17 00:00:00', '2019-08-30 12:46:40', 2, 2, 'LD3Z4752'),
	(73, 'J6K5L4BM21', '2316 REPORT', '2020-12-31 00:00:00', '2019-08-30 13:22:55', 0, 0, 'ENKH4752'),
	(74, 'J6K5L4BM21', 'EMPLOYEE ALPHA LIST', '2020-12-31 00:00:00', '2019-08-30 13:23:20', 2, 0, 'VKPL4752'),
	(76, 'J6K5L4BM21', 'JOURNAL ENTRY - BYPASS STATUS FOR ENTRY', '2019-07-31 00:00:00', '2019-08-30 13:25:26', 2, 1, 'SALR4752'),
	(77, 'J6K5L4BM21', 'CASH FLOW STATEMENT', '2020-12-31 00:00:00', '2019-08-30 13:34:01', 2, 2, 'ANMP4752'),
	(78, 'Q9W8ETYUO', 'Simulate from Formulation fetch Raw Materials with quantity only', '2019-09-08 00:00:00', '2019-08-31 08:40:09', 2, 0, 'K4G54752'),
	(81, 'Q9W8ETYUO', 'Saving Formula to Master Data save all Nutrients', '2019-09-07 00:00:00', '2019-08-31 10:24:19', 2, 0, 'LVHT4752'),
	(82, 'J6K5L4BM21', 'Advances attach PO (Payable)', '2019-09-21 00:00:00', '2019-08-31 15:35:27', 2, 1, 'L1RH4752'),
	(83, 'J6K5L4BM21', 'Cancel PO per Items', '2019-09-07 00:00:00', '2019-08-31 15:36:00', 2, 0, 'XS8K4752'),
	(84, 'J6K5L4BM21', 'Purchase Report by Supplier', '2019-09-28 00:00:00', '2019-08-31 15:36:44', 2, 1, 'EB1T4752'),
	(85, 'J6K5L4BM21', 'Purchase Report by Category', '2019-09-28 00:00:00', '2019-08-31 15:37:06', 2, 1, 'U0SH4752'),
	(87, 'J6K5L4BM21', 'Deposit multi check', '2019-09-30 00:00:00', '2019-08-31 15:39:34', 1, 1, '1IV84752'),
	(88, 'J6K5L4BM21', 'Change Price in PO - Allow all pending items', '2019-09-27 00:00:00', '2019-08-31 15:43:32', 0, 2, 'CTOL4752'),
	(89, 'J6K5L4BM21', 'Print and Export Data in Delivery', '2019-09-30 00:00:00', '2019-08-31 15:44:27', 1, 2, 'B8JS4752'),
	(90, 'J6K5L4BM21', 'Accounts Transfer', '0000-00-00 00:00:00', '2019-08-31 15:45:05', 2, 2, 'BRG14752'),
	(91, 'J6K5L4BM21', 'PAYROLL', '2019-07-31 00:00:00', '2019-08-31 15:52:10', 2, 2, 'UQXA4752'),
	(92, 'J6K5L4BM21', 'SYNC ARCHIVE DATABASE', '2019-07-31 00:00:00', '2019-08-31 15:52:57', 2, 2, 'BF6T4752'),
	(93, 'J6K5L4BM21', 'DYNAMIC EXPENSE BUDGET', '2019-07-31 00:00:00', '2019-08-31 15:53:29', 2, 2, 'WVA14752'),
	(94, 'J6K5L4BM21', 'CONSUMABLES SCRIPT', '2019-08-31 00:00:00', '2019-08-31 15:55:02', 2, 1, 'B3NR4752'),
	(95, 'J6K5L4BM21', 'MONTHLY CONSUMABLES CONSUMED REPORT', '2019-07-31 00:00:00', '2019-08-31 15:55:48', 2, 2, '2OXM4752'),
	(96, 'J6K5L4BM21', 'UPDATE OLD GLTRAN DETAIL USER COLOR', '2019-10-01 00:00:00', '2019-08-31 15:57:32', 1, 2, 'HMHG4752'),
	(97, 'J6K5L4BM21', 'BALANCE SHEET RESTRUCTURE (COMPUTATION)', '2019-09-07 00:00:00', '2019-08-31 15:59:45', 2, 2, 'VZPC4752'),
	(98, 'J6K5L4BM21', 'PURCHASE AND EXPENSE BUDGET', '2019-08-24 00:00:00', '2019-08-31 16:00:29', 2, 2, 'UBYW4752'),
	(99, 'Q9W8ETYUO', 'MINOR TASK\n* FORMULATION => Enable/Disabled Change to (CHANGE STATUS)', '2019-09-30 00:00:00', '2019-09-02 10:43:29', 2, 1, 'NDV04752'),
	(100, 'J6K5L4BM21', 'Allow Boar for Mass culling', '2019-09-09 00:00:00', '2019-09-02 10:50:58', 2, 2, 'XFWY4752'),
	(101, 'J6K5L4BM21', 'Delete Cull entry if swine sales return', '2019-09-16 00:00:00', '2019-09-02 10:51:41', 2, 2, '732X4752'),
	(102, 'J6K5L4BM21', 'Add Farm statistics (Culled boar & Gilt)', '2019-09-09 00:00:00', '2019-09-02 10:52:38', 2, 2, 'LUBX4752'),
	(103, 'J6K5L4BM21', 'SPOILAGE OF EGGS REPORT \n1. RENAME: AMOUNT -> PESO VALUE \n2.ADD TOTAL (ALL COMPUTATION BASE IN RANGE) \n3.CUMMULATIVE (COMPUTATION BASE IN RANGE)', '2019-09-06 00:00:00', '2019-09-02 11:16:47', 2, 1, 'VSSO4752'),
	(104, 'J6K5L4BM21', 'SALES \n1. CURRENT QTY: PCS AND OTHER PACKAGING SPACING \n2. PCS -> OTHER PACKAGING AUTO CONVERT ONLY', '2019-09-06 00:00:00', '2019-09-02 11:17:21', 2, 2, '533N4752'),
	(105, 'J6K5L4BM21', 'INCUBATION REPORT \n1. LAYOUT BASE ON NOTEBOOK', '2019-09-06 00:00:00', '2019-09-02 11:17:49', 2, 1, 'XOCJ4752'),
	(106, 'J6K5L4BM21', 'INCUBATION \n1.IF FINISHED STAY DATA FOR HISTORY PURPOSES \n2.ADD FILTER (ALL,IN PROGRESS,CANDLING,FINISHED) \n3.IF NOT FINISHED IT IS STILL IN PROGRESS', '2019-09-06 00:00:00', '2019-09-02 11:18:17', 2, 1, 'VIIE4752'),
	(107, 'J6K5L4BM21', 'Add dry days in Daily Breeding Report', '2019-09-03 00:00:00', '2019-09-02 12:53:31', 2, 2, 'HNMD4752'),
	(108, 'J6K5L4BM21', 'Parity Report (Recheck # of heads weaned, Ave weaning wt,A,B,C)', '2019-09-06 00:00:00', '2019-09-02 12:54:16', 2, 1, 'LCER4752'),
	(109, 'J6K5L4BM21', 'Performance By Class (Recheck Formula)', '2019-09-16 00:00:00', '2019-09-02 12:54:53', 2, 0, 'AZLG4752'),
	(113, 'Q9W8ETYUO', 'LOCAL FORMULATION\n* NOT SUBSCRIBER CHANGE TO SUBSCRIBER\n* EXPORT FINISHED FORMULA (NOT SUBSCRIBER)\n', '2019-09-07 00:00:00', '2019-09-04 09:15:46', 2, 1, 'NHR14752'),
	(115, 'ASDX987XCV', 'Notes app: Parity report', '2020-12-31 00:00:00', '2019-09-04 10:20:07', 0, 0, 'GIFD4752'),
	(116, 'ASDX987XCV', 'Notes app: Accounts Recievable report', '2020-12-31 00:00:00', '2019-09-04 10:21:04', 0, 0, 'KHHR4752'),
	(120, 'ASDX987XCV', 'schednotes updates: on and off location to user logs', '2019-09-05 00:00:00', '2019-09-05 09:49:25', 2, 0, '1ASO4752'),
	(121, 'ASDX987XCV', 'Notes app: updates on UI\nRequisition Canvassing - button color gray when approved, Income Statement - date range up to future, notes app logo when logged in. ', '2019-09-05 00:00:00', '2019-09-05 09:50:00', 2, 0, 'YCUT4752'),
	(123, 'ASDX987XCV', 'RF scanner: updates on medication and vacination, feeding modules', '2019-09-07 00:00:00', '2019-09-05 09:50:59', 2, 2, 'TUVO4752'),
	(124, 'J6K5L4BM21', 'Salesman Collection - Change fetching date from deposit date to cleared date.', '2019-09-07 00:00:00', '2019-09-05 13:26:42', 2, 2, '2XE74752'),
	(125, 'J6K5L4BM21', 'Boar Performance Report  add column profit', '2019-09-21 00:00:00', '2019-09-05 14:12:57', 2, 0, '5RXW4752'),
	(127, 'Q9W8ETYUO', 'Saving formulation include 0 and disabled', '2019-09-19 00:00:00', '2019-09-05 14:27:11', 2, 2, 'HSQS4752'),
	(129, 'J6K5L4BM21', 'Approved By - Petty Cash, Revolving Fund, Cash Voucher and Check Voucher', '2019-09-14 00:00:00', '2019-09-07 09:24:55', 2, 1, 'KWKZ4752'),
	(130, 'J6K5L4BM21', 'Optimize Feeding Entry(Transaction)', '2019-09-18 00:00:00', '2019-09-07 10:19:41', 2, 0, 'SIET4752'),
	(134, 'J6K5L4BM21', 'BROODING AND PRODUCTION REPORT\n1. WATER ACTUAL- WEEKLY AND DAILY SALA\n2. ALL BATCH - MORTALITY ACTUAL (%) AND CUMM MORT (%) NUMBERFORMAT (2)\n', '2019-09-10 00:00:00', '2019-09-09 13:48:39', 2, 1, 'AFGL4752'),
	(139, 'J6K5L4BM21', 'MASS FEEDING: BROODING AND PRODUCTION\n\n1. CHANGE QTY --> QTY PER HEAD\n2. RANGE QTY PER HEAD (HIGH AND LOW FARM STANDARDS)\n3. QUANTITY TEXTBOX CONDITION FORMULA \n -QUANTITY PER HEAD TIMES POPULATION', '2019-09-13 00:00:00', '2019-09-10 15:26:17', 2, 1, 'SHC04752'),
	(140, 'J6K5L4BM21', 'ADD BROODING AND PORDUCTION\n\n1. RESULT NUMBER FORMAT 2 DECIMALS', '2019-09-06 00:00:00', '2019-09-10 15:32:33', 2, 1, '30Q64752'),
	(142, 'J6K5L4BM21', 'Farrowing Report(Print)', '2019-09-20 00:00:00', '2019-09-12 15:09:48', 2, 1, 'S7CI4752'),
	(143, 'ASDX987XCV', 'check voucher -approve\ncash voucher - approve', '2019-09-21 00:00:00', '2019-09-14 09:51:54', 1, 2, 'B5BV4752'),
	(144, 'ASDX987XCV', 'petty cash - approve\nrevolving fund - approve', '2019-09-21 00:00:00', '2019-09-14 09:52:34', 0, 2, 'WILM4752'),
	(145, 'ASDX987XCV', 'PO - approve', '2019-09-21 00:00:00', '2019-09-14 09:53:13', 1, 2, 'V6A64752'),
	(146, 'J6K5L4BM21', 'Add Alert Parameters (Ultrasound1, Ultrasound2)', '2019-09-21 00:00:00', '2019-09-14 14:10:01', 2, 0, '1K1L4752'),
	(148, 'J6K5L4BM21', 'Check ave ADG(Boar Performance Report)', '2019-09-23 00:00:00', '2019-09-14 14:11:09', 2, 1, 'YKYR4752'),
	(149, 'J6K5L4BM21', 'Finisher Age Sold (Review)', '2019-09-21 00:00:00', '2019-09-14 14:11:44', 2, 2, 'IW9H4752'),
	(150, 'J6K5L4BM21', 'Check Average Age(Daily Swine Delivery Report )', '2019-09-26 00:00:00', '2019-09-14 14:12:52', 2, 0, 'EVHF4752'),
	(152, 'J6K5L4BM21', 'Med & Vacc Schedule For Sow with 0 Parity', '2019-10-02 00:00:00', '2019-09-14 14:14:23', 2, 0, 'KSTU4752'),
	(158, 'Q9W8ETYUO', 'Import/export database', '2019-09-19 00:00:00', '2019-09-16 10:41:39', 2, 1, 'LME94752'),
	(159, 'Q9W8ETYUO', 'Import/export client', '2019-09-19 00:00:00', '2019-09-16 10:41:56', 2, 1, '6KNF4752'),
	(160, 'Q9W8ETYUO', 'formulation Import/export', '2019-09-19 00:00:00', '2019-09-16 10:42:22', 2, 1, 'FEKG4752'),
	(161, 'Q9W8ETYUO', '* one time login (get either machine code or generated code)\n* allow change password after install (first login)\n* save local and online\n* check credentials online/offline', '2019-09-19 00:00:00', '2019-09-16 10:45:48', 2, 1, 'UQZZ4752'),
	(163, 'J6K5L4BM21', 'import/export databases', '2019-09-19 00:00:00', '2019-09-16 10:46:25', 2, 1, 'H5FX4752'),
	(164, 'J6K5L4BM21', 'import/export formulation', '2019-09-19 00:00:00', '2019-09-16 10:46:39', 2, 1, 'AT7J4752'),
	(165, 'J6K5L4BM21', 'Nutritionist code \n* default pass is 12345\n* pili if gusto niya iya nga code\n* pili if aton nga code', '2019-09-19 00:00:00', '2019-09-16 10:48:23', 0, 2, 'SNAU4752'),
	(166, 'J6K5L4BM21', 'NATIVE optiNOTES application\n* exe\n* dmg', '2020-01-01 00:00:00', '2019-09-16 10:54:58', 1, 0, 'WCKV4752'),
	(167, 'J6K5L4BM21', 'Add , Remove employee inside payroll details.\nCancel Payroll Reverse Entry\n', '2019-09-20 00:00:00', '2019-09-17 16:53:46', 2, 1, 'WH4Q4752'),
	(168, 'J6K5L4BM21', 'Revolving Fund / Pettycash Fund Multi Branch Charging', '2019-09-21 00:00:00', '2019-09-19 10:16:48', 2, 1, 'EUN24752'),
	(169, 'J6K5L4BM21', 'Feeding Consumption (Dropdown)', '2019-09-25 00:00:00', '2019-09-23 08:59:22', 2, 2, 'DZJQ4752'),
	(170, 'J6K5L4BM21', 'All in One Report(New Module)', '2019-10-03 00:00:00', '2019-09-23 09:00:05', 2, 2, 'HX4R4752'),
	(171, 'J6K5L4BM21', 'Parity Performance ( Add Adg, Show Year , select months, Remove No. of Sows)', '2019-09-28 00:00:00', '2019-09-23 10:25:24', 2, 2, 'Y9SU4752'),
	(173, 'Q9W8ETYUO', 'Export (text file compatibility to other browser)', '2019-09-27 00:00:00', '2019-09-24 08:26:37', 2, 1, 'QG7R4752'),
	(174, 'J6K5L4BM21', 'Export (text file compatibility to other browser)', '2019-09-27 00:00:00', '2019-09-24 08:26:50', 2, 1, '2CGN4752'),
	(175, 'J6K5L4BM21', 'Export (computable optimized ndi isa.isa ang pag export)', '2019-09-30 00:00:00', '2019-09-24 08:29:40', 2, 1, 'AOH34752'),
	(176, 'Q9W8ETYUO', 'export (min-max local)', '2019-09-27 00:00:00', '2019-09-24 08:30:14', 2, 1, 'OSDZ4752'),
	(177, 'Q9W8ETYUO', 'add formula name when exporting nutrients', '2019-09-27 00:00:00', '2019-09-24 08:30:59', 2, 1, 'VXJL4752'),
	(181, 'J6K5L4BM21', 'DUPLICATE LAYER TO BRONOTES', '2020-05-09 00:00:00', '2019-09-24 13:31:51', 0, 2, 'IBNE4752'),
	(183, 'J6K5L4BM21', 'Move Swine Delivery to Farm Reports', '2019-09-25 00:00:00', '2019-09-25 08:48:21', 2, 2, 'UZED4752'),
	(184, 'J6K5L4BM21', 'Profit Per Parity (Add in Farm Statistics report)', '2019-10-09 00:00:00', '2019-09-25 09:12:46', 2, 1, 'WO7Z4752'),
	(185, 'J6K5L4BM21', 'Revise (Farrowing Report)', '2019-10-08 00:00:00', '2019-10-01 14:34:11', 0, 2, 'ZA5V4752'),
	(186, 'J6K5L4BM21', 'script for number of heads(Feeding)', '2019-10-01 00:00:00', '2019-10-01 14:35:19', 2, 2, '5N7S4752'),
	(188, 'J6K5L4BM21', 'Change zero to "" in Parity Report & farrowing Report', '2019-10-02 00:00:00', '2019-10-01 14:36:16', 2, 2, 'X8GW4752'),
	(189, 'J6K5L4BM21', 'Accounts Transfer in Report', '2019-10-05 00:00:00', '2019-10-05 08:14:02', 2, 2, 'ZHJS4752'),
	(190, 'J6K5L4BM21', 'Accounts Transfer in Payment', '2019-10-11 00:00:00', '2019-10-05 08:14:16', 2, 1, 'TPLM4752'),
	(191, 'J6K5L4BM21', 'Add download template and upload Finisher Age Sold (Farm Statistics)', '2019-10-19 00:00:00', '2019-10-08 11:27:59', 2, 1, 'K7I44752'),
	(192, 'J6K5L4BM21', 'Change background color farm statistics ', '2019-10-19 00:00:00', '2019-10-08 11:36:04', 2, 0, 'O95Q4752'),
	(193, 'J6K5L4BM21', 'Farm statistic explanation', '2019-10-31 00:00:00', '2019-10-08 15:56:49', 2, 1, 'LMPS4752'),
	(194, 'J6K5L4BM21', 'Upload update Swine Birthdate (Farm Stat)', '2019-10-11 00:00:00', '2019-10-09 09:47:33', 2, 1, '25PE4752'),
	(195, 'J6K5L4BM21', 'Feeding Transaction(revise)', '2019-10-15 00:00:00', '2019-10-09 09:48:08', 2, 1, '7BNM4752'),
	(196, 'J6K5L4BM21', 'Add Alert in Farm View ( Ultrasound 1 & 2 )', '2019-10-19 00:00:00', '2019-10-12 10:29:10', 2, 1, 'JZTV4752'),
	(197, 'J6K5L4BM21', 'Add weight @ 21 days old ( Swine Card )', '2019-10-21 00:00:00', '2019-10-14 13:56:07', 2, 0, 'PLNX4752'),
	(198, 'J6K5L4BM21', 'Add Mass Feeding Revision ', '2019-11-04 00:00:00', '2019-10-14 13:56:43', 0, 0, 'NB1G4752'),
	(199, 'J6K5L4BM21', 'Inventory report (W-W) all branch', '2019-10-31 00:00:00', '2019-10-14 14:49:08', 2, 2, 'DBLJ4752'),
	(200, 'J6K5L4BM21', 'patch new files to the local server', '2019-10-31 00:00:00', '2019-10-14 14:50:14', 1, 2, '1GO14752'),
	(201, 'J6K5L4BM21', 'Releasing Stock', '2019-10-31 00:00:00', '2019-10-15 09:23:25', 2, 2, '1GPI4752'),
	(202, 'J6K5L4BM21', 'Releasing Stock Report', '2019-10-31 00:00:00', '2019-10-15 09:23:37', 2, 2, 'XXBU4752'),
	(203, 'J6K5L4BM21', '[siteWide] hide product will not appear in all product dropdowns', '2019-10-31 00:00:00', '2019-10-15 14:30:59', 2, 2, '1MLD4752'),
	(205, 'J6K5L4BM21', 'EMPLOYEE COMPENSATION REPORT', '2019-12-31 00:00:00', '2019-10-19 10:05:53', 1, 1, 'WD8H4752'),
	(206, 'J6K5L4BM21', 'add total of swine in monthly delivery swine', '2019-10-26 00:00:00', '2019-10-21 15:40:05', 2, 1, 'WMUY4752'),
	(207, 'J6K5L4BM21', 'abort browser tab process. if possible background process para indi mag affect sa tanan nga tabs.', '2019-10-30 00:00:00', '2019-10-22 09:44:38', 0, 1, 'KA984752'),
	(208, 'J6K5L4BM21', 'Remove CTP others , Add Sow and piglet CTP', '2019-10-31 00:00:00', '2019-10-23 13:43:01', 2, 1, 'YSPM4752'),
	(210, 'J6K5L4BM21', 'Add e-signature in every print (PO, Check & Cash Voucher, * Petty Cash, * Revolving Fund)                                                                                                                                                                                                           ', '2019-10-26 00:00:00', '2019-10-23 15:43:13', 2, 2, '5APY4752'),
	(211, 'J6K5L4BM21', 'Income Statement Restructure', '2019-11-16 00:00:00', '2019-10-24 09:15:24', 2, 2, 'AOSL4752'),
	(212, 'J6K5L4BM21', 'Add FCR to daily swine delivery report', '2019-10-24 00:00:00', '2019-10-24 09:55:17', 2, 1, 'KDFY4752'),
	(213, 'J6K5L4BM21', 'Update price watch - cost = live wt price', '2019-10-26 00:00:00', '2019-10-24 09:56:42', 2, 2, 'NUMR4752'),
	(214, 'J6K5L4BM21', 'Swine delivery - disallow sales for swine with no birthdate', '2019-10-31 00:00:00', '2019-10-26 13:39:12', 2, 1, 'SOFR4752'),
	(215, 'J6K5L4BM21', 'Update Age sold in Swine Delivery', '2019-11-09 00:00:00', '2019-10-31 10:37:29', 2, 1, 'WLIO4752'),
	(216, 'J6K5L4BM21', 'User-agreement: Price watch: Price watch for all modules is preset "ON". If you do not want price watch, you can turn it off in Settings > Manage Company Information.', '2019-11-09 00:00:00', '2019-11-04 15:26:30', 2, 0, 'Y69O4752'),
	(217, 'J6K5L4BM21', 'Database Set Ingredients Default Error', '2019-11-06 00:00:00', '2019-11-06 08:57:17', 2, 2, 'LHCC4752'),
	(218, 'J6K5L4BM21', 'Ingredients with no Default must treat Original as default', '2019-11-06 00:00:00', '2019-11-06 08:58:25', 2, 2, '2STJ4752'),
	(219, 'J6K5L4BM21', 'Ingredients matrix value change limit to 5 decimals', '2019-11-09 00:00:00', '2019-11-06 08:59:20', 0, 0, '1TCM4752'),
	(220, 'J6K5L4BM21', 'Restructure swine card for faster page load', '2019-11-23 00:00:00', '2019-11-16 09:44:38', 2, 1, 'QQCO4752'),
	(222, 'J6K5L4BM21', 'Privilege for delete, approval,book closing, change cost etc. for users, owner no need to pop up for authentication', '2019-11-16 00:00:00', '2019-11-16 10:01:59', 2, 2, 'SP524752'),
	(224, 'J6K5L4BM21', 'All reports kung ano ang nka assign sa user branches, amo lang na ma gwa sa report dropdown branch', '2019-11-16 00:00:00', '2019-11-16 10:04:41', 2, 2, 'QM5N4752'),
	(225, 'J6K5L4BM21', 'Hide show password for authentication', '2019-11-16 00:00:00', '2019-11-16 10:05:23', 2, 0, 'OWZ04752'),
	(226, 'J6K5L4BM21', 'Users nka datatable ang privilege', '2019-11-16 00:00:00', '2019-11-16 10:06:06', 2, 2, '5Q204752'),
	(227, 'J6K5L4BM21', 'check ang assigned colors for users, kung nka inactive dapat ma update sa color nga default nga nka set', '2019-11-30 00:00:00', '2019-11-16 10:06:50', 0, 2, 'RJRZ4752'),
	(229, 'J6K5L4BM21', 'Add selectable column preset for parity performance reports (and related reports)', '2019-11-30 00:00:00', '2019-11-16 11:01:51', 0, 1, 'SXE24752'),
	(230, 'J6K5L4BM21', 'Add save group for customized farm report', '2019-11-23 00:00:00', '2019-11-16 11:03:06', 2, 1, 'KVPK4752'),
	(231, 'J6K5L4BM21', 'Add 21 day weight report', '2019-11-30 00:00:00', '2019-11-16 11:21:30', 2, 1, 'XQQD4752'),
	(232, 'J6K5L4BM21', 'E seperate ang Optinotes, Proximit Analysis sa feednotes, kung optinotes lang kwaon dpat sa system name optinotes man ma gwa', '2019-11-30 00:00:00', '2019-11-16 11:45:24', 2, 2, 'Z8G14752'),
	(233, 'J6K5L4BM21', 'Feeds Production Report filter per category(premix, basemix.concentrate)', '2019-12-15 00:00:00', '2019-11-25 15:18:39', 2, 2, '77MH4752'),
	(234, 'J6K5L4BM21', 'Check update productmaster wla ga views mag chage category', '2019-12-15 00:00:00', '2019-11-25 15:19:07', 0, 2, 'ZFIZ4752'),
	(235, 'J6K5L4BM21', 'tanan nga loading dapat uniform', '2019-12-15 00:00:00', '2019-11-27 11:52:53', 0, 2, '47N04752'),
	(237, 'J6K5L4BM21', 'Weigh swine at 21 days old - individual', '2019-11-30 00:00:00', '2019-11-27 16:50:36', 2, 2, 'PIY94752'),
	(238, 'J6K5L4BM21', 'Delivery Report (Change filter by Month and Year)', '2019-12-21 00:00:00', '2019-12-05 15:37:55', 2, 2, 'ZZS74752'),
	(240, 'J6K5L4BM21', 'Feeding archiving', '2019-12-28 00:00:00', '2019-12-11 10:07:09', 1, 2, 'WLVV4752'),
	(241, 'J6K5L4BM21', 'Mass swine entry - encode pen, age, and number of heads only. (Entry to be updated during sales)', '2019-12-28 00:00:00', '2019-12-11 16:46:05', 2, 0, 'OE8O4752'),
	(242, 'Q9W8ETYUO', 'set density bind to company for with/without one of the density\n(ex. [company1=>"sperlow, low, medium, high"], [company2=>"low, medium"])', '2020-01-07 00:00:00', '2020-01-02 10:56:37', 2, 1, 'OOJN4752'),
	(246, 'Q9W8ETYUO', 'DIY view formula (datatable only ang mag load)', '2020-01-07 00:00:00', '2020-01-02 11:02:42', 2, 1, 'FFXH4752'),
	(247, 'Q9W8ETYUO', 'DIY view formula (datatable only ang mag load)', '2020-01-07 00:00:00', '2020-01-02 11:02:49', 2, 1, '9YZA4752'),
	(248, 'Q9W8ETYUO', 'Double click DIY', '2020-01-07 00:00:00', '2020-01-02 11:03:11', 2, 1, 'AXSD4752'),
	(249, 'Q9W8ETYUO', 'Diff cost delayed in showing dapat indi', '2020-01-07 00:00:00', '2020-01-02 11:03:40', 2, 2, 'GYHW4752'),
	(252, 'J6K5L4BM21', 'Update finish swine details in swine delivery', '2020-01-02 00:00:00', '2020-01-07 09:08:03', 2, 0, 'G5EQ4752'),
	(253, 'J6K5L4BM21', 'Farm Statistics ADG and FCR Details', '2020-01-18 00:00:00', '2020-01-10 10:28:22', 2, 0, 'ECW74752'),
	(258, 'J6K5L4BM21', 'Customized Print in  Check Voucher', '2019-12-20 00:00:00', '2020-01-16 15:55:59', 2, 1, 'PH3D4752'),
	(259, 'J6K5L4BM21', 'Customized Print in  Cash Voucher', '2019-12-20 00:00:00', '2020-01-16 15:56:46', 2, 2, '9PUE4752'),
	(261, 'J6K5L4BM21', 'Customized Print in  Petty Cash', '2019-12-31 00:00:00', '2020-01-16 15:57:50', 2, 1, '4LPX4752'),
	(262, 'J6K5L4BM21', 'Customized Print in  Revolving Fund', '2019-12-31 00:00:00', '2020-01-16 15:58:19', 2, 1, 'GRJE4752'),
	(263, 'J6K5L4BM21', 'Customized Print in  Delivery', '2020-01-25 00:00:00', '2020-01-16 15:58:57', 2, 1, 'ZV9U4752'),
	(265, 'J6K5L4BM21', 'Customized Print in  Requisition', '2020-01-25 00:00:00', '2020-01-16 16:00:11', 2, 1, 'VUOD4752'),
	(266, 'J6K5L4BM21', 'Customized Print in  Purchase Order', '2020-01-31 00:00:00', '2020-01-16 16:00:39', 2, 1, 'KV024752'),
	(267, 'J6K5L4BM21', 'Customized Print in  Receiving', '2020-01-31 00:00:00', '2020-01-16 16:00:54', 2, 1, 'TBSB4752'),
	(268, 'J6K5L4BM21', 'Transfer Deposit', '2020-01-31 00:00:00', '2020-01-16 16:03:59', 2, 2, 'R5JG4752'),
	(269, 'J6K5L4BM21', 'Nutritionist Account', '2020-01-18 00:00:00', '2020-01-21 08:41:19', 2, 2, 'VGGD4752'),
	(270, 'J6K5L4BM21', 'Projection Formulation put with and without pending po(if 0 dont display)', '2020-01-11 00:00:00', '2020-01-21 08:46:08', 2, 1, 'JZ4C4752'),
	(271, 'J6K5L4BM21', 'Projection Job Order put with and without pending po(if 0 dont display)', '2020-01-11 00:00:00', '2020-01-21 08:47:13', 2, 1, 'KCZJ4752'),
	(272, 'J6K5L4BM21', 'Restructure Users Authentication', '2019-12-14 00:00:00', '2020-01-21 08:49:30', 2, 1, 'SZLZ4752'),
	(273, 'J6K5L4BM21', 'Job Order add and update', '2020-01-31 00:00:00', '2020-01-21 08:50:43', 1, 1, '8S7E4752'),
	(274, 'Q9W8ETYUO', 'mass optinmization\n* individual checking if enable price and inclusion\nStandard Formulation\nOptimize checker by group', '2020-01-31 00:00:00', '2020-01-22 09:01:12', 2, 2, '1PAI4752'),
	(276, 'J6K5L4BM21', 'AI semen production:\n*do not allow same boar for same date\n*put warning for boar if used less than 2 days\n', '2020-02-01 00:00:00', '2020-01-28 10:32:17', 2, 1, 'LGHX4752'),
	(277, 'J6K5L4BM21', 'Update Farm Statistics (Can update in every stats)', '2020-02-29 00:00:00', '2020-01-29 16:35:00', 1, 0, 'YUWP4752'),
	(278, 'J6K5L4BM21', 'Update transaction declared status', '2020-02-21 00:00:00', '2020-02-05 09:39:36', 2, 1, 'R1A44752'),
	(279, 'J6K5L4BM21', 'Swine export to excel. update mated history (remarks) and farrowing stats (A, B, C)', '2020-02-14 00:00:00', '2020-02-12 11:14:45', 2, 1, '6CHB4752'),
	(280, 'J6K5L4BM21', 'Fixed scroll update swine details in swine delivery', '2020-02-22 00:00:00', '2020-02-13 10:45:50', 0, 0, 'NRGC4752'),
	(281, 'J6K5L4BM21', 'Microfilming - Rotating image', '2020-03-07 00:00:00', '2020-02-17 14:22:23', 2, 1, 'DMIC4752'),
	(282, 'J6K5L4BM21', 'Corporate Check all Expenses', '2020-03-14 00:00:00', '2020-02-22 10:30:43', 1, 1, 'ZX3V4752'),
	(285, 'J6K5L4BM21', 'CV Customized Print - E-signature and Approved By Filed added', '2020-02-06 00:00:00', '2020-02-26 13:53:51', 2, 1, 'XOIM4752'),
	(286, 'J6K5L4BM21', 'DIY Mass Optimization\n   - joined with STD Mass Optimisation but not affected', '2020-02-29 00:00:00', '2020-02-27 09:18:36', 1, 1, '45TZ4752'),
	(287, 'J6K5L4BM21', 'STD Formulation\n - add manual (ma reflect sa view details sa gwa)\nSIMULATE FORMLA\n - after delete wrong redirection', '2020-02-29 00:00:00', '2020-02-27 09:21:50', 2, 1, 'RPZT4752'),
	(288, 'J6K5L4BM21', 'STD\n ( ) databases per company\n ( ) change database in formulation and break down tags\n ( ) stored cost per branch\n ( ) formulation per branch\n ( ) if 0 cost used stored cost', '2020-03-14 00:00:00', '2020-02-27 09:30:59', 0, 1, 'L8IQ4752'),
	(289, 'J6K5L4BM21', 'Customized Print - Delivery', '2020-03-07 00:00:00', '2020-02-28 09:05:05', 1, 1, '06C14752'),
	(290, 'J6K5L4BM21', 'Add Comparison in Delivery by customer (Amount)', '2020-03-14 00:00:00', '2020-02-28 14:24:58', 0, 1, 'AECX4752'),
	(291, 'J6K5L4BM21', 'Add Comparison in Delivery by Customer ( Packaging )', '2020-03-21 00:00:00', '2020-02-28 14:26:20', 0, 1, '1COW4752'),
	(292, 'J6K5L4BM21', 'Inventory adjusment filter by category', '2019-11-30 00:00:00', '2020-03-02 08:38:30', 2, 2, 'HMFM4752'),
	(293, 'J6K5L4BM21', 'Nutritionist Account\n-Owner limited in sidebar\n  -employee, employee classification\n  -branch,formulation,product category, productmaster\n  -optinotes', '2020-03-04 00:00:00', '2020-03-02 08:41:23', 2, 2, 'CJ0N4752'),
	(294, 'J6K5L4BM21', 'Nutritionist account \n- can duplicate product to other branches\n-checker if branches already product, cant dupplicate\n-cannot delete product that already used optinotes transaction', '2020-03-04 00:00:00', '2020-03-02 08:44:26', 2, 2, 'T3S54752'),
	(295, 'J6K5L4BM21', 'PROJECTION BASED ON JO\n-pending PO show, must not filter by date', '2020-03-14 00:00:00', '2020-03-02 08:46:25', 2, 1, 'JBF54752'),
	(297, 'J6K5L4BM21', 'NUTRITIONIST USER\n-UI for user expiry date( show if company type = nutritionist(1) )', '2020-03-21 00:00:00', '2020-03-02 08:52:25', 2, 1, 'SRAT4752'),
	(299, 'J6K5L4BM21', 'Dashboard for all pignotes transactions', '2020-03-14 00:00:00', '2020-03-06 09:50:47', 1, 1, 'ZIXF4752'),
	(300, 'J6K5L4BM21', 'Mass AI Breeding', '2020-03-31 00:00:00', '2020-03-07 08:55:47', 0, 1, 'K4BK4752'),
	(301, 'J6K5L4BM21', 'Revised ADFI report with assigning of products by corresponding feed type', '2020-03-31 00:00:00', '2020-03-07 08:56:51', 0, 1, 'PNUA4752'),
	(302, 'J6K5L4BM21', 'Sorting in Delivery Reports', '2020-03-03 00:00:00', '2020-03-07 10:24:53', 2, 1, 'J2BM4752'),
	(303, 'J6K5L4BM21', 'Remove header farm statistics', '2020-03-14 00:00:00', '2020-03-09 10:02:24', 1, 0, 'JOAZ4752'),
	(304, 'J6K5L4BM21', 'Update breeding in transaction: if AI breeding disable boar1, boar2, boar3 in update', '2020-03-28 00:00:00', '2020-03-09 14:24:17', 0, 1, '3JPF4752'),
	(306, 'J6K5L4BM21', 'PROJECTION (revised)\n-Formulation\n-Packaging\n-Jo', '2020-03-14 00:00:00', '2020-03-10 10:58:27', 2, 1, 'R8LS4752'),
	(307, 'J6K5L4BM21', 'Beginning Balance AP and AR declared status', '2020-03-28 00:00:00', '2020-03-17 14:46:57', 0, 2, '4HZZ4752'),
	(311, 'J6K5L4BM21', 'Individual Employee Salary Ledger (With Microfilming)', '2020-04-04 00:00:00', '2020-04-02 08:39:25', 2, 2, '8HFI4752'),
	(312, 'J6K5L4BM21', 'All Employee Yearly Earnings', '2020-04-04 00:00:00', '2020-04-02 08:39:46', 2, 2, 'S7KG4752'),
	(313, 'J6K5L4BM21', 'Include Holiday Pay to Statutory benefits', '2020-04-04 00:00:00', '2020-04-02 08:40:14', 2, 2, 'GQQB4752'),
	(314, 'J6K5L4BM21', 'Fix Functions (Manual Add Employee to payroll , Update Days Bulk Employee)', '2020-04-04 00:00:00', '2020-04-02 08:40:56', 2, 2, 'ICZP4752'),
	(315, 'J6K5L4BM21', 'DTR Summary print color per legend', '2020-04-04 00:00:00', '2020-04-02 11:48:15', 1, 2, 'U3YX4752'),
	(316, 'J6K5L4BM21', 'Compensation Report\n-Auto Display Statutory Benefits if Employee resigned or terminated\n-Fixed Employee Display\n-Production Incentives\n-Summary Computation (Check formulas)', '2020-04-04 00:00:00', '2020-04-02 11:51:52', 2, 2, 'GKSN4752'),
	(317, 'J6K5L4BM21', 'Loan Report\n- Summary (All Employees)', '2020-04-04 00:00:00', '2020-04-02 11:52:14', 2, 2, 'Z7DE4752'),
	(318, 'J6K5L4BM21', 'Payroll Computation and Print\n-Salary Increase (Recheck)\n-Report Print (iframe)', '2020-04-04 00:00:00', '2020-04-02 11:53:10', 2, 2, 'YVKU4752'),
	(323, 'J6K5L4BM21', 'archiving structure', '2020-05-30 00:00:00', '2020-05-01 17:07:50', 0, 2, 'PKAR0750'),
	(324, 'Q9W8ETYUO', 'test task 123', '2020-05-10 00:00:00', '2020-05-10 21:44:17', 0, 1, 'ICST4417'),
	(325, 'Q9W8ETYUO', 'kajsldkjalksjdlkaj kajdkajslkjdA\nSDAS\nDA\nSD\nAS\nD\nASD\nAS\nD', '2020-05-30 00:00:00', '2020-05-10 21:44:33', 0, 2, '14DQ4433');
/*!40000 ALTER TABLE `tbl_task` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_task_member
DROP TABLE IF EXISTS `tbl_task_member`;
CREATE TABLE IF NOT EXISTS `tbl_task_member` (
  `task_member_id` int(11) NOT NULL AUTO_INCREMENT,
  `teamCode` text NOT NULL,
  `projectCode` text NOT NULL,
  `task_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`task_member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=327 DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_task_member: ~233 rows (approximately)
/*!40000 ALTER TABLE `tbl_task_member` DISABLE KEYS */;
INSERT INTO `tbl_task_member` (`task_member_id`, `teamCode`, `projectCode`, `task_id`, `user_id`) VALUES
	(1, '6D5GB5NM4', 'J6K5L4BM21', 1, 2),
	(3, '6D5GB5NM4', 'J6K5L4BM21', 3, 2),
	(5, 'B5M4YTU', 'J6K5L4BM21', 5, 4),
	(6, 'B5M4YTU', 'J6K5L4BM21', 6, 3),
	(7, '6D5GB5NM4', 'J6K5L4BM21', 8, 2),
	(14, 'X9CV63S39', 'J6K5L4BM21', 13, 14),
	(15, 'X9CV63S39', 'J6K5L4BM21', 14, 14),
	(16, 'X9CV63S39', 'J6K5L4BM21', 15, 14),
	(21, 'B5M4YTU', 'J6K5L4BM21', 20, 3),
	(22, 'B5M4YTU', 'J6K5L4BM21', 21, 4),
	(24, 'B5M4YTU', 'J6K5L4BM21', 23, 3),
	(26, 'B5M4YTU', 'J6K5L4BM21', 25, 3),
	(27, 'B5M4YTU', 'J6K5L4BM21', 26, 3),
	(28, 'B5M4YTU', 'J6K5L4BM21', 27, 3),
	(29, 'B5M4YTU', 'J6K5L4BM21', 28, 3),
	(32, 'X9CV63S39', 'J6K5L4BM21', 31, 14),
	(33, 'X9CV63S39', 'J6K5L4BM21', 32, 14),
	(34, '6D5GB5NM4', 'J6K5L4BM21', 33, 2),
	(37, '6D5GB5NM4', 'Q9W8ETYUO', 36, 5),
	(38, 'X9CV63S39', 'J6K5L4BM21', 37, 14),
	(41, 'B5M4YTU', 'J6K5L4BM21', 40, 3),
	(42, 'BNM568FGH', 'J6K5L4BM21', 41, 10),
	(43, 'BNM568FGH', 'J6K5L4BM21', 42, 10),
	(44, 'BNM568FGH', 'J6K5L4BM21', 43, 13),
	(45, 'BNM568FGH', 'J6K5L4BM21', 44, 13),
	(46, 'BNM568FGH', 'J6K5L4BM21', 45, 13),
	(49, 'BNM568FGH', 'J6K5L4BM21', 48, 13),
	(50, 'X9CV63S39', 'J6K5L4BM21', 49, 14),
	(51, 'X9CV63S39', 'J6K5L4BM21', 50, 14),
	(53, 'FY54SD621', 'J6K5L4BM21', 52, 8),
	(55, 'B5M4YTU', 'J6K5L4BM21', 54, 3),
	(56, 'FY54SD621', 'J6K5L4BM21', 55, 6),
	(57, 'FY54SD621', 'J6K5L4BM21', 56, 6),
	(58, 'U4IO5HJK5', 'J6K5L4BM21', 57, 12),
	(60, 'FY54SD621', 'J6K5L4BM21', 59, 6),
	(61, '6D5GB5NM4', 'Q9W8ETYUO', 60, 5),
	(62, 'U4IO5HJK5', 'J6K5L4BM21', 61, 9),
	(63, 'FY54SD621', 'J6K5L4BM21', 62, 7),
	(64, 'U4IO5HJK5', 'J6K5L4BM21', 63, 9),
	(65, 'FY54SD621', 'J6K5L4BM21', 64, 7),
	(66, '6D5GB5NM4', 'J6K5L4BM21', 65, 5),
	(67, 'U4IO5HJK5', 'J6K5L4BM21', 66, 11),
	(69, 'U4IO5HJK5', 'J6K5L4BM21', 68, 11),
	(70, 'FY54SD621', 'J6K5L4BM21', 69, 7),
	(71, '6D5GB5NM4', 'Q9W8ETYUO', 70, 5),
	(73, 'B5M4YTU', 'J6K5L4BM21', 72, 3),
	(74, 'FY54SD621', 'J6K5L4BM21', 73, 7),
	(75, 'FY54SD621', 'J6K5L4BM21', 74, 7),
	(77, 'FY54SD621', 'J6K5L4BM21', 76, 8),
	(78, 'FY54SD621', 'J6K5L4BM21', 77, 6),
	(79, '6D5GB5NM4', 'Q9W8ETYUO', 78, 5),
	(82, '6D5GB5NM4', 'Q9W8ETYUO', 81, 5),
	(83, 'X9CV63S39', 'J6K5L4BM21', 82, 14),
	(84, 'X9CV63S39', 'J6K5L4BM21', 83, 14),
	(85, 'X9CV63S39', 'J6K5L4BM21', 84, 14),
	(86, 'X9CV63S39', 'J6K5L4BM21', 85, 14),
	(87, 'X9CV63S39', 'J6K5L4BM21', 86, 14),
	(88, 'X9CV63S39', 'J6K5L4BM21', 87, 14),
	(89, 'X9CV63S39', 'J6K5L4BM21', 88, 14),
	(90, 'X9CV63S39', 'J6K5L4BM21', 89, 14),
	(91, 'X9CV63S39', 'J6K5L4BM21', 90, 14),
	(92, 'FY54SD621', 'J6K5L4BM21', 91, 7),
	(93, 'FY54SD621', 'J6K5L4BM21', 92, 8),
	(94, 'FY54SD621', 'J6K5L4BM21', 93, 6),
	(95, 'FY54SD621', 'J6K5L4BM21', 94, 8),
	(96, 'FY54SD621', 'J6K5L4BM21', 95, 8),
	(97, 'FY54SD621', 'J6K5L4BM21', 96, 6),
	(98, 'FY54SD621', 'J6K5L4BM21', 97, 6),
	(99, 'FY54SD621', 'J6K5L4BM21', 98, 6),
	(100, '6D5GB5NM4', 'Q9W8ETYUO', 99, 5),
	(101, 'U4IO5HJK5', 'J6K5L4BM21', 100, 12),
	(102, 'U4IO5HJK5', 'J6K5L4BM21', 101, 12),
	(103, 'U4IO5HJK5', 'J6K5L4BM21', 102, 11),
	(104, 'BNM568FGH', 'J6K5L4BM21', 103, 10),
	(105, 'BNM568FGH', 'J6K5L4BM21', 104, 10),
	(106, 'BNM568FGH', 'J6K5L4BM21', 105, 10),
	(107, 'BNM568FGH', 'J6K5L4BM21', 106, 10),
	(108, 'U4IO5HJK5', 'J6K5L4BM21', 107, 12),
	(109, 'U4IO5HJK5', 'J6K5L4BM21', 108, 12),
	(110, 'U4IO5HJK5', 'J6K5L4BM21', 109, 12),
	(114, '6D5GB5NM4', 'Q9W8ETYUO', 113, 5),
	(116, 'RGMG45S83', 'ASDX987XCV', 115, 16),
	(117, 'RGMG45S83', 'ASDX987XCV', 116, 16),
	(121, 'RGMG45S83', 'ASDX987XCV', 120, 16),
	(122, 'RGMG45S83', 'ASDX987XCV', 121, 16),
	(124, 'RGMG45S83', 'ASDX987XCV', 123, 17),
	(125, 'U4IO5HJK5', 'J6K5L4BM21', 124, 12),
	(126, 'U4IO5HJK5', 'J6K5L4BM21', 125, 9),
	(128, '6D5GB5NM4', 'Q9W8ETYUO', 127, 5),
	(130, 'X9CV63S39', 'J6K5L4BM21', 129, 15),
	(131, 'U4IO5HJK5', 'J6K5L4BM21', 130, 9),
	(135, 'BNM568FGH', 'J6K5L4BM21', 134, 13),
	(140, 'BNM568FGH', 'J6K5L4BM21', 139, 13),
	(141, 'BNM568FGH', 'J6K5L4BM21', 140, 10),
	(143, 'U4IO5HJK5', 'J6K5L4BM21', 142, 12),
	(144, 'RGMG45S83', 'ASDX987XCV', 143, 16),
	(145, 'RGMG45S83', 'ASDX987XCV', 144, 17),
	(146, 'RGMG45S83', 'ASDX987XCV', 145, 16),
	(147, 'U4IO5HJK5', 'J6K5L4BM21', 146, 12),
	(149, 'U4IO5HJK5', 'J6K5L4BM21', 148, 12),
	(150, 'U4IO5HJK5', 'J6K5L4BM21', 149, 11),
	(151, 'U4IO5HJK5', 'J6K5L4BM21', 150, 12),
	(153, 'U4IO5HJK5', 'J6K5L4BM21', 152, 12),
	(159, '6D5GB5NM4', 'Q9W8ETYUO', 158, 5),
	(160, '6D5GB5NM4', 'Q9W8ETYUO', 159, 5),
	(161, '6D5GB5NM4', 'Q9W8ETYUO', 160, 5),
	(162, '6D5GB5NM4', 'Q9W8ETYUO', 161, 5),
	(164, '6D5GB5NM4', 'J6K5L4BM21', 163, 5),
	(165, '6D5GB5NM4', 'J6K5L4BM21', 164, 5),
	(166, '6D5GB5NM4', 'J6K5L4BM21', 165, 2),
	(167, '6D5GB5NM4', 'J6K5L4BM21', 166, 2),
	(168, 'FY54SD621', 'J6K5L4BM21', 167, 7),
	(169, 'FY54SD621', 'J6K5L4BM21', 168, 7),
	(170, 'U4IO5HJK5', 'J6K5L4BM21', 169, 12),
	(171, 'U4IO5HJK5', 'J6K5L4BM21', 170, 12),
	(172, 'U4IO5HJK5', 'J6K5L4BM21', 171, 12),
	(174, '6D5GB5NM4', 'Q9W8ETYUO', 173, 5),
	(175, '6D5GB5NM4', 'J6K5L4BM21', 174, 5),
	(176, '6D5GB5NM4', 'J6K5L4BM21', 175, 5),
	(177, '6D5GB5NM4', 'Q9W8ETYUO', 176, 5),
	(178, '6D5GB5NM4', 'Q9W8ETYUO', 177, 5),
	(182, 'BNM568FGH', 'J6K5L4BM21', 181, 10),
	(184, 'U4IO5HJK5', 'J6K5L4BM21', 183, 12),
	(185, 'U4IO5HJK5', 'J6K5L4BM21', 184, 11),
	(186, '', 'J6K5L4BM21', 185, 9),
	(187, 'U4IO5HJK5', 'J6K5L4BM21', 186, 12),
	(189, 'U4IO5HJK5', 'J6K5L4BM21', 188, 12),
	(190, 'X9CV63S39', 'J6K5L4BM21', 189, 14),
	(191, 'X9CV63S39', 'J6K5L4BM21', 190, 14),
	(192, 'U4IO5HJK5', 'J6K5L4BM21', 191, 11),
	(193, 'U4IO5HJK5', 'J6K5L4BM21', 192, 11),
	(194, 'U4IO5HJK5', 'J6K5L4BM21', 193, 11),
	(195, 'U4IO5HJK5', 'J6K5L4BM21', 194, 12),
	(196, 'U4IO5HJK5', 'J6K5L4BM21', 195, 12),
	(197, 'U4IO5HJK5', 'J6K5L4BM21', 196, 9),
	(198, 'U4IO5HJK5', 'J6K5L4BM21', 197, 9),
	(199, 'U4IO5HJK5', 'J6K5L4BM21', 198, 9),
	(200, '6D5GB5NM4', 'J6K5L4BM21', 199, 2),
	(201, '6D5GB5NM4', 'J6K5L4BM21', 200, 2),
	(202, 'X9CV63S39', 'J6K5L4BM21', 201, 14),
	(203, 'X9CV63S39', 'J6K5L4BM21', 202, 14),
	(204, '6D5GB5NM4', 'J6K5L4BM21', 203, 2),
	(206, 'FY54SD621', 'J6K5L4BM21', 205, 6),
	(207, 'U4IO5HJK5', 'J6K5L4BM21', 206, 11),
	(208, '6D5GB5NM4', 'J6K5L4BM21', 207, 2),
	(209, 'U4IO5HJK5', 'J6K5L4BM21', 208, 11),
	(211, 'X9CV63S39', 'J6K5L4BM21', 210, 15),
	(212, 'FY54SD621', 'J6K5L4BM21', 211, 7),
	(213, 'U4IO5HJK5', 'J6K5L4BM21', 212, 9),
	(214, 'U4IO5HJK5', 'J6K5L4BM21', 213, 9),
	(215, 'U4IO5HJK5', 'J6K5L4BM21', 214, 9),
	(216, 'U4IO5HJK5', 'J6K5L4BM21', 215, 11),
	(217, 'U4IO5HJK5', 'J6K5L4BM21', 216, 9),
	(218, '6D5GB5NM4', 'J6K5L4BM21', 217, 5),
	(219, '6D5GB5NM4', 'J6K5L4BM21', 218, 5),
	(220, '6D5GB5NM4', 'J6K5L4BM21', 219, 5),
	(221, 'U4IO5HJK5', 'J6K5L4BM21', 220, 9),
	(223, 'B5M4YTU', 'J6K5L4BM21', 222, 3),
	(225, 'B5M4YTU', 'J6K5L4BM21', 224, 3),
	(226, 'B5M4YTU', 'J6K5L4BM21', 225, 3),
	(227, 'B5M4YTU', 'J6K5L4BM21', 226, 3),
	(228, 'B5M4YTU', 'J6K5L4BM21', 227, 3),
	(230, 'U4IO5HJK5', 'J6K5L4BM21', 229, 12),
	(231, 'U4IO5HJK5', 'J6K5L4BM21', 230, 9),
	(232, 'U4IO5HJK5', 'J6K5L4BM21', 231, 9),
	(233, 'B5M4YTU', 'J6K5L4BM21', 232, 3),
	(234, 'B5M4YTU', 'J6K5L4BM21', 233, 3),
	(235, 'B5M4YTU', 'J6K5L4BM21', 234, 3),
	(236, 'B5M4YTU', 'J6K5L4BM21', 235, 4),
	(238, 'U4IO5HJK5', 'J6K5L4BM21', 237, 9),
	(239, 'U4IO5HJK5', 'J6K5L4BM21', 238, 12),
	(241, 'U4IO5HJK5', 'J6K5L4BM21', 240, 9),
	(242, 'U4IO5HJK5', 'J6K5L4BM21', 241, 9),
	(243, '6D5GB5NM4', 'Q9W8ETYUO', 242, 5),
	(247, '6D5GB5NM4', 'Q9W8ETYUO', 246, 5),
	(248, '6D5GB5NM4', 'Q9W8ETYUO', 247, 5),
	(249, '6D5GB5NM4', 'Q9W8ETYUO', 248, 5),
	(250, '6D5GB5NM4', 'Q9W8ETYUO', 249, 5),
	(253, 'U4IO5HJK5', 'J6K5L4BM21', 252, 11),
	(254, 'U4IO5HJK5', 'J6K5L4BM21', 253, 11),
	(259, 'X9CV63S39', 'J6K5L4BM21', 258, 15),
	(260, 'X9CV63S39', 'J6K5L4BM21', 259, 15),
	(262, 'X9CV63S39', 'J6K5L4BM21', 261, 15),
	(263, 'X9CV63S39', 'J6K5L4BM21', 262, 15),
	(264, 'X9CV63S39', 'J6K5L4BM21', 263, 15),
	(266, 'X9CV63S39', 'J6K5L4BM21', 265, 15),
	(267, 'X9CV63S39', 'J6K5L4BM21', 266, 15),
	(268, 'X9CV63S39', 'J6K5L4BM21', 267, 15),
	(269, 'X9CV63S39', 'J6K5L4BM21', 268, 14),
	(270, 'B5M4YTU', 'J6K5L4BM21', 269, 3),
	(271, 'B5M4YTU', 'J6K5L4BM21', 270, 3),
	(272, 'B5M4YTU', 'J6K5L4BM21', 271, 3),
	(273, 'B5M4YTU', 'J6K5L4BM21', 272, 3),
	(274, 'B5M4YTU', 'J6K5L4BM21', 273, 3),
	(275, '6D5GB5NM4', 'Q9W8ETYUO', 274, 5),
	(277, 'U4IO5HJK5', 'J6K5L4BM21', 276, 9),
	(278, 'U4IO5HJK5', 'J6K5L4BM21', 277, 11),
	(279, 'X9CV63S39', 'J6K5L4BM21', 278, 14),
	(280, 'U4IO5HJK5', 'J6K5L4BM21', 279, 12),
	(281, 'U4IO5HJK5', 'J6K5L4BM21', 280, 11),
	(282, 'X9CV63S39', 'J6K5L4BM21', 281, 15),
	(283, 'X9CV63S39', 'J6K5L4BM21', 282, 14),
	(286, 'X9CV63S39', 'J6K5L4BM21', 285, 15),
	(287, '6D5GB5NM4', 'J6K5L4BM21', 286, 5),
	(288, '6D5GB5NM4', 'J6K5L4BM21', 287, 2),
	(289, '6D5GB5NM4', 'J6K5L4BM21', 288, 5),
	(290, 'X9CV63S39', 'J6K5L4BM21', 289, 15),
	(291, 'U4IO5HJK5', 'J6K5L4BM21', 290, 12),
	(292, 'U4IO5HJK5', 'J6K5L4BM21', 291, 12),
	(293, 'B5M4YTU', 'J6K5L4BM21', 292, 3),
	(294, 'B5M4YTU', 'J6K5L4BM21', 293, 3),
	(295, 'B5M4YTU', 'J6K5L4BM21', 294, 3),
	(296, 'B5M4YTU', 'J6K5L4BM21', 295, 3),
	(298, 'B5M4YTU', 'J6K5L4BM21', 297, 3),
	(300, 'U4IO5HJK5', 'J6K5L4BM21', 299, 9),
	(301, 'U4IO5HJK5', 'J6K5L4BM21', 300, 9),
	(302, 'U4IO5HJK5', 'J6K5L4BM21', 301, 12),
	(303, 'X9CV63S39', 'J6K5L4BM21', 302, 14),
	(304, 'U4IO5HJK5', 'J6K5L4BM21', 303, 11),
	(305, 'U4IO5HJK5', 'J6K5L4BM21', 304, 12),
	(307, 'B5M4YTU', 'J6K5L4BM21', 306, 3),
	(308, 'X9CV63S39', 'J6K5L4BM21', 307, 14),
	(312, 'FY54SD621', 'J6K5L4BM21', 311, 7),
	(313, 'FY54SD621', 'J6K5L4BM21', 312, 7),
	(314, 'FY54SD621', 'J6K5L4BM21', 313, 7),
	(315, 'FY54SD621', 'J6K5L4BM21', 314, 7),
	(316, 'FY54SD621', 'J6K5L4BM21', 315, 7),
	(317, 'FY54SD621', 'J6K5L4BM21', 316, 7),
	(318, 'FY54SD621', 'J6K5L4BM21', 317, 7),
	(319, 'FY54SD621', 'J6K5L4BM21', 318, 7),
	(324, '', 'J6K5L4BM21', 323, 2),
	(325, '', 'Q9W8ETYUO', 324, 2),
	(326, '', 'Q9W8ETYUO', 325, 2);
/*!40000 ALTER TABLE `tbl_task_member` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_team
DROP TABLE IF EXISTS `tbl_team`;
CREATE TABLE IF NOT EXISTS `tbl_team` (
  `team_id` int(11) NOT NULL AUTO_INCREMENT,
  `teamCode` varchar(50) NOT NULL,
  `teamName` text NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`team_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_team: ~7 rows (approximately)
/*!40000 ALTER TABLE `tbl_team` DISABLE KEYS */;
INSERT INTO `tbl_team` (`team_id`, `teamCode`, `teamName`, `created_by`) VALUES
	(1, '6D5GB5NM4', 'Optinotes', 18),
	(2, 'B5M4YTU', 'Feednotes Team', 18),
	(3, 'FY54SD621', 'Accounting Team', 18),
	(4, 'U4IO5HJK5', 'Pignotes Team', 18),
	(5, 'BNM568FGH', 'Eggnotes Team', 18),
	(6, 'RGMG45S83', 'Mobile Dev', 18),
	(7, 'X9CV63S39', 'NOTES Global', 18),
	(8, '6D5GB5N64', 'Optinotes 123', 2);
/*!40000 ALTER TABLE `tbl_team` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_team_member
DROP TABLE IF EXISTS `tbl_team_member`;
CREATE TABLE IF NOT EXISTS `tbl_team_member` (
  `team_member_id` int(11) NOT NULL AUTO_INCREMENT,
  `teamCode` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`team_member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_team_member: ~16 rows (approximately)
/*!40000 ALTER TABLE `tbl_team_member` DISABLE KEYS */;
INSERT INTO `tbl_team_member` (`team_member_id`, `teamCode`, `user_id`, `role_id`) VALUES
	(2, '6D5GB5NM4', 2, 0),
	(3, 'B5M4YTU', 3, 0),
	(4, 'B5M4YTU', 4, 0),
	(5, '6D5GB5NM4', 5, 0),
	(6, 'FY54SD621', 6, 0),
	(7, 'FY54SD621', 7, 0),
	(8, 'FY54SD621', 8, 0),
	(9, 'U4IO5HJK5', 9, 0),
	(10, 'BNM568FGH', 10, 0),
	(11, 'U4IO5HJK5', 11, 0),
	(12, 'U4IO5HJK5', 12, 0),
	(13, 'BNM568FGH', 13, 0),
	(14, 'X9CV63S39', 14, 0),
	(15, 'X9CV63S39', 15, 0),
	(16, 'RGMG45S83', 16, 0),
	(17, 'RGMG45S83', 17, 0),
	(18, '6D5GB5N64', 5, 0),
	(19, '6D5GB5N64', 2, 0);
/*!40000 ALTER TABLE `tbl_team_member` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_transfer_logs
DROP TABLE IF EXISTS `tbl_transfer_logs`;
CREATE TABLE IF NOT EXISTS `tbl_transfer_logs` (
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `inv_id` int(11) NOT NULL,
  `log_message` text NOT NULL,
  `log_user` int(11) NOT NULL,
  `date_transfered` date NOT NULL,
  PRIMARY KEY (`trans_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_transfer_logs: 0 rows
/*!40000 ALTER TABLE `tbl_transfer_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_transfer_logs` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_unit
DROP TABLE IF EXISTS `tbl_unit`;
CREATE TABLE IF NOT EXISTS `tbl_unit` (
  `unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_name` text NOT NULL,
  `unit_qty` int(11) NOT NULL,
  PRIMARY KEY (`unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_unit: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_unit` DISABLE KEYS */;
INSERT INTO `tbl_unit` (`unit_id`, `unit_name`, `unit_qty`) VALUES
	(1, 'pcs', 1);
/*!40000 ALTER TABLE `tbl_unit` ENABLE KEYS */;

-- Dumping structure for table pms.tbl_users
DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(75) NOT NULL,
  `role_id` int(11) NOT NULL,
  `status` varchar(3) NOT NULL,
  `email` varchar(150) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table pms.tbl_users: ~19 rows (approximately)
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;
INSERT INTO `tbl_users` (`user_id`, `name`, `username`, `password`, `role_id`, `status`, `email`) VALUES
	(1, 'Root', 'root', '827ccb0eea8a706c4c34a16891f84e7b', 1, '0', 'roo@root.com'),
	(2, 'Judywen Guapin', 'jag', '52aacdcb5f4a5ae9e8f9defb7aed9b1f', 1, '1', 'jag@mail.com'),
	(3, 'John Mark Amar', 'johnmark', '827ccb0eea8a706c4c34a16891f84e7b', 0, '1', 'jmark@mail.com'),
	(4, 'Rose Canoy', 'rose', '827ccb0eea8a706c4c34a16891f84e7b', 0, '1', 'rose@mail.com'),
	(5, 'Eduard Rino Carton', 'rino', '1cbc99b5dcaa0e5623ae299ef80d7e49', 0, '1', 'rino@mail.com'),
	(6, 'Jerry Saydoquis', 'jerry', '827ccb0eea8a706c4c34a16891f84e7b', 0, '0', 'jerry@mail.com'),
	(7, 'Abel Bayon', 'abel', '827ccb0eea8a706c4c34a16891f84e7b', 0, '0', 'abel@mail.com'),
	(8, 'Miljan Jonota', 'miljan', '827ccb0eea8a706c4c34a16891f84e7b', 0, '0', 'miljan@mail.com'),
	(9, 'Jeffred Lim', 'jefred', '827ccb0eea8a706c4c34a16891f84e7b', 0, '0', 'jefred@mail.com'),
	(10, 'Rafael Claveria', 'rafael', '827ccb0eea8a706c4c34a16891f84e7b', 0, '1', 'rafael@mail.com'),
	(11, 'Kim Jade Baroa', 'kim', '827ccb0eea8a706c4c34a16891f84e7b', 0, '1', 'kim@mail.com'),
	(12, 'Jochelle Bravo', 'q', '7694f4a66316e53c8cdd9d9954bd611d', 0, '1', 'jochelle@mail.com'),
	(13, 'Arman Jacolbe', 'arman', '827ccb0eea8a706c4c34a16891f84e7b', 0, '1', 'arman@mail.com'),
	(14, 'Kaye Jacildo', 'kaye', '71e4e5af2c51dabe73732781a9275b30', 0, '1', 'kaye@mail.com'),
	(15, 'Ginery Songaling', 'ginery', '827ccb0eea8a706c4c34a16891f84e7b', 0, '1', 'ginery@mail.com'),
	(16, 'Juanito Hinolan Navalesca Jr', 'juanito', '827ccb0eea8a706c4c34a16891f84e7b', 0, '0', 'jr@mail.com'),
	(17, 'Aron Andrada', 'aron', '827ccb0eea8a706c4c34a16891f84e7b', 0, '1', 'aron@mail.com'),
	(18, 'Wilson Dy', 'wilson', '827ccb0eea8a706c4c34a16891f84e7b', 2, '0', 'wilson@mail.com'),
	(19, 'Pristine Baguios', 'pristine', '827ccb0eea8a706c4c34a16891f84e7b', 3, '0', 'pristine@mail.com');
/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
